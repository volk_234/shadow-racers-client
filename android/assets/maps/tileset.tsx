<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tileset" tilewidth="32" tileheight="32" spacing="8" margin="4" tilecount="156" columns="12">
 <image source="tileset.png" width="512" height="512"/>
 <terraintypes>
  <terrain name="Новый участок" tile="0"/>
 </terraintypes>
 <tile id="6" terrain=",,,0"/>
 <tile id="7" terrain=",,0,0"/>
 <tile id="8" terrain=",,0,"/>
 <tile id="18" terrain=",0,,0"/>
 <tile id="19" terrain="0,0,0,0"/>
 <tile id="20" terrain="0,,0,"/>
 <tile id="30" terrain=",0,,"/>
 <tile id="31" terrain="0,0,,"/>
 <tile id="32" terrain="0,,,"/>
 <tile id="42" probability="0.001"/>
 <tile id="43" probability="0.001"/>
 <tile id="44" probability="0.001"/>
 <tile id="56" probability="0.001"/>
</tileset>
