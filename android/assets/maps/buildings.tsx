<?xml version="1.0" encoding="UTF-8"?>
<tileset name="buildings" tilewidth="32" tileheight="32" spacing="8" margin="4" tilecount="144" columns="12">
 <image source="buildings.png" width="512" height="512"/>
 <terraintypes>
  <terrain name="building" tile="13"/>
 </terraintypes>
 <tile id="0" terrain=",,,0"/>
 <tile id="1" terrain=",,0,0"/>
 <tile id="2" terrain=",,0,"/>
 <tile id="12" terrain=",0,,0"/>
 <tile id="13" terrain="0,0,0,0"/>
 <tile id="14" terrain="0,,0,"/>
 <tile id="24" terrain=",0,,"/>
 <tile id="25" terrain="0,0,,"/>
 <tile id="26" terrain="0,,,"/>
</tileset>
