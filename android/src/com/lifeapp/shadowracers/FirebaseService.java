package com.lifeapp.shadowracers;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FirebaseService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            try {
                if (remoteMessage.getData().get("type").equals("newGame")) {
                    AndroidLauncher.offerOnline();
                }
            } catch (NullPointerException e) {

            }
        }
    }
}
