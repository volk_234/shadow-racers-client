package com.lifeapp.shadowracers;

import android.app.Activity;
import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

public class AndroidFireBaseApi implements FireBaseApi {
    private Activity activity;
    ImageManager imageManager;
    static int RC_SIGN_IN = 1;
    static int RC_LEADERBOARD_UI = 2;
    static int RESULT_RECONNECT_REQUIRED = 10001;
    private GoogleSignInAccount googleSignInAccount;

    AndroidFireBaseApi(Activity activity) {
        this.activity = activity;
        FirebaseApp.initializeApp(this.activity);
        imageManager = ImageManager.create(activity);
//        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(activity);
//        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
//                .requestEmail()
//                .build();
    }

    @Override
    public void subscribeToTopic(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
    }

    @Override
    public void unSubscribeFromTopic(String topic) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
    }

    @Override
    public String getIID() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    @Override
    public void showLeaderBoardForLevel(int level) {
        int resourceId = getStringIdentifier("leaderboard_level_" + level);
        Games.getLeaderboardsClient(activity, googleSignInAccount)
                .getLeaderboardIntent(activity.getString(resourceId))
                .addOnSuccessListener(intent -> activity.startActivityForResult(intent, RC_LEADERBOARD_UI));
    }

    @Override
    public void putScoreForLevel(int level, float score) {
        int resourceId = getStringIdentifier("leaderboard_level_" + level);
        Games.getLeaderboardsClient(activity, googleSignInAccount)
                .submitScore(activity.getString(resourceId), (long) score);
    }

    @Override
    public boolean isSignedIn() {
        return googleSignInAccount != null;
    }

    @Override
    public void signInSilently() {
        GoogleSignInClient signInClient = GoogleSignIn.getClient(activity,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        signInClient.silentSignIn().addOnCompleteListener(activity,
                task -> {
                    if (task.isSuccessful()) {
                        googleSignInAccount = task.getResult();
                    } else {
                        startSignInIntent();
                    }
                });
    }

    @Override
    public void startSignInIntent() {
        GoogleSignInClient signInClient = GoogleSignIn.getClient(activity,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        Intent intent = signInClient.getSignInIntent();
        activity.startActivityForResult(intent, RC_SIGN_IN);
    }

    @Override
    public ResponseListener<ArrayList<LbScore>> getTopScores(int level) {
        if (!isSignedIn()) {
            return null;
        }
        ResponseListener<ArrayList<LbScore>> listener = new ResponseListener<>();

        int resourceId = getStringIdentifier("leaderboard_level_" + level);
        Task<AnnotatedData<LeaderboardsClient.LeaderboardScores>> task =
                Games.getLeaderboardsClient(activity, googleSignInAccount).loadTopScores(activity.getString(resourceId),
                        LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC,
                        5, true);
        task.addOnCompleteListener(activity, resultTask -> {
            if (resultTask.isSuccessful()) {
                LeaderboardsClient.LeaderboardScores leaderboardScores = resultTask.getResult().get();
                if (leaderboardScores != null) {
                    ArrayList<LbScore> scores = new ArrayList<>();
                    for (int i = 0; i < leaderboardScores.getScores().getCount(); i++) {
                        LeaderboardScore leaderboardScore = leaderboardScores.getScores().get(i);
                        LbScore lbScore = new LbScore();
                        lbScore.displayName = leaderboardScore.getScoreHolderDisplayName();
                        lbScore.displayScore = leaderboardScore.getDisplayScore();
                        lbScore.rank = leaderboardScore.getRank();
                        scores.add(lbScore);
                    }
                    leaderboardScores.release();
                    listener.success(scores);
                }
            }
        });
        return listener;
    }

    @Override
    public ResponseListener<LbScore> getPlayerScore(int level) {
        if (!isSignedIn()) {
            return null;
        }
        ResponseListener<LbScore> listener = new ResponseListener<>();

        int resourceId = getStringIdentifier("leaderboard_level_" + level);
        Task<AnnotatedData<LeaderboardScore>> task =
                Games.getLeaderboardsClient(activity, googleSignInAccount).loadCurrentPlayerLeaderboardScore(
                        activity.getString(resourceId),
                        LeaderboardVariant.TIME_SPAN_ALL_TIME,
                        LeaderboardVariant.COLLECTION_PUBLIC);
        task.addOnCompleteListener(activity, resultTask -> {
            if (resultTask.isSuccessful()) {
                LeaderboardScore leaderboardScore = resultTask.getResult().get();
                if (leaderboardScore != null) {
                    LbScore lbScore = new LbScore();
                    lbScore.displayName = leaderboardScore.getScoreHolderDisplayName();
                    lbScore.displayScore = leaderboardScore.getDisplayScore();
                    lbScore.rank = leaderboardScore.getRank();
                    listener.success(lbScore);
                }
            }
        });
        return listener;
    }

    public void setGoogleSignInAccount(GoogleSignInAccount googleSignInAccount) {
        this.googleSignInAccount = googleSignInAccount;
    }

    private int getStringIdentifier(String name) {
        return activity.getResources().getIdentifier(name, "string", activity.getPackageName());
    }
}
