package com.lifeapp.shadowracers.listeners;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.mapobjects.NpcCar;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.variables.CategoryBits;

import java.util.Objects;

/**
 * Created by Александр on 15.01.2018.
 */

public class WorldContactListener implements ContactListener {

    private Mode mode;

    public WorldContactListener(Mode mode) {
        this.mode = mode;
    }

    @Override
    public void beginContact(Contact contact) {
        if (isContactOf(contact, CategoryBits.PLAYER | CategoryBits.GOAL)) {
            mode.goalReached();
        } else if (isContactOf(contact, CategoryBits.PLAYER | CategoryBits.OBJECT)) {
            Objects.requireNonNull(getBodyWithType(contact, BaseCar.class)).collision(contact);
        } else if (isContactOf(contact, CategoryBits.PLAYER | CategoryBits.NPC_CAR)) {
            Objects.requireNonNull(getBodyWithType(contact, BaseCar.class)).collision(contact);
            Objects.requireNonNull(getBodyWithType(contact, NpcCar.class)).panic(contact);
        } else if (isContactOf(contact, CategoryBits.PLAYER | CategoryBits.CAR)) {
            Objects.requireNonNull(getBodyWithType(contact, BaseCar.class)).collision(contact);
        } else if (isContactOf(contact, CategoryBits.CAR | CategoryBits.OBJECT)) {
            Objects.requireNonNull(getBodyWithType(contact, BaseCar.class)).collision(contact);
        } else if (isContactOf(contact, CategoryBits.CAR | CategoryBits.NPC_CAR)) {
            Objects.requireNonNull(getBodyWithType(contact, NpcCar.class)).panic(contact);
        } else if (isContactOf(contact, CategoryBits.CAR)) {
            Objects.requireNonNull(getBodyWithType(contact, BaseCar.class)).collision(contact);
        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private boolean isContactOf(Contact contact, int mask) {
        return ((contact.getFixtureA().getUserData() != null) &&
                (contact.getFixtureB().getUserData() != null)) &&
                (((Short) contact.getFixtureA().getUserData() |
                        (Short) contact.getFixtureB().getUserData()) == mask);
    }

    private <T> T getBodyWithType(Contact contact, Class<T> clazz) {
        if (contact.getFixtureA().getBody().getUserData() != null &&
                clazz.isInstance(contact.getFixtureA().getBody().getUserData())) {
            return (clazz.cast(contact.getFixtureA().getBody().getUserData()));
        } else if (contact.getFixtureB().getBody().getUserData() != null &&
                clazz.isInstance(contact.getFixtureB().getBody().getUserData())) {
            return (clazz.cast(contact.getFixtureB().getBody().getUserData()));
        }
        return null;
    }
}

