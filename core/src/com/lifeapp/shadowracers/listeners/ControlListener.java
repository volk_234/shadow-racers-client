package com.lifeapp.shadowracers.listeners;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 14.12.2017.
 */

public class ControlListener extends InputListener {

    private Mode mode;
    private Vector2 control = new Vector2();
    public enum Control {Left, Up, Right, Down}

    public ControlListener(Mode mode) {
        this.mode = mode;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        event.getListenerActor().addAction(Actions.alpha(1));
        switch ((Control) event.getListenerActor().getUserObject()) {
            case Left:
                control.x = -1;
                break;
            case Up:
                control.y = 1;
                break;
            case Right:
                control.x = 1;
                break;
            case Down:
                control.y = -1;
                break;
        }
        mode.control(control);
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        event.getListenerActor().addAction(Actions.alpha(0.5f));
        switch ((Control) event.getListenerActor().getUserObject()) {
            case Left:
                control.x = 0;
                break;
            case Up:
                control.y = 0;
                break;
            case Right:
                control.x = 0;
                break;
            case Down:
                control.y = 0;
                break;
        }
        mode.control(control);
    }
}
