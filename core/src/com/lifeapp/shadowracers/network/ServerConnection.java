package com.lifeapp.shadowracers.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;
import com.lifeapp.shadowracers.network.messages.ErrorMessage;
import com.lifeapp.shadowracers.network.messages.GameEndMessage;
import com.lifeapp.shadowracers.network.messages.GameInitMessage;
import com.lifeapp.shadowracers.network.messages.GameStartMessage;
import com.lifeapp.shadowracers.network.messages.GoalReachedMessage;
import com.lifeapp.shadowracers.network.messages.LobbyFoundMessage;
import com.lifeapp.shadowracers.network.messages.PlayerExitMessage;
import com.lifeapp.shadowracers.network.messages.PlayerJoinMessage;
import com.lifeapp.shadowracers.network.messages.PlayerReadyMessage;
import com.lifeapp.shadowracers.network.messages.RequestLobbyMessage;
import com.lifeapp.shadowracers.network.messages.StateMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Александр on 18.01.2018.
 */

public class ServerConnection {
    private Client client;

    private Listener listener;
    //        private String ip = "188.225.37.50";
    private String ip = "192.168.3.162";
    private int tcpPort = 8082;
    private int udpPort = 8083;


    public ServerConnection() {

        client = new Client();

        Kryo kryo = client.getKryo();
        kryo.register(ErrorMessage.class);
        kryo.register(RequestLobbyMessage.class);
        kryo.register(GameInitMessage.class);
        kryo.register(GameEndMessage.class);
        kryo.register(GoalReachedMessage.class);
        kryo.register(StateMessage.class);
        kryo.register(PlayerReadyMessage.class);
        kryo.register(GameStartMessage.class);
        kryo.register(ArrayList.class);
        kryo.register(HashMap.class);
        kryo.register(String[].class);
        kryo.register(int[].class);
        kryo.register(float[].class);
        kryo.register(PlayerExitMessage.class);
        kryo.register(PlayerJoinMessage.class);
        kryo.register(LobbyFoundMessage.class);
        kryo.register(PlayerModel.class);
        kryo.register(PlayerModel[].class);

        client.start();
        reconnect();
    }

    public void reconnect() {
        try {
            client.connect(5000, ip, tcpPort, udpPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isAlive() {
        return client.isConnected();
    }

    public void sendTCP(Object object) {
        client.sendTCP(object);
    }

    public void sendUDP(Object object) {
        client.sendUDP(object);
    }

    public void close() {
        try {
            client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListener(Listener newListener) {
        if (listener != null)
            client.removeListener(this.listener);
        listener = newListener;
        client.addListener(listener);
    }
}
