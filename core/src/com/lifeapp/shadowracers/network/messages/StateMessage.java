package com.lifeapp.shadowracers.network.messages;

public class StateMessage {
    public String name;
    public float controlX, controlY;
    public float positionX, positionY;
    public float speedX, speedY;
    public float angle;

    public StateMessage() {

    }

    public StateMessage(String name, float controlX, float controlY,
                        float positionX, float positionY, float speedX, float speedY, float angle) {
        this.name = name;
        this.controlX = controlX;
        this.controlY = controlY;
        this.positionX = positionX;
        this.positionY = positionY;
        this.speedX = speedX;
        this.speedY = speedY;
        this.angle = angle;
    }
}
