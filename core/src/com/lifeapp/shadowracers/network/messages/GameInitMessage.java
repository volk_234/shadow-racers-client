package com.lifeapp.shadowracers.network.messages;

import com.lifeapp.shadowracers.network.PlayerModel;

/**
 * Created by Александр on 16.03.2018.
 */

public class GameInitMessage {
    public PlayerModel[] playerModels;
    public int mode;
    public int[] spawns;
    public int[] goals;

    public GameInitMessage() {
    }
}
