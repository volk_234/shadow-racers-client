package com.lifeapp.shadowracers.network;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;


public class OnlinePlayer {
    protected MyGame game = (MyGame) Gdx.app.getApplicationListener();
    private BaseCar car;
    private PlayerModel playerModel;
    private Image pointerImage;

    private Label scoreLabel;
    private HorizontalGroup horizontalGroup;

    public float score = 0;
    public Color color;

    public OnlinePlayer(BaseCar car, PlayerModel playerModel, Color color) {
        this.car = car;
        this.playerModel = playerModel;
        this.color = color;

        Skin skin = MyGame.getSkin();

        pointerImage = new Image(skin.newDrawable(Assets.POINTER, color)) {
            @Override
            public void act(float delta) {
                super.act(delta);
                Mode mode = game.gameScreen.getMode();
                if (mode.getDstFromPlayer(car.getBody().getPosition()) < 15) {
                    pointerImage.setVisible(false);
                    return;
                }
                pointerImage.setVisible(true);
                int rotation = MathUtils.round(mode.getAngleFromPlayer(car.getBody().getPosition()) / 90) * 90;
                setRotation(rotation);
                Vector2 position = new Vector2();
                float topY = Gdx.graphics.getHeight() - 10 - pointerImage.getHeight();
                float rightX = Gdx.graphics.getWidth() - 10 - pointerImage.getWidth();
                switch (rotation) {
                    case 0:
                    case 360:
                        position.x = 10;
                        position.y = Constants.H1 * 5 + car.getY() - mode.getPlayerCar().getY();
                        break;
                    case 180:
                        position.x = rightX;
                        position.y = Constants.H1 * 5 + car.getY() - mode.getPlayerCar().getY();
                        break;
                    case 90:
                        position.x = Constants.W1 * 5 + car.getX() - mode.getPlayerCar().getX();
                        position.y = 10;
                        break;
                    case 270:
                        position.x = Constants.W1 * 5 + car.getX() - mode.getPlayerCar().getX();
                        position.y = topY;
                        break;
                }
                if (position.x < 0) position.x = 10;
                else if (position.x > rightX) position.x = rightX;
                if (position.y < 0) position.y = 10;
                else if (position.y > topY) position.y = topY;
                pointerImage.setPosition(position.x, position.y);
            }
        };
        pointerImage.setSize(Constants.H1 * 0.5f, Constants.H1 * 0.5f);
        pointerImage.setOrigin(Align.center);

        horizontalGroup = new HorizontalGroup();
        horizontalGroup.setUserObject(this);

        Label winsLabel = new Label(String.valueOf(playerModel.wins), skin, "small", new Color(0xffc868ff));
        horizontalGroup.addActor(winsLabel);

        Image cupImage = new Image(skin.getRegion(Assets.CUP));
        horizontalGroup.addActor(new Container<>(cupImage).size(Constants.H1 * 0.7f));

        scoreLabel = new Label("", skin, "small", color);
        horizontalGroup.addActor(scoreLabel);

        updateLabel();
    }


    public PlayerModel getPlayerModel() {
        return playerModel;
    }

    public BaseCar getCar() {
        return car;
    }

    public void enableMarker() {
        Image image = new Image(MyGame.getSkin().newDrawable(Assets.CIRCLE, color));
        image.setPosition(car.getWidth() * 0.5f - image.getWidth() * 0.5f,
                -image.getHeight() * 1.5f);
        car.addActor(image);
    }

    public void enablePointer() {
        game.gameScreen.getMode().getUiStage().addActor(pointerImage);
    }

    public void disablePointer() {
        pointerImage.remove();
    }

    public void updateLabel() {
        scoreLabel.setText(playerModel.name + ": " + (int) score);
//        scoreLabel.addAction(Actions.sequence(
//                Actions.scaleTo(1.2f, 0.5f),
//                Actions.scaleTo(1, 0.5f)
//        ));
    }

    public Actor getScoreLabel() {
        return horizontalGroup;
    }
}
