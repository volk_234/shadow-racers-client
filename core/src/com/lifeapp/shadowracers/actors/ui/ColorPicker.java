package com.lifeapp.shadowracers.actors.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pools;
import com.lifeapp.shadowracers.variables.Assets;

public class ColorPicker extends WidgetGroup {
    Image bgImage;
    Image pickerImage;

    Pixmap bgPixmap;
    Pixmap texturePixmap;

    Color pickedColor;

    float prefWidth;
    float prefHeight;


    public ColorPicker(Skin skin, Texture texture) {
        prefWidth = texture.getWidth();
        prefHeight = texture.getHeight();

        bgImage = new Image(texture);
        bgImage.setSize(prefWidth, prefHeight);
        bgImage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (x > 0 && x < getWidth() && y > 0 && y < getHeight()) {
                    pickerImage.setPosition(x, y, Align.center);
                    updateColor();
                }
            }
        });
        addActor(bgImage);

        pickerImage = new Image(skin.newDrawable(Assets.ICON_BG, new Color(0xffffffaa)));
        pickerImage.setSize(prefHeight * 0.1f, prefHeight * 0.1f);
        addActor(pickerImage);

        texture.getTextureData().prepare();
        texturePixmap = texture.getTextureData().consumePixmap();

        setSize(prefWidth, prefHeight);
        setOrigin(Align.center);
        layout();

    }

    @Override
    public float getPrefWidth() {
        return prefWidth;
    }

    @Override
    public float getPrefHeight() {
        return prefHeight;
    }

    @Override
    public void layout() {
        super.layout();
        bgImage.setSize(getWidth(), getHeight());

        bgPixmap = new Pixmap((int) getWidth(), (int) getHeight(), Pixmap.Format.RGBA8888);
        bgPixmap.drawPixmap(texturePixmap, 0, 0,
                texturePixmap.getWidth(), texturePixmap.getHeight(),
                0, 0,
                bgPixmap.getWidth(), bgPixmap.getHeight());
        setOrigin(Align.center);
    }

    private void updateColor() {
        pickedColor = new Color(bgPixmap.getPixel((int) pickerImage.getX(Align.center),
                (int) (bgImage.getHeight() - pickerImage.getY(Align.center))));

        ChangeListener.ChangeEvent changeEvent = Pools.obtain(ChangeListener.ChangeEvent.class);
        fire(changeEvent);
        Pools.free(changeEvent);
    }

    public Color getPickedColor() {
        return pickedColor;
    }
}
