package com.lifeapp.shadowracers.actors.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class LevelTextButton extends IconTextButton {
    public LevelTextButton(Skin skin, int level, int stars) {
        super(skin);
        Label levelLabel = new Label(String.valueOf(level), skin, "big", Constants.COLOR_BLUE);
        iconGroup.addActor(levelLabel);
        levelLabel.setPosition(iconBgImage.getX(Align.center), iconBgImage.getY(Align.center), Align.center);

        StarsActor starsActor = new StarsActor(stars);
        iconGroup.addActor(starsActor);
        starsActor.setPosition(iconBgImage.getX(Align.center), iconBgImage.getHeight() * 0.1f, Align.center);
        starsActor.makeShowAnimation();
    }

    public LevelTextButton(Skin skin) {
        super(skin, skin.newDrawable(Assets.ICON_LOCK, Color.DARK_GRAY));
        iconBgImage.setColor(Constants.COLOR_GRAY);
    }
}
