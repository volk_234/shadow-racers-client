package com.lifeapp.shadowracers.actors.ui;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.IntAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class MoneyLabel extends WidgetGroup {
    Image coinImage;
    Label moneyLabel;
    int moneyValue;

    private Action changeAction;

    public MoneyLabel(int value) {
        Skin skin = MyGame.getSkin();
        moneyValue = value;

        coinImage = new Image(skin.getRegion(Assets.COIN));
        coinImage.setSize(getPrefHeight(), getPrefHeight());
        addActor(coinImage);

        moneyLabel = new Label(String.valueOf(moneyValue), skin);
        addActor(moneyLabel);

        setSize(getPrefWidth(), getPrefHeight());
        setOrigin(Align.center);
        layout();
    }

    @Override
    public float getPrefWidth() {
        return Constants.H1 * 2f;
    }

    @Override
    public float getPrefHeight() {
        return Constants.H1 * 1f;
    }

    @Override
    public void layout() {
        super.layout();
        coinImage.setSize(getHeight(), getHeight());
        moneyLabel.setHeight(getHeight());
        moneyLabel.setX(getHeight() * 1.2f);
        setOrigin(Align.center);
    }

    public void modify(int value) {
        int toValue = moneyValue + value;
        modifyTo(toValue);
    }

    public void modifyTo(int toValue) {
        removeAction(changeAction);
        IntAction intAction = new IntAction(moneyValue, toValue);
        intAction.setDuration(1.5f);
        changeAction = Actions.parallel(
                intAction,
                Actions.forever(
                        Actions.run(() -> {
                            moneyLabel.setText(String.valueOf(intAction.getValue()));
                            if (intAction.getValue() == toValue) {
                                removeAction(changeAction);
                            }
                        })
                )
        );
        addAction(changeAction);
        moneyValue = toValue;
    }

}
