package com.lifeapp.shadowracers.actors.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.ArrayList;

public class StarsActor extends WidgetGroup {
    //    Image bgImage;
    ArrayList<Image> offStars = new ArrayList<>();
    ArrayList<Image> onStars = new ArrayList<>();

    public StarsActor(int count) {
        Skin skin = MyGame.getSkin();

//        bgImage = new Image(skin.getRegion(Assets.STARS_BG));
//        bgImage.setSize(getPrefWidth(), getPrefHeight());
//        addActor(bgImage);

        for (int i = 0; i < 3; i++) {
            Image offStar = new Image(skin.getRegion(Assets.ICON_STAR_OFF));
            addActor(offStar);
            offStars.add(offStar);
        }
        for (int i = 0; i < count; i++) {
            Image onStar = new Image(skin.getRegion(Assets.ICON_STAR_ON));
            addActor(onStar);
            onStars.add(onStar);
        }
        setSize(getPrefWidth(), getPrefHeight());
        setOrigin(Align.center);
    }

    @Override
    public float getPrefWidth() {
        return Constants.H1 * 1.5f;
    }

    @Override
    public float getPrefHeight() {
        return Constants.H1 * 0.75f;
    }

    @Override
    public void layout() {
        super.layout();
        float pad = getHeight() * 0.1f;
        float starWidth = (getWidth() - pad * 2) / 3f;
        float starHeight = (getHeight() - pad * 2) / 1.5f;

        float sideStarOffset = starHeight * 0.5f;

        offStars.get(0).setBounds(pad + starWidth * 0, pad + sideStarOffset, starWidth, starHeight);
        offStars.get(1).setBounds(pad + starWidth * 1, pad, starWidth, starHeight);
        offStars.get(2).setBounds(pad + starWidth * 2, pad + sideStarOffset, starWidth, starHeight);

        if (onStars.size() > 0) {
            onStars.get(0).setBounds(pad + starWidth * 0, pad + sideStarOffset, starWidth, starHeight);
        }
        if (onStars.size() > 1) {
            onStars.get(1).setBounds(pad + starWidth * 1, pad, starWidth, starHeight);
        }
        if (onStars.size() > 2) {
            onStars.get(2).setBounds(pad + starWidth * 2, pad + sideStarOffset, starWidth, starHeight);
        }
//        bgImage.setSize(getWidth(), getHeight());
        for (int i = 0; i < onStars.size(); i++) {
            onStars.get(i).setOrigin(Align.center);
        }
        for (int i = 0; i < offStars.size(); i++) {
            offStars.get(i).setOrigin(Align.center);
        }
        setOrigin(Align.center);
    }

    public void makeShowAnimation() {
        for (int i = 0; i < onStars.size(); i++) {
            onStars.get(i).setScale(0);
            onStars.get(i).addAction(
                    Actions.scaleTo(1, 1, 0.5f, Interpolation.swingOut)
            );
        }

    }

    public void makeShakeAnimation() {
        for (int i = 0; i < onStars.size(); i++) {
            offStars.get(i).addAction(
                    Actions.forever(
                            Actions.sequence(
                                    Actions.delay(i * 0.5f),
                                    Actions.scaleTo(0.90f, 0.90f, 0.5f, Interpolation.swingOut),
                                    Actions.scaleTo(1f, 1f, 0.5f, Interpolation.swingOut)
                            )
                    )
            );
            onStars.get(i).addAction(
                    Actions.forever(
                            Actions.sequence(
                                    Actions.delay(i * 0.5f),
                                    Actions.scaleTo(0.90f, 0.90f, 0.5f, Interpolation.swingOut),
                                    Actions.scaleTo(1f, 1f, 0.5f, Interpolation.swingOut)
                            )
                    )
            );
        }
    }
}
