package com.lifeapp.shadowracers.actors.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class IconTextButton extends Table {
    final float iconSize = Constants.H1 * 1.5f;
    final Vector2 shadowOffset = new Vector2(iconSize * 0.05f, -iconSize * 0.05f);

    Group iconGroup;
    Image iconImage;
    Image iconBgImage;
    Image iconBgShadowImage;


    public IconTextButton(String text, Skin skin, Drawable image, Color iconBgColor) {
        this(text, skin, image);
        iconBgImage.setColor(iconBgColor);
    }

    public IconTextButton(String text, Skin skin, Drawable image) {
        this(skin, image);

        row().padTop(iconSize * 0.05f);
        Label textLabel = new Label(text, skin, "small", Color.WHITE);
        textLabel.setAlignment(Align.center);
        add(textLabel).center();

        setOrigin(getPrefWidth() * 0.5f, getPrefHeight() * 0.5f);
    }

    public IconTextButton(Skin skin, Drawable image) {
        this(skin);

        iconImage = new Image(image);
        float textureWidth = iconImage.getDrawable().getMinWidth();
        float textureHeight = iconImage.getDrawable().getMinHeight();
        float dimen = Math.max(textureWidth, textureHeight) / (iconSize * 0.5f);
        iconImage.setSize(textureWidth / dimen, textureHeight / dimen);
        iconImage.setPosition(iconBgImage.getX(Align.center), iconBgImage.getY(Align.center), Align.center);
        iconGroup.addActor(iconImage);
        setOrigin(getPrefWidth() * 0.5f, getPrefHeight() * 0.5f);
    }

    public IconTextButton(Skin skin) {
        iconGroup = new Group();
        add(iconGroup).size(iconSize).center();

        iconBgShadowImage = new Image(skin.newDrawable(Assets.ICON_BG, new Color(0xffffff33)));
        iconBgShadowImage.setSize(iconSize, iconSize);
        iconBgShadowImage.moveBy(shadowOffset.x, shadowOffset.y);
        iconGroup.addActor(iconBgShadowImage);

        iconBgImage = new Image(skin.getRegion(Assets.ICON_BG));
        iconBgImage.setSize(iconSize, iconSize);
        iconGroup.addActor(iconBgImage);
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                addAction(Actions.scaleTo(0.95f, 0.95f, 0.07f));
                iconBgShadowImage.addAction(Actions.moveTo(0f, 0f, 0.1f));
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                addAction(Actions.scaleTo(1, 1, 0.07f));
                iconBgShadowImage.addAction(Actions.moveTo(shadowOffset.x, shadowOffset.y, 0.1f));
            }
        });
        setOrigin(getPrefWidth() * 0.5f, getPrefHeight() * 0.5f);
        setTransform(true);
    }
}
