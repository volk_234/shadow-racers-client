package com.lifeapp.shadowracers.actors.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class RectangleTextButton extends WidgetGroup {
    final Vector2 shadowOffset = new Vector2(Constants.H1 * 0.1f, -Constants.H1 * 0.1f);

    private Image bgImage;
    private Image bgShadowImage;
    private Image iconImage;
    private Label textLabel;

    public RectangleTextButton(String text, Skin skin, Drawable image, Color bgColor, Color textColor) {
        super();
        bgShadowImage = new Image(skin.newDrawable(Assets.BUTTON_BG, new Color(0xffffff33)));
        bgShadowImage.moveBy(shadowOffset.x, shadowOffset.y);
        addActor(bgShadowImage);

        bgImage = new Image(skin.newDrawable(Assets.BUTTON_BG, bgColor));
        addActor(bgImage);

        iconImage = new Image(image);
        addActor(iconImage);

        textLabel = new Label(text, skin, "default", textColor);
        textLabel.setAlignment(Align.center);
        addActor(textLabel);

        setTransform(true);
        setOrigin(getPrefWidth() * 0.5f, getPrefHeight() * 0.5f);

        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                addAction(Actions.scaleTo(0.95f, 0.95f, 0.07f));
                bgShadowImage.addAction(Actions.moveTo(0f, 0f, 0.1f));
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                addAction(Actions.scaleTo(1, 1, 0.07f));
                bgShadowImage.addAction(Actions.moveTo(shadowOffset.x, shadowOffset.y, 0.1f));
            }
        });
        layout();

        bgImage.setSize(getPrefWidth(), getPrefHeight());
        bgShadowImage.setSize(getPrefWidth(), getPrefHeight());
    }

    @Override
    public float getPrefWidth() {
        return textLabel.getPrefWidth() + iconImage.getPrefWidth() + iconImage.getPrefWidth() * 0.5f;
    }

    @Override
    public float getPrefHeight() {
        return iconImage.getPrefHeight() * 1.3f;
    }

    @Override
    public void layout() {
        bgImage.setSize(getWidth(), getHeight());
        bgShadowImage.setSize(getWidth(), getHeight());
        float textureWidth = iconImage.getDrawable().getMinWidth();
        float textureHeight = iconImage.getDrawable().getMinHeight();
        float dimen = Math.max(textureWidth, textureHeight) / (getHeight() * 0.5f);
        iconImage.setSize(textureWidth / dimen, textureHeight / dimen);
        iconImage.setPosition(getWidth() * 0.125f, getHeight() * 0.5f, Align.center);
        textLabel.setPosition(getHeight() * 1.3f, getHeight() * 0.5f, Align.center | Align.left);
    }
}
