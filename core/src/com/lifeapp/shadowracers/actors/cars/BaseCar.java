package com.lifeapp.shadowracers.actors.cars;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.ParticleActor;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.utils.OptionsUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

import box2dLight.ChainLight;
import box2dLight.ConeLight;
import box2dLight.Light;
import box2dLight.RayHandler;


/**
 * Created by Александр on 11.12.2017.
 */

public abstract class BaseCar extends Group implements Disposable {

    public class Wheel extends Image {
        private Body body;
        private float maxForwardSpeed;
        private float maxBackwardSpeed;
        private float maxDriveForce;
        private float maxLateralImpulse;
        private float currentTraction = 1;
        private float desiredSpeed, currentSpeed;
        private boolean drift = false;

        Wheel(Body body) {
            super(MyGame.getTextureRegion(Assets.ATLAS, Assets.CAR_WHEEL));
            this.body = body;
        }

        void setCharacteristics(float maxForwardSpeed, float maxBackwardSpeed, float maxDriveForce, float maxLateralImpulse) {
            this.maxForwardSpeed = maxForwardSpeed;
            this.maxBackwardSpeed = maxBackwardSpeed;
            this.maxDriveForce = maxDriveForce;
            this.maxLateralImpulse = maxLateralImpulse;
        }

        void setCurrentTraction(float value) {
            currentTraction = value;
        }

        private Vector2 getLateralVelocity() {
            Vector2 rightNormal = body.getWorldVector(new Vector2(1, 0));
            return rightNormal.scl(rightNormal.dot(body.getLinearVelocity()));
        }

        private Vector2 getForwardVelocity() {
            Vector2 forwardNormal = body.getWorldVector(new Vector2(0, 1));
            return forwardNormal.scl(forwardNormal.dot(body.getLinearVelocity()));
        }

        void updateFriction() {

            Vector2 lateralVelocity = getLateralVelocity().scl(-body.getMass());
            float maxLateralImpulse = this.maxLateralImpulse * currentTraction - currentSpeed * 0.01f;
            if (lateralVelocity.len() > maxLateralImpulse && currentSpeed > 0) {
                lateralVelocity.scl(maxLateralImpulse / lateralVelocity.len());
                drift = true;
            } else {
                drift = false;
            }
            body.applyLinearImpulse(lateralVelocity, body.getWorldCenter(), true);

//            body.applyAngularImpulse(-body.getAngularVelocity() * currentTraction
//                    * body.getInertia() * 0.1f, true);

            Vector2 forwardVelocity = getForwardVelocity();
            float forwardSpeed = forwardVelocity.len();
            float dragForceMagnitude = forwardSpeed * 0.01f;
            forwardVelocity.nor();
            body.applyForce(forwardVelocity.scl(currentTraction * -forwardSpeed * dragForceMagnitude),
                    body.getWorldCenter(), true);
        }

        void updateDrive(int controlState) {
            switch (controlState) {
                case -1:
                    desiredSpeed = maxBackwardSpeed;
                    break;
                case 1:
                    desiredSpeed = maxForwardSpeed;
                    break;
                default:
                    return;
            }

            Vector2 forwardVector = body.getWorldVector(new Vector2(0, 1)).cpy();
            currentSpeed = getForwardVelocity().dot(forwardVector);
            float force;
            if (desiredSpeed > currentSpeed) {
                force = maxDriveForce;
            } else if (desiredSpeed < currentSpeed) {
                force = -maxDriveForce;
            } else {
                return;
            }
            body.applyForce(forwardVector.scl(force), body.getWorldCenter(), true);
        }

        @Override
        public void act(float delta) {
            super.act(delta);
//            setRotation((float) (getParent().getRotation()
//                    + Math.toDegrees(body.getAngle())));
//            body.setTransform(body.getPosition(),
//                    (float) Math.toRadians((getParent().getRotation() + getRotation())));
        }

        public Body getBody() {
            return body;
        }

//        @Override
//        public void draw(Batch batch, float parentAlpha) {
//            batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(),
//                    getScaleX(), getScaleY(), getRotation());
//        }

        boolean isDrift() {
            return drift;
        }
    }

    public class CarInfo {
        public String textureName;
        public Vector2[] bodyVertices;
        public Vector2[] wheelPositions;
        public Vector2[] lightPositions;
        public float[][] backLightChains;
        public float[][] neonChains;
        public float lightDistance;
        public float carWidth;
        public float carHeight;
        public float turnMaxAngle;
        public float turnStep;
        public float maxForwardSpeed;
        public float maxBackwardSpeed;
        public float backTireMaxDriveForce;
        public float frontTireMaxDriveForce;
        public float backTireMaxLateralImpulse;
        public float frontTireMaxLateralImpulse;
        public int price;
        public int priceColor;
        public int priceLights;
        public int priceNeons;
    }

    CarInfo carInfo;
    private Mode mode;

    private Image carImage;
    private Body body;

    private Vector2 control = new Vector2();
    private Array<Wheel> wheels = new Array<>();

    private float turnAngle;

    private int driftScore = 0;
    private float timeBeforeDriftEnd = 0;
    private Label driftLabel;

    private long soundEngineId, soundDriftId;

    private Array<Light> lights = new Array<>();
    private Array<Light> backLights = new Array<>();
    private Array<Light> neons = new Array<>();

    private RayHandler rayHandler;
    private ParticleEffectPool crashParticlePool;
    private ParticleEffect burnOutEffect;

    private RevoluteJoint leftJoint, rightJoint;

    private float volume;

    BaseCar(World world, RayHandler rayHandler, Vector2 position, float angle) {
        this.rayHandler = rayHandler;
        getCarInfo();
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(position);
        bodyDef.angle = (float) Math.toRadians(angle);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.linearDamping = 0.5f;
        body = world.createBody(bodyDef);
        body.setUserData(this);
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(getCarInfo().bodyVertices);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.restitution = 0.1f;
        fixtureDef.density = 1;
        fixtureDef.filter.categoryBits = CategoryBits.CAR;
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(CategoryBits.CAR);
        polygonShape.dispose();


        for (int i = 0; i < getCarInfo().wheelPositions.length; i++) {
            bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.DynamicBody;
            bodyDef.position.set(position);
//            bodyDef.angularDamping = 1;
//            bodyDef.linearDamping = 1;
            bodyDef.angle = (float) Math.toRadians(angle);
            Body wheel = world.createBody(bodyDef);

            polygonShape = new PolygonShape();
            polygonShape.setAsBox(0.25f, 0.4f);
            fixtureDef = new FixtureDef();
            fixtureDef.density = 1;
            fixtureDef.isSensor = true;
            fixtureDef.shape = polygonShape;
//            fixtureDef.filter.maskBits = 0;
            wheel.createFixture(fixtureDef);
            polygonShape.dispose();

            RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
            revoluteJointDef.bodyA = body;
            revoluteJointDef.localAnchorB.setZero();
            revoluteJointDef.bodyB = wheel;
            revoluteJointDef.localAnchorA.set(carInfo.wheelPositions[i]);
            revoluteJointDef.enableLimit = true;
            revoluteJointDef.lowerAngle = 0;
            revoluteJointDef.upperAngle = 0;
            revoluteJointDef.collideConnected = false;
            RevoluteJoint joint = (RevoluteJoint) world.createJoint(revoluteJointDef);
            if (i == 0) {
                leftJoint = joint;
            } else if (i == 1) {
                rightJoint = joint;
            }

            Wheel wheel1 = new Wheel(wheel);
            wheel1.setCharacteristics(carInfo.maxForwardSpeed, carInfo.maxBackwardSpeed,
                    i < 2 ? carInfo.frontTireMaxDriveForce : carInfo.backTireMaxDriveForce,
                    i < 2 ? carInfo.frontTireMaxLateralImpulse : carInfo.backTireMaxLateralImpulse);
            addActor(wheel1);

            wheel1.setPosition((carInfo.wheelPositions[i].x + carInfo.carWidth * 0.5f - 0.25f) * Constants.PPT,
                    (carInfo.wheelPositions[i].y + carInfo.carHeight * 0.5f - 0.4f) * Constants.PPT);
            wheel1.setSize(0.5f * Constants.PPT, 0.8f * Constants.PPT);
            wheel1.setOrigin(Align.center);
            wheels.add(wheel1);
        }

        setSize(carInfo.carWidth * Constants.PPT, carInfo.carHeight * Constants.PPT);
        setOrigin(Align.center);
        Array<TextureAtlas.AtlasRegion> textures = MyGame.getAtlas(Assets.ATLAS).findRegions(carInfo.textureName);
        for (int i = 0; i < textures.size; i++) {
            Image image = new Image(textures.get(i));
            if (i == 0) {
                carImage = image;
            }
            image.setSize(getWidth(), getHeight());
            image.setAlign(Align.center);
            image.setPosition(0, 0);
            addActor(image);
        }

        updatePosition();
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);
        this.mode = (Mode) stage;
    }

    private void updatePosition() {
        setPosition(Constants.PPT * body.getPosition().x - getWidth() * 0.5f,
                Constants.PPT * body.getPosition().y - getHeight() * 0.5f);
        setRotation((float) Math.toDegrees(body.getAngle()));
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        Vector2 forwardVector = body.getWorldVector(new Vector2(0, 1)).cpy();
        float relativeSpeed = Math.abs(getForwardVelocity().dot(forwardVector) / (getCarInfo().maxForwardSpeed));
        volume = 1 - MathUtils.clamp(mode.getDstFromPlayer(body.getPosition()) / 32, 0, 1);


        updatePosition();

        if (burnOutEffect != null) {
            Vector2 burnoutPos = localToStageCoordinates(new Vector2(getWidth() * 0.5f, 0));
            burnOutEffect.setPosition(burnoutPos.x, burnoutPos.y);
            burnOutEffect.update(delta);
        }

        turnAngle = -control.x * getCarInfo().turnMaxAngle;
        float angleNow = leftJoint.getJointAngle();
        float angleToTurn = turnAngle - angleNow;
        angleToTurn = MathUtils.clamp(angleToTurn, -getCarInfo().turnStep, getCarInfo().turnStep);
        float newAngle = angleNow + angleToTurn;
        leftJoint.setLimits(newAngle, newAngle);
        rightJoint.setLimits(newAngle, newAngle);
        wheels.get(0).setRotation((float) Math.toDegrees(newAngle));
        wheels.get(1).setRotation((float) Math.toDegrees(newAngle));

        for (int i = 0; i < wheels.size; i++) {
            wheels.get(i).updateFriction();
            wheels.get(i).updateDrive((int) control.y);
        }

        if (burnOutEffect != null) {
            float driftValue = 0;
            if (wheels.get(2).isDrift() || wheels.get(3).isDrift()) {
                for (Wheel wheel : wheels) {
                    wheel.setCurrentTraction(relativeSpeed + 0.33f);
                }
                driftValue = 10 * relativeSpeed;
                if (burnOutEffect.isComplete()) {
                    burnOutEffect.start();
                }
                if (soundDriftId == -1) {
                    soundDriftId = SoundUtils.loop(Assets.CAR_DRIFT, 0.4f * volume);
                }

            } else {
                burnOutEffect.allowCompletion();
                if (soundDriftId > 0) {
                    SoundUtils.stop(Assets.CAR_DRIFT, soundDriftId);
                    soundDriftId = -1;
                }
            }
            if (driftLabel != null) {
                if (driftValue > 0) {
                    timeBeforeDriftEnd = 0.5f;
                    driftScore += driftValue;
                    if (driftScore > Constants.DriftThredshold) {
                        driftLabel.setText(String.valueOf(driftScore));
                        if (!driftLabel.isVisible()) {
                            startDrift();
                        }
                    }
                } else {
                    timeBeforeDriftEnd -= delta;
                    if (timeBeforeDriftEnd <= 0) {
                        if (driftScore >= Constants.DriftThredshold) {
                            successDrift();
                        } else {
                            driftScore = 0;
                        }
                    }
                }
            }
        }

        updateLoops(relativeSpeed * volume + 0.25f, 0.5f + relativeSpeed / 2);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = batch.getColor();
        if (burnOutEffect != null)
            burnOutEffect.draw(batch);
        batch.setColor(color);
        super.draw(batch, parentAlpha);
    }

    @Override
    public void dispose() {
        SoundUtils.stop(Assets.CAR_ENGINE, soundEngineId);
        SoundUtils.stop(Assets.CAR_DRIFT, soundDriftId);
        while (lights.size > 0) {
            lights.pop().remove();
        }
        while (neons.size > 0) {
            neons.pop().remove();
        }
        while (backLights.size > 0) {
            backLights.pop().remove();
        }
        mode.deleteBody(body);
        for (Wheel wheel : wheels) {
            mode.deleteBody(wheel.body);
        }
        this.remove();
    }

    public void initLights() {
        for (Vector2 LIGHT_POSITION : carInfo.lightPositions) {
            ConeLight light = new ConeLight(rayHandler, Constants.RAYS,
                    new Color(0xffffffaa),
                    carInfo.lightDistance,
                    0, 0, 90, 50);
            light.attachToBody(body, LIGHT_POSITION.x, LIGHT_POSITION.y, 90);
            light.setContactFilter(CategoryBits.DEFAULT, CategoryBits.NONE,
                    (short) (CategoryBits.OBJECT | CategoryBits.CAR | CategoryBits.NPC_CAR));
            lights.add(light);
        }

        for (float[] backLightChain : carInfo.backLightChains) {
            ChainLight chainLight = new ChainLight(rayHandler, Constants.RAYS,
                    new Color(0xff0000ff), 0.4f, -1, backLightChain);
            chainLight.attachToBody(getBody());
            backLights.add(chainLight);
        }
    }

    public void initEffects() {

        ParticleEffect particleEffect = new ParticleEffect();
        particleEffect.load(Gdx.files.internal(Assets.PARTICLE_CRASH), MyGame.getAtlas(Assets.ATLAS));
        crashParticlePool = new ParticleEffectPool(particleEffect, 2, 2);

        burnOutEffect = new ParticleEffect();
        burnOutEffect.load(Gdx.files.internal(Assets.PARTICLE_BURNOUT), MyGame.getAtlas(Assets.ATLAS));
        burnOutEffect.scaleEffect(Constants.PPT);

        addAction(Actions.forever(Actions.run(() -> {
            for (int i = 0; i < wheels.size; i++) {
                Wheel wheel = wheels.get(i);
                if (wheel.isDrift()) {
                    Image sprite = new Image(MyGame.getTextureRegion(Assets.ATLAS, Assets.BURNOUT));
                    getStage().addActor(sprite);
                    Vector2 coords = localToStageCoordinates(new Vector2(wheel.getX(Align.center),
                            wheel.getY(Align.center)));
                    sprite.setPosition(coords.x, coords.y, Align.center);
                    sprite.addAction(Actions.delay(3, Actions.removeActor()));
                    sprite.setRotation(getParent().getRotation());
                    sprite.toBack();
                }
            }
        })));
    }

    public void initSounds() {
        soundDriftId = -1;
        soundEngineId = SoundUtils.loop(Assets.CAR_ENGINE, 1, 0.5f, 1);
    }

    public void controlByPlayer() {
        mode.setPlayerCar(this);
        body.getFixtureList().first().setUserData(CategoryBits.PLAYER);

        driftLabel = new Label("0", MyGame.getSkin(), "big", Color.WHITE);
        driftLabel.setWidth(Constants.W1 * 0.3f);
        driftLabel.setPosition((getWidth() - driftLabel.getWidth()) * 0.5f, -driftLabel.getHeight());
        driftLabel.setAlignment(Align.center);
        driftLabel.setVisible(false);
        addActor(driftLabel);
    }

    private void startDrift() {
        timeBeforeDriftEnd = 0.5f;
        mode.startDrift();
        driftLabel.setVisible(true);
    }

    public void successDrift() {
        mode.successDrift(driftScore);
        driftScore = 0;
        driftLabel.addAction(Actions.sequence(
                Actions.color(Color.GREEN, 0.5f, Interpolation.bounce),
                Actions.moveBy(0, 200, 0.2f, Interpolation.bounce),
                Actions.color(Color.WHITE),
                Actions.moveBy(0, -200),
                Actions.visible(false)
        ));
    }

    private void failDrift() {
        mode.failDrift();
        driftScore = 0;
        driftLabel.addAction(Actions.sequence(
                Actions.color(Color.RED, 0.5f, Interpolation.bounce),
                Actions.moveBy(0, -200, 0.2f, Interpolation.bounce),
                Actions.color(Color.WHITE),
                Actions.moveBy(0, 200),
                Actions.visible(false)
        ));

    }

    public Vector2 getForwardVelocity() {
        Vector2 forwardNormal = body.getWorldVector(new Vector2(0, 1)).cpy();
        return forwardNormal.scl(forwardNormal.dot(body.getLinearVelocity()));
    }

    public Body getBody() {
        return body;
    }

    public Array<Wheel> getWheels() {
        return wheels;
    }

    public void setControl(Vector2 control) {
        setControl(control.x, control.y);
    }

    public void setControl(float x, float y) {
        if (y != control.y) {
            float backLightsDst = y < 0 ? 1.5f : 0.5f;
            for (Light backLight : backLights) {
                backLight.setDistance(backLightsDst);
            }
        }
        control.x = x;
        control.y = y;
    }

    public Vector2 getControl() {
        return control;
    }

    public void collision(Contact contact) {
        float strength = contact.getFixtureA().getBody().getLinearVelocity().
                sub(contact.getFixtureB().getBody().getLinearVelocity()).len();

        if (mode.getPlayerCar() == this) {
            if (OptionsUtils.isVibrationEnabled()) {
                Gdx.input.vibrate((int) strength);
            }
            mode.collision(contact);
        }

        if (driftLabel != null && driftScore > 0) {
            failDrift();
        }

        if (strength > 10)
            SoundUtils.play(Assets.CAR_CRASH, volume * strength / 30f);
        else if (strength > 4)
            SoundUtils.play(Assets.CAR_CRASH_SMALL, volume * strength / 30f);

        ParticleEffect particleEffect = crashParticlePool.obtain();
        ParticleActor particleActor = new ParticleActor(particleEffect);
        Vector2 particlePos =
                contact.getWorldManifold().getPoints()[0].scl(Constants.PPT);
        mode.addActor(particleActor);
        particleActor.setPosition(particlePos.x, particlePos.y);
        particleActor.addAction(Actions.delay(0.3f, Actions.removeActor()));
    }

    public void changeCarColor(Color color) {
        if (color == null)
            return;
        carImage.setColor(color);
    }

    public void changeLightsColor(Color color) {
        if (color == null)
            return;
        for (Light light : lights) {
            light.setColor(color.r, color.g, color.b, 0.7f);
        }
    }

    public void changeNeonsColor(Color color) {
        if (color == null) {
            while (neons.size > 0) {
                neons.pop().remove();
            }
        } else if (neons.size == 0) {
            ChainLight chainLight = new ChainLight(rayHandler, Constants.RAYS,
                    color, 0.8f, 1, carInfo.neonChains[0]);
            chainLight.attachToBody(getBody());
            neons.add(chainLight);

            chainLight = new ChainLight(rayHandler, Constants.RAYS,
                    color, 0.8f, -1, carInfo.neonChains[1]);
            chainLight.attachToBody(getBody());
            neons.add(chainLight);
        } else {
            for (Light neon : neons) {
                neon.setColor(color);
            }
        }
    }

    public abstract CarInfo getCarInfo();

    public void updateLoops(float engineVolume, float enginePitch) {
        SoundUtils.setPitch(Assets.CAR_ENGINE, soundEngineId, enginePitch);
        SoundUtils.setVolume(Assets.CAR_ENGINE, soundEngineId, engineVolume);

        SoundUtils.setPitch(Assets.CAR_DRIFT, soundDriftId, enginePitch);
        SoundUtils.setVolume(Assets.CAR_DRIFT, soundDriftId, engineVolume);
    }

}
