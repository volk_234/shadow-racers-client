package com.lifeapp.shadowracers.actors.cars;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.lifeapp.shadowracers.variables.Assets;

import box2dLight.RayHandler;


/**
 * Created by Александр on 11.12.2017.
 */

public class TruckCar extends BaseCar {

    public TruckCar(World world, RayHandler rayHandler, Vector2 position, float angle) {
        super(world, rayHandler, position, angle);
    }

    @Override
    public CarInfo getCarInfo() {
        if (carInfo == null) {
            carInfo = new CarInfo();
            carInfo.backLightChains = new float[][]{
                    {-0.8f, -1.8f, -0.5f, -2f, -0.2f, -1.8f},
                    {0.2f, -1.8f, 0.5f, -2f, 0.8f, -1.8f},
            };
            carInfo.neonChains = new float[][]{
                    {0.8f, 0.8f, 0.8f, -1},
                    {-0.8f, 0.8f, -0.8f, -1},
            };
            carInfo.bodyVertices = new Vector2[]{
                    new Vector2(-1f, 1.3f),
                    new Vector2(-0.5f, 2f),
                    new Vector2(0.5f, 2f),
                    new Vector2(1f, 1.3f),
                    new Vector2(1f, -1.3f),
                    new Vector2(0.6f, -2f),
                    new Vector2(-0.6f, -2f),
                    new Vector2(-1f, -1.3f)
            };
            carInfo.wheelPositions = new Vector2[]{
                    new Vector2(-0.8f, 1.15f),
                    new Vector2(0.8f, 1.15f),
                    new Vector2(0.8f, -1.15f),
                    new Vector2(-0.8f, -1.15f)
            };
            carInfo.lightPositions = new Vector2[]{
                    new Vector2(0.75f, 1.55f),
                    new Vector2(-0.75f, 1.55f),
            };
            carInfo.textureName = Assets.CAR_TRUCK;
            carInfo.carWidth = 2;
            carInfo.carHeight = 4;
            carInfo.lightDistance = 15;
            carInfo.turnMaxAngle = 42;
            carInfo.turnStep = 2f;
            carInfo.maxForwardSpeed = 40;
            carInfo.maxBackwardSpeed = -10;
            carInfo.backTireMaxDriveForce = 50;
            carInfo.frontTireMaxDriveForce = 20;
            carInfo.backTireMaxLateralImpulse = 1.5f;
            carInfo.frontTireMaxLateralImpulse = 1.5f;
            carInfo.price = 1000;
            carInfo.priceColor = 1;
            carInfo.priceLights = 1;
            carInfo.priceNeons = 1;
        }
        return carInfo;
    }
}
