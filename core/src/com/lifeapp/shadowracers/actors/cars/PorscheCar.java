package com.lifeapp.shadowracers.actors.cars;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.lifeapp.shadowracers.variables.Assets;

import box2dLight.RayHandler;


/**
 * Created by Александр on 11.12.2017.
 */

public class PorscheCar extends BaseCar {

    public PorscheCar(World world, RayHandler rayHandler, Vector2 position, float angle) {
        super(world, rayHandler, position, angle);
    }

    @Override
    public CarInfo getCarInfo() {
        if (carInfo == null) {
            carInfo = new CarInfo();
            carInfo.backLightChains = new float[][]{
                    {-0.8f, -1.8f, -0.5f, -2f, -0.2f, -1.8f},
                    {0.2f, -1.8f, 0.5f, -2f, 0.8f, -1.8f},
            };
            carInfo.neonChains = new float[][]{
                    {0.8f, 0.8f, 0.8f, -1},
                    {-0.8f, 0.8f, -0.8f, -1},
            };
            carInfo.bodyVertices = new Vector2[]{
                    new Vector2(-1f, 1.3f),
                    new Vector2(-0.5f, 2f),
                    new Vector2(0.5f, 2f),
                    new Vector2(1f, 1.3f),
                    new Vector2(1f, -1.3f),
                    new Vector2(0.6f, -2f),
                    new Vector2(-0.6f, -2f),
                    new Vector2(-1f, -1.3f)
            };
            carInfo.wheelPositions = new Vector2[]{
                    new Vector2(-0.8f, 1.15f),
                    new Vector2(0.8f, 1.15f),
                    new Vector2(0.8f, -1.15f),
                    new Vector2(-0.8f, -1.15f)
            };
            carInfo.lightPositions = new Vector2[]{
                    new Vector2(0.75f, 1.55f),
                    new Vector2(-0.75f, 1.55f),
            };
            carInfo.textureName = Assets.CAR_PORSCHE;
            carInfo.carWidth = 2;
            carInfo.carHeight = 4;
            carInfo.lightDistance = 15;
            carInfo.turnMaxAngle = (float) Math.toRadians(42f);
            carInfo.turnStep = (float) Math.toRadians(5f);
            carInfo.maxForwardSpeed = 33;
            carInfo.maxBackwardSpeed = -13;
            carInfo.backTireMaxDriveForce = 30;
            carInfo.frontTireMaxDriveForce = 45;
            carInfo.backTireMaxLateralImpulse = 1.5f;
            carInfo.frontTireMaxLateralImpulse = 1.9f;
            carInfo.price = 10000;
            carInfo.priceColor = 800;
            carInfo.priceLights = 2000;
            carInfo.priceNeons = 5000;
        }
        return carInfo;
    }
}
