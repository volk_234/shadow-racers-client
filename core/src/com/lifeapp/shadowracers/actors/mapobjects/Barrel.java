package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 19.12.2017.
 */

public class Barrel extends com.lifeapp.shadowracers.actors.mapobjects.BaseDynamicObject {
    private final float SIZE = 1f;

    public Barrel(World world, Vector2 position, float angle) {
        super(world, position, angle);
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(SIZE * 0.5f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.density = 7;
        fixtureDef.restitution = 0.2f;
        body.createFixture(fixtureDef).setUserData(CategoryBits.OBJECT);
        circleShape.dispose();
        textureRegion = MyGame.getTextureRegion(Assets.ATLAS, Assets.BARREL);
        setSize(SIZE * Constants.PPT, SIZE * Constants.PPT);
        setOrigin(Align.center);
    }
}
