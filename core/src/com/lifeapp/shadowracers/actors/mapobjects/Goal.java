package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.ParticleActor;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

import box2dLight.PointLight;
import box2dLight.RayHandler;

/**
 * Created by Александр on 19.12.2017.
 */

public class Goal extends BaseObject {
    private final float SIZE = 5;
    private PointLight light;
    private Image arrowImage;


    public Goal(World world, Vector2 position, float angle, RayHandler rayHandler) {
        super(world, position, angle);

        body.setType(BodyDef.BodyType.StaticBody);
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(SIZE * 0.5f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.isSensor = true;
        fixtureDef.filter.categoryBits = CategoryBits.GOAL;
        fixtureDef.filter.maskBits = CategoryBits.CAR;
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(CategoryBits.GOAL);
        circleShape.dispose();
        textureRegion = MyGame.getTextureRegion(Assets.ATLAS, Assets.GOAL);
        setSize(SIZE * Constants.PPT, SIZE * Constants.PPT);
        updatePosition();
        addAction(Actions.forever(
                Actions.sequence(
                        Actions.alpha(0.3f, 0.5f),
                        Actions.alpha(1, 0.2f)
                )
        ));


        light = new PointLight(rayHandler, Constants.RAYS,
                new Color(0x0000ffff), 6, 0, 0);
        light.attachToBody(body, 0, 0, 0);
        light.setIgnoreAttachedBody(true);
        light.setXray(true);
    }

    public void makeArrow(float rotation) {
        arrowImage = new Image(MyGame.getSkin().newDrawable(Assets.ARROW, Color.BLACK));
        arrowImage.setSize(getWidth() * 0.35f, getHeight() * 0.35f);
        arrowImage.setPosition(getX(Align.center) - arrowImage.getWidth() * 0.5f,
                getY(Align.center) - arrowImage.getHeight() * 0.5f);
        arrowImage.setRotation(rotation);
        arrowImage.setOrigin(Align.center);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (arrowImage != null) {
            arrowImage.draw(batch, parentAlpha);
        }
    }

    @Override
    public void dispose() {
        light.remove(true);
        super.dispose();
    }

    public void showParticle() {
        ParticleEffect particleEffect = new ParticleEffect();
        particleEffect.load(Gdx.files.internal(Assets.PARTICLE_GOAL_REACHED), MyGame.getAtlas(Assets.ATLAS));
        ParticleActor particleActor = new ParticleActor(particleEffect);
        particleActor.setPosition(getX(Align.center), getY(Align.center));
        getStage().addActor(particleActor);
        particleActor.addAction(Actions.delay(1.5f, Actions.removeActor()));
    }
}
