package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.FloatAction;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

import box2dLight.ConeLight;
import box2dLight.RayHandler;

/**
 * Created by Александр on 19.12.2017.
 */

public class BuildingLight extends BaseDynamicObject {
    private final float SIZE = 0.6f;
    private ConeLight light;
    private FloatAction brightnessAction;

    public BuildingLight(World world, Vector2 position, float angle, RayHandler rayHandler) {
        super(world, position, angle);
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(SIZE * 0.5f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.density = 7;
        fixtureDef.restitution = 0.2f;

        body.createFixture(fixtureDef).setUserData(CategoryBits.OBJECT);
        circleShape.dispose();
        textureRegion = MyGame.getTextureRegion(Assets.ATLAS, Assets.BUILDING_LIGHT);
        setSize(SIZE * Constants.PPT, SIZE * Constants.PPT);
        setOrigin(Align.center);

        brightnessAction = new FloatAction(8, 10);
        brightnessAction.setDuration(0.5f);
        addAction(Actions.forever(
                Actions.sequence(brightnessAction,
                        Actions.run(() -> brightnessAction.setReverse(!brightnessAction.isReverse()))
                ))
        );

        light = new ConeLight(rayHandler, Constants.RAYS,
                new Color(0xD9D6FAcc), 10, 0, 0, 0, 30);
        light.attachToBody(body, -0.1f, 0, 0);
        light.setIgnoreAttachedBody(true);
//        light.setContactFilter(CategoryBits.DEFAULT, CategoryBits.NONE, CategoryBits.DEFAULT);

        updatePosition();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        light.setDistance(brightnessAction.getValue());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }
}
