package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

import box2dLight.ChainLight;
import box2dLight.ConeLight;
import box2dLight.Light;
import box2dLight.RayHandler;

/**
 * Created by Александр on 19.12.2017.
 */

public class NpcCar extends BaseDynamicObject {
    private final float WIDTH = 2f;
    private final float HEIGHT = 4f;
    private Array<Light> lights = new Array<>();
    private boolean panic = false;

    public NpcCar(World world, Vector2 position, float angle, RayHandler rayHandler) {
        super(world, position, angle);
        Vector2[] lightPositions = new Vector2[]{
                new Vector2(0.75f, 1.55f),
                new Vector2(-0.75f, 1.55f),
        };

        Vector2[] bodyVertices = new Vector2[]{
                new Vector2(-1f, 1.3f),
                new Vector2(-0.5f, 2f),
                new Vector2(0.5f, 2f),
                new Vector2(1f, 1.3f),
                new Vector2(1f, -1.3f),
                new Vector2(0.6f, -2f),
                new Vector2(-0.6f, -2f),
                new Vector2(-1f, -1.3f)
        };
        float[][] backLightChains = new float[][]{
                {-0.8f, -1.8f, -0.5f, -2f, -0.2f, -1.8f},
                {0.2f, -1.8f, 0.5f, -2f, 0.8f, -1.8f},
        };

        body.setUserData(this);
        body.setLinearDamping(5);
        body.setAngularDamping(5);
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(bodyVertices);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.restitution = 0.5f;
        fixtureDef.density = 1.5f;
        fixtureDef.filter.categoryBits = CategoryBits.CAR;
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(CategoryBits.NPC_CAR);
        polygonShape.dispose();

        for (Vector2 LIGHT_POSITION : lightPositions) {
            ConeLight light = new ConeLight(rayHandler, Constants.RAYS,
                    new Color(0xffffffaa),
                    3,
                    0, 0, 90, 50);
            light.attachToBody(body, LIGHT_POSITION.x, LIGHT_POSITION.y, 90);
            light.setContactFilter(CategoryBits.DEFAULT, CategoryBits.NONE, (short) (CategoryBits.DEFAULT | CategoryBits.CAR));
            lights.add(light);
        }

        for (float[] backLightChain : backLightChains) {
            ChainLight chainLight = new ChainLight(rayHandler, Constants.RAYS,
                    new Color(0xff0000ff), 0.4f, -1, backLightChain);
            chainLight.attachToBody(body);
            lights.add(chainLight);
        }

        setSize(WIDTH * Constants.PPT, HEIGHT * Constants.PPT);
        setOrigin(Align.center);

        Array<TextureAtlas.AtlasRegion> textures = MyGame.getAtlas(Assets.ATLAS).findRegions(Assets.CAR_BMW);
        textureRegion = textures.get(0);
        setColor(
                MathUtils.random(0.3f, 1f),
                MathUtils.random(0.3f, 1f),
                MathUtils.random(0.3f, 1f),
                1
        );

        for (Light light : lights) {
            light.setActive(false);
        }

    }

    public void panic(Contact contact) {
        if (panic) {
            return;
        }
        panic = true;
        addAction(Actions.sequence(
                Actions.repeat(10, Actions.delay(1,
                        Actions.run(() -> {
                            Gdx.app.log("time", String.valueOf(TimeUtils.millis()));
                            for (Light light : lights) {
                                light.setActive(!light.isActive());
                            }
                        }))),
                Actions.run(() -> panic = false))
        );
    }
}
