package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 19.12.2017.
 */

public class CargoContainer extends BaseDynamicObject {
    private final float WIDTH = 4f;
    private final float HEIGHT = 5f;

    public CargoContainer(World world, Vector2 position, float angle) {
        super(world, position, angle);
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(WIDTH * 0.5f, HEIGHT * 0.5f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = 11;
        fixtureDef.restitution = 0f;
        body.createFixture(fixtureDef).setUserData(CategoryBits.OBJECT);
        body.setLinearDamping(5);
        body.setAngularDamping(5);
        polygonShape.dispose();
        textureRegion = MyGame.getTextureRegion(Assets.ATLAS, Assets.CARGO_CONTAINER);
        setSize(WIDTH * Constants.PPT, HEIGHT * Constants.PPT);
        setOrigin(Align.center);

        setColor(MathUtils.randomBoolean() ? Constants.COLOR_BLUE : Constants.COLOR_RED);
    }
}
