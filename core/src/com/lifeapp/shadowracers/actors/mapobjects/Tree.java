package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 19.12.2017.
 */

public class Tree extends BaseObject {
    private final float SIZE = 0.75f;

    public Tree(World world, Vector2 position, float angle) {
        super(world, position, angle);
        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(SIZE * 0.5f);
        fixtureDef.shape = circleShape;
        fixtureDef.filter.categoryBits = CategoryBits.OBJECT;
        body.createFixture(fixtureDef).setUserData(CategoryBits.OBJECT);
        circleShape.dispose();
        Array<TextureAtlas.AtlasRegion> regions = MyGame.getAtlas(Assets.ATLAS).findRegions("tree");
        textureRegion = regions.get(MathUtils.random(regions.size - 1));
        float scl = MathUtils.random(4);
        setSize(SIZE * Constants.PPT * (4 + scl), SIZE * Constants.PPT * (4 + scl));
        setRotation(MathUtils.random(0, 360));
        updatePosition();
        toFront();
    }
}
