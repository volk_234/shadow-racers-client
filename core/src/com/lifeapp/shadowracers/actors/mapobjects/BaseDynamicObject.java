package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Александр on 19.12.2017.
 */

public class BaseDynamicObject extends BaseObject {

    public BaseDynamicObject(World world, Vector2 position, float angle) {
        super(world, position, angle);
        body.setType(BodyDef.BodyType.DynamicBody);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        updatePosition();
    }
}
