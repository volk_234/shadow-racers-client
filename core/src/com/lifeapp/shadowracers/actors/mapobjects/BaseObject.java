package com.lifeapp.shadowracers.actors.mapobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 19.12.2017.
 */

public class BaseObject extends Actor implements Disposable {
    protected MyGame game = (MyGame) Gdx.app.getApplicationListener();

    protected Body body;
    protected TextureRegion textureRegion;

    public BaseObject(World world, Vector2 position, float angle) {
        angle = -(float) Math.toRadians(angle);
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(position);
        bodyDef.linearDamping = Constants.OBJECT_VELOCITY_DAMPING;
        bodyDef.angularDamping = Constants.OBJECT_VELOCITY_DAMPING;
        bodyDef.angle = angle;
        body = world.createBody(bodyDef);
    }

    void updatePosition() {
        setPosition(body.getPosition().x * Constants.PPT,
                body.getPosition().y * Constants.PPT, Align.center);
        setRotation(body.getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color temp = batch.getColor();
        batch.setColor(getColor());
        batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(),
                getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        batch.setColor(temp);
    }

    public Body getBody() {
        return body;
    }

    @Override
    public void dispose() {
        if (getStage() != null) {
            ((Mode) getStage()).deleteBody(body);
        }
        this.remove();
    }
}
