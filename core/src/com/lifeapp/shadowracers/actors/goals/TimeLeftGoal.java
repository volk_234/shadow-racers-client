package com.lifeapp.shadowracers.actors.goals;

import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 15.02.2018.
 */

public class TimeLeftGoal extends AdvGoal {

    public TimeLeftGoal(Mode mode, int goal) {
        super(mode, "", goal);
    }
    public TimeLeftGoal(Mode mode, int[] goals) {
        super(mode, "", goals);
    }

    @Override
    protected void updateText() {

        Mode.GameInfo gameInfo = mode.getGameInfo();
        setText(game.i18NBundle.format("advGoalTime", getGoalsAsString(), gameInfo.passTime));
    }

}
