package com.lifeapp.shadowracers.actors.goals;

import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 15.02.2018.
 */

public class CollisionGoal extends AdvGoal {

    public CollisionGoal(Mode mode, int goal) {
        super(mode, "", goal);
        complete();
    }


    @Override
    protected void updateText() {

        Mode.GameInfo gameInfo = mode.getGameInfo();
        setText(game.i18NBundle.format("collisions", getGoalsAsString(), gameInfo.collisionCount));
        if (gameInfo.collisionCount > goals[0]) {
            fail();
        }
    }

}
