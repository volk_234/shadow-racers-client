package com.lifeapp.shadowracers.actors.goals;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.mapobjects.Goal;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class GoalPointer extends Group {
    public enum Type {RANDOM, ARROW, DISTANCE}

    private Type type;
    private Goal goal;

    private Image arrowImage;
    private Label dstValueLabel;

    public GoalPointer(Type type, Goal goal) {
        if (type == Type.RANDOM) {
            type = MathUtils.randomBoolean() ? Type.ARROW : Type.DISTANCE;
        }
        this.type = type;
        this.goal = goal;

        MyGame game = MyGame.getInstance();
        Skin skin = MyGame.getSkin();
        switch (type) {
            case ARROW:
                arrowImage = new Image(skin.getRegion(Assets.ARROW));
                arrowImage.setSize(Constants.H1, Constants.H1);
                arrowImage.setOrigin(Align.center);
                addActor(arrowImage);
                arrowImage.setPosition(0, 0, Align.top);
                break;
            case DISTANCE:
                VerticalGroup verticalGroup = new VerticalGroup();
                addActor(verticalGroup);

                dstValueLabel = new Label("0", skin, "big", new Color(0xfffd48ff));
                verticalGroup.addActor(dstValueLabel);

                Label dstLabel = new Label(game.i18NBundle.get("distanceToGoal"), skin, "small", Color.WHITE);
                dstLabel.setAlignment(Align.center);
                dstLabel.setFontScale(0.5f);
                verticalGroup.addActor(dstLabel);
                break;
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        Mode mode = (Mode) goal.getStage();
        switch (type) {
            case ARROW:
                float rotation = 90 + mode.getAngleFromPlayer(goal.getBody().getPosition());
                arrowImage.setRotation(rotation);
                break;
            case DISTANCE:
                int dst = (int) mode.getDstFromPlayer(goal.getBody().getPosition());
                dstValueLabel.setText(String.valueOf(dst));
                break;
        }
    }
}
