package com.lifeapp.shadowracers.actors.goals;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 15.02.2018.
 */

public abstract class AdvGoal extends Label {
    MyGame game = (MyGame) Gdx.app.getApplicationListener();

    Mode mode;
    int status = 0;
    int[] goals = new int[]{};

    public AdvGoal(Mode mode, String text, int goal) {
        this(mode, text, new int[]{goal});
    }

    AdvGoal(Mode mode, String text, int[] goals) {
        super(text, MyGame.getSkin(), "small", new Color(0xffffff66));
        this.mode = mode;
        this.goals = goals;
    }

    public void blink() {
        Color color = getColor();
        addAction(Actions.sequence(
                Actions.color(Color.RED, 0.1f),
                Actions.color(color, 0.1f),
                Actions.color(Color.RED, 0.1f),
                Actions.color(color, 0.1f)
        ));
    }

    void complete() {
        if (status == 1) {
            return;
        }
        status = 1;
        addAction(Actions.sequence(
                Actions.color(new Color(0xffffffff), 0.15f),
                Actions.color(new Color(0x00ff0088), 0.30f)
                )
        );
    }

    void fail() {
        if (status == -1) {
            return;
        }
        status = -1;
        addAction(Actions.sequence(
                Actions.color(new Color(0xffffffff), 0.15f),
                Actions.color(new Color(0xff0000ff), 0.30f)
                )
        );
    }

    public boolean isFailed() {
        return status == -1;
    }

    protected abstract void updateText();


    @Override
    public void act(float delta) {
        super.act(delta);
        updateText();
    }

    String getGoalsAsString() {
        if (goals.length == 1) {
            return String.format("%d", goals[0]);
        } else {
            return String.format("%d/%d/%d", goals[0], goals[1], goals[2]);
        }
    }
}
