package com.lifeapp.shadowracers.actors.goals;

import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 15.02.2018.
 */

public class GoalsCountGoal extends AdvGoal {

    public GoalsCountGoal(Mode mode, int goal) {
        super(mode, "", goal);
    }

    public GoalsCountGoal(Mode mode, int[] goals) {
        super(mode, "", goals);
    }

    @Override
    protected void updateText() {
        Mode.GameInfo gameInfo = mode.getGameInfo();
        setText(String.format("Целей %s | %s", getGoalsAsString(), gameInfo.goalsReached));
    }

}
