package com.lifeapp.shadowracers.actors.goals;

import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 15.02.2018.
 */

public class BestDriftGoal extends AdvGoal {

    public BestDriftGoal(Mode mode, int goal) {
        super(mode, "", goal);
    }

    public BestDriftGoal(Mode mode, int[] goals) {
        super(mode, "", goals);
    }

    @Override
    protected void updateText() {

        Mode.GameInfo gameInfo = mode.getGameInfo();
        setText(game.i18NBundle.format("bestDrift", getGoalsAsString(), gameInfo.bestDrift));
        if (gameInfo.bestDrift >= goals[0]) {
            complete();
        }
    }

}
