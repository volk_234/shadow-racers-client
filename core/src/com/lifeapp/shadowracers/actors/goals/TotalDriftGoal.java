package com.lifeapp.shadowracers.actors.goals;

import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 15.02.2018.
 */

public class TotalDriftGoal extends AdvGoal {

    public TotalDriftGoal(Mode mode, int goal) {
        super(mode, "", goal);
    }

    public TotalDriftGoal(Mode mode, int[] goals) {
        super(mode, "", goals);
    }

    @Override
    protected void updateText() {
        Mode.GameInfo gameInfo = mode.getGameInfo();
        setText(game.i18NBundle.format("drift", getGoalsAsString(), gameInfo.totalDrift));
        if (gameInfo.totalDrift >= goals[0]) {
            complete();
        }
    }

}
