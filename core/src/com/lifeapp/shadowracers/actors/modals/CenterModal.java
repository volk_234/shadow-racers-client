package com.lifeapp.shadowracers.actors.modals;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class CenterModal extends BaseModal {

    private Image backgroundImage;

    public CenterModal() {
        backgroundImage = new Image(skin.newDrawable(Assets.PIXEL, new Color(0x00000088)));
        backgroundImage.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        modalTable = new Table();
        modalTable.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        modalTable.center();

        addActor(modalTable);
        setOriginX(getWidth() * 0.5f);
        getColor().a = 0;
        setY(-Constants.H1 * 3);
    }

    public void hide() {
        addAction(Actions.sequence(
                Actions.parallel(
                        Actions.moveTo(0, -Constants.H1 * 3, 0.3f, Interpolation.exp5Out),
                        Actions.fadeOut(0.3f, Interpolation.exp5Out)
                ),
                Actions.removeActor()));
        mode.setBlur(0);
    }

    public void show() {
        addAction(Actions.parallel(
                Actions.moveTo(0, 0, 0.6f, Interpolation.exp5Out),
                Actions.fadeIn(0.6f, Interpolation.exp5Out)
        ));
        mode.setBlur(3);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        drawBackground(batch, parentAlpha);
        super.draw(batch, parentAlpha);
    }

    protected void drawBackground(Batch batch, float parentAlpha) {
        backgroundImage.getColor().a = getColor().a;
        backgroundImage.draw(batch, parentAlpha);
    }
}
