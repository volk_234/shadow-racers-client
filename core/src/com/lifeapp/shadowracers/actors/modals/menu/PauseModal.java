package com.lifeapp.shadowracers.actors.modals.menu;

import com.appodeal.gdx.GdxAppodeal;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.lifeapp.shadowracers.actors.ui.IconTextButton;
import com.lifeapp.shadowracers.actors.modals.RightModal;
import com.lifeapp.shadowracers.modes.CampaignMode;
import com.lifeapp.shadowracers.screens.GameScreen;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class PauseModal extends RightModal {
    private GameScreen screen;

    public PauseModal(final GameScreen screen) {
        super();
        this.screen = screen;
        IconTextButton menuButton =
                new IconTextButton(game.i18NBundle.get("menu"), skin,
                        skin.newDrawable(Assets.ICON_MENU, Constants.COLOR_GRAY));
        modalTable.add(menuButton).padBottom(Constants.UI_PAD);
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                hide();
                mode.exit();
                mode.showModal(new MenuModal());
            }
        });


        if (screen.getMode() instanceof CampaignMode) {
            IconTextButton restartButton =
                    new IconTextButton(game.i18NBundle.get("restart"), skin,
                            skin.getDrawable(Assets.ICON_RESTART), Constants.COLOR_RED);
            modalTable.row();
            modalTable.add(restartButton).padBottom(Constants.UI_PAD);
            restartButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundUtils.playClick();
                    ((CampaignMode) screen.getMode()).restart();
                }
            });
        }

        IconTextButton playButton =
                new IconTextButton(game.i18NBundle.get("continue"), skin,
                        skin.getDrawable(Assets.ICON_PLAY), Constants.COLOR_BLUE);
        modalTable.row();
        modalTable.add(playButton).padBottom(Constants.UI_PAD);
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                hide();
                screen.resume();
            }
        });
    }

    @Override
    public void show() {
        super.show();
        mode.getUiGroup().setVisible(false);
        GdxAppodeal.show(GdxAppodeal.BANNER_BOTTOM);
        Vector2 cameraPos = new Vector2(mode.getPlayerCar().getX() + Constants.W1 * 2.5f,
                mode.getPlayerCar().getY());
        mode.setCameraTarget(cameraPos, 1);
        mode.setBlur(3);
    }

    @Override
    public void hide() {
        super.hide();
        mode.getUiGroup().setVisible(true);
        GdxAppodeal.hide(GdxAppodeal.BANNER_BOTTOM);
        mode.setBlur(0);
    }
}
