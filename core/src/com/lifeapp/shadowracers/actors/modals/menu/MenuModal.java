package com.lifeapp.shadowracers.actors.modals.menu;

import com.appodeal.gdx.GdxAppodeal;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.lifeapp.shadowracers.actors.modals.RightModal;
import com.lifeapp.shadowracers.actors.modals.campaign.LevelSelectModal;
import com.lifeapp.shadowracers.actors.modals.tuning.TuningModal;
import com.lifeapp.shadowracers.actors.ui.IconTextButton;
import com.lifeapp.shadowracers.modes.LobbyMode;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class MenuModal extends RightModal {

    public MenuModal() {
        super();

        IconTextButton singleButton =
                new IconTextButton(game.i18NBundle.get("singleMode"), skin,
                        skin.getDrawable(Assets.ICON_SINGLE), Constants.COLOR_BLUE);
        singleButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                game.gameScreen.getMode().showModal(new LevelSelectModal());
            }
        });
        modalTable.add(singleButton).pad(Constants.UI_PAD).left().fillX();

        IconTextButton onlineButton =
                new IconTextButton(game.i18NBundle.get("onlineMode"), skin,
                        skin.getDrawable(Assets.ICON_ONLINE), Constants.COLOR_BLUE);
        onlineButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                game.gameScreen.initMode(new LobbyMode());
            }
        });
        modalTable.add(onlineButton).pad(Constants.UI_PAD).left().fillX();

        modalTable.row();
        IconTextButton tuningButton =
                new IconTextButton(game.i18NBundle.get("tuning"), skin,
                        skin.getDrawable(Assets.ICON_TUNING), Constants.COLOR_RED);
        tuningButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new TuningModal());
            }
        });
        modalTable.add(tuningButton).pad(Constants.UI_PAD).left().fillX();

        IconTextButton optionsButton =
                new IconTextButton(game.i18NBundle.get("options"), skin,
                        skin.newDrawable(Assets.ICON_OPTIONS, Constants.COLOR_GRAY));
        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                OptionsModal optionsModal = new OptionsModal();
                game.gameScreen.getMode().showModal(optionsModal);
            }
        });
        modalTable.add(optionsButton).pad(Constants.UI_PAD).left().fillX();
    }

    @Override
    public void show() {
        super.show();
        mode.getUiGroup().clear();
        GdxAppodeal.hide(GdxAppodeal.BANNER_BOTTOM);
        Vector2 cameraPos = new Vector2(mode.getPlayerCar().getX() + Constants.W1 * 2.5f,
                mode.getPlayerCar().getY());
        mode.setCameraTarget(cameraPos, 1);
        mode.setBlur(0);
    }
}
