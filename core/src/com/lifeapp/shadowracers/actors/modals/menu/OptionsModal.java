package com.lifeapp.shadowracers.actors.modals.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.lifeapp.shadowracers.actors.modals.RightModal;
import com.lifeapp.shadowracers.utils.OptionsUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

class OptionsModal extends RightModal {


    OptionsModal() {
        super();

        final CheckBox soundCheckBox = new CheckBox(game.i18NBundle.get("sound"), skin);
        soundCheckBox.getImageCell().padRight(Constants.UI_PAD);
        modalTable.add(soundCheckBox).left();
        soundCheckBox.setChecked(OptionsUtils.isSoundEnabled());
        soundCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtils.playSwitch();
                OptionsUtils.setSound(soundCheckBox.isChecked());
            }
        });
        soundCheckBox.getImage().setScaling(Scaling.stretch);
        soundCheckBox.getImageCell().size(Constants.H1 * 0.5f);

        modalTable.row().padTop(Constants.UI_PAD);
        final CheckBox vibrationCheckBox = new CheckBox(game.i18NBundle.get("vibration"), skin);
        vibrationCheckBox.getImageCell().padRight(Constants.UI_PAD);
        modalTable.add(vibrationCheckBox).left();
        vibrationCheckBox.setChecked(OptionsUtils.isVibrationEnabled());
        vibrationCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtils.playSwitch();
                OptionsUtils.setVibration(vibrationCheckBox.isChecked());
            }
        });
        vibrationCheckBox.getImage().setScaling(Scaling.stretch);
        vibrationCheckBox.getImageCell().size(Constants.H1 * 0.5f);

        modalTable.row().padTop(Constants.UI_PAD);
        final CheckBox invertControlCheckBox = new CheckBox(game.i18NBundle.get("invertControl"), skin);
        invertControlCheckBox.getImageCell().padRight(Constants.UI_PAD);
        modalTable.add(invertControlCheckBox).left();
        invertControlCheckBox.setChecked(OptionsUtils.isInvertControlEnabled());
        invertControlCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtils.playSwitch();
                OptionsUtils.setInvertControl(invertControlCheckBox.isChecked());
            }
        });
        invertControlCheckBox.getImage().setScaling(Scaling.stretch);
        invertControlCheckBox.getImageCell().size(Constants.H1 * 0.5f);

        modalTable.row().padTop(Constants.UI_PAD);
        final CheckBox grainCheckBox = new CheckBox(game.i18NBundle.get("grain"), skin);
        grainCheckBox.getImageCell().padRight(Constants.UI_PAD);
        modalTable.add(grainCheckBox).left();
        grainCheckBox.setChecked(OptionsUtils.isGrainEnabled());
        grainCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtils.playSwitch();
                OptionsUtils.setGrain(grainCheckBox.isChecked());
            }
        });
        grainCheckBox.getImage().setScaling(Scaling.stretch);
        grainCheckBox.getImageCell().size(Constants.H1 * 0.5f);

        modalTable.row().padTop(Constants.UI_PAD);
        final CheckBox noticeCheckBox = new CheckBox(game.i18NBundle.get("notifyForNewOnlineGame"), skin);
        noticeCheckBox.getImageCell().padRight(Constants.UI_PAD);
        modalTable.add(noticeCheckBox).left();
        noticeCheckBox.setChecked(OptionsUtils.isSubscribedNewGame());
        noticeCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SoundUtils.playSwitch();
                OptionsUtils.subscribeNewGame(noticeCheckBox.isChecked());
            }
        });
        noticeCheckBox.getImage().setScaling(Scaling.stretch);
        noticeCheckBox.getImageCell().size(Constants.H1 * 0.5f);

        ImageButton backButton = new ImageButton(skin.getDrawable(Assets.ICON_LEFT));
        addActor(backButton);
        backButton.setSize(Constants.H1, Constants.H1);
        backButton.setPosition(20, Gdx.graphics.getHeight() * 0.5f -
                backButton.getHeight() * 0.5f);
        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new MenuModal());
            }
        });
    }
}
