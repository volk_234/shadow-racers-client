package com.lifeapp.shadowracers.actors.modals;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class RightModal extends BaseModal {

    public RightModal() {
        setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        modalTable = new Table();
        modalTable.setBackground(skin.newDrawable(Assets.MENU_RIGHT, new Color(0x000000aa)));
        modalTable.setSize(Constants.W1 * 5, Constants.H1 * 10);
        modalTable.padLeft(0.1971f * modalTable.getWidth());
        modalTable.setX(Gdx.graphics.getWidth() - modalTable.getWidth());

        setPosition(Gdx.graphics.getWidth() * 0.5f, 0);
        getColor().a = 0;

        addActor(modalTable);
    }

    public void hide() {
        addAction(Actions.sequence(
                Actions.parallel(
                        Actions.fadeOut(0.3f, Interpolation.exp5Out),
                        Actions.moveTo(Gdx.graphics.getWidth() * 0.5f, 0,
                                0.3f, Interpolation.exp5Out)
                ),
                Actions.removeActor())
        );
    }

    public void show() {
        addAction(Actions.parallel(
                Actions.fadeIn(0.3f, Interpolation.exp5Out),
                Actions.moveTo(0, 0, 0.3f, Interpolation.exp5Out)
        ));
    }
}
