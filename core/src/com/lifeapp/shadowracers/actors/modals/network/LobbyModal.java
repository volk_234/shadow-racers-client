package com.lifeapp.shadowracers.actors.modals.network;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.modes.LobbyMode;
import com.lifeapp.shadowracers.screens.GameScreen;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class LobbyModal extends CenterModal {

    public LobbyModal(final GameScreen screen) {
        setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        modalTable.background(skin.getDrawable(Assets.LOBBY_MODAL));
        modalTable.setSize(Gdx.graphics.getWidth(), Constants.H1 * 2);

        Label enterNameLabel = new Label(game.i18NBundle.get("yourName"), skin);
        enterNameLabel.setAlignment(Align.right);
        modalTable.add(enterNameLabel).width(Constants.W1 * 4).padRight(Constants.UI_PAD);

        final TextField nicknameField =
                new TextField(StatsUtils.getNickname(), skin);
        modalTable.add(nicknameField).size(Constants.W1 * 3, Constants.H1);

        TextButton playButton = new TextButton(game.i18NBundle.get("play"),
                skin, "default");
        playButton.getLabel().setColor(Constants.COLOR_YELLOW);
        playButton.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Gdx.input.setOnscreenKeyboardVisible(false);
                        SoundUtils.playClick();
                        String name = nicknameField.getText();
                        StatsUtils.setNickname(name);
                        ((LobbyMode) screen.getMode()).requestForLobby();
                    }
                });
        modalTable.add(playButton).size(Constants.W1 * 3, Constants.H1 * 2);

        this.addListener(new ClickListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (keycode == 66) {
                    modalTable.addAction(Actions.moveTo(0, 0, 0.15f));
                    Gdx.input.setOnscreenKeyboardVisible(false);
                }
                return true;
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (event.getTarget() == nicknameField) {
                    modalTable.addAction(Actions.moveTo(0,
                            Gdx.graphics.getHeight() - modalTable.getHeight(), 0.15f));
                } else {
                    modalTable.addAction(Actions.moveTo(0, 0, 0.15f));
                    Gdx.input.setOnscreenKeyboardVisible(false);
                }
            }
        });
    }
}
