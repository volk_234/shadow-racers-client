package com.lifeapp.shadowracers.actors.modals.network;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.lifeapp.shadowracers.actors.modals.CenterModal;

/**
 * Created by Александр on 11.02.2018.
 */

public class ConnectionModal extends CenterModal {

    public ConnectionModal() {
        super();

        Label label = new Label("Соединение с сервером...", skin);
        modalTable.add(label);
    }
}
