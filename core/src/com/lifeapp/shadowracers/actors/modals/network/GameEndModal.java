package com.lifeapp.shadowracers.actors.modals.network;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.actors.modals.menu.MenuModal;
import com.lifeapp.shadowracers.actors.ui.IconTextButton;
import com.lifeapp.shadowracers.actors.ui.MoneyLabel;
import com.lifeapp.shadowracers.modes.LobbyMode;
import com.lifeapp.shadowracers.network.OnlinePlayer;
import com.lifeapp.shadowracers.network.messages.GameEndMessage;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Александр on 11.02.2018.
 */

public class GameEndModal extends CenterModal {

    public GameEndModal(GameEndMessage message, Collection<OnlinePlayer> players) {
        MoneyLabel moneyLabel = new MoneyLabel(StatsUtils.getMoney());
        moneyLabel.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD - moneyLabel.getHeight());
        addActor(moneyLabel);

        Label label = new Label(game.i18NBundle.format("playerWin", message.winner.name),
                skin, "default", Color.WHITE);
        label.setAlignment(Align.center);
        label.setWrap(true);
        modalTable.add(label).fillX();

        IconTextButton menuButton =
                new IconTextButton(game.i18NBundle.get("menu"), skin,
                        skin.newDrawable(Assets.ICON_MENU, Constants.COLOR_GRAY));
        menuButton.setPosition(Constants.W1 * 1.5f, Constants.UI_PAD);
        menuButton.left().bottom();
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new MenuModal());
            }
        });
        addActor(menuButton);

        IconTextButton restartButton =
                new IconTextButton(game.i18NBundle.get("restart"), skin,
                        skin.getDrawable(Assets.ICON_RESTART), Constants.COLOR_RED);
        restartButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.addToQueue(() -> MyGame.getInstance().gameScreen.initMode(new LobbyMode()));
            }
        });
        restartButton.setPosition(Gdx.graphics.getWidth() - Constants.W1 * 1.5f - menuButton.getPrefWidth(),
                Constants.UI_PAD);
        restartButton.left().bottom();
        addActor(restartButton);

        moneyLabel = new MoneyLabel(StatsUtils.getMoney());
        moneyLabel.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD - moneyLabel.getHeight());
        addActor(moneyLabel);

        if (StatsUtils.getNickname().equals(message.winner.name)) {
            modalTable.row();
            StatsUtils.incOnlineWins();
            StatsUtils.modifyMoney(500);
            moneyLabel.modify(500);
        }

        modalTable.row().padBottom(Constants.H1);
        ArrayList<OnlinePlayer> playersArray = new ArrayList<>(players);
        Collections.sort(playersArray, (left, right) -> (int) (right.score - left.score));

        for (int i = 0; i < playersArray.size(); i++) {
            OnlinePlayer onlinePlayer = playersArray.get(i);
            Actor playerLabel = onlinePlayer.getScoreLabel();
            modalTable.row();
            modalTable.add(playerLabel).center();
        }
    }
}
