package com.lifeapp.shadowracers.actors.modals.network;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.actors.modals.menu.MenuModal;
import com.lifeapp.shadowracers.network.messages.ErrorMessage;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class NetworkErrorModal extends CenterModal {
    public NetworkErrorModal(ErrorMessage errorMessage) {

        Label label = new Label(game.i18NBundle.get("error"), skin, "big", Color.RED);
        label.setAlignment(Align.center);
        modalTable.add(label);

        Label labelInfo = new Label("", skin, "small");
        labelInfo.setAlignment(Align.center);
        labelInfo.setWrap(true);
        switch (errorMessage.code) {
            case 1:
                labelInfo.setText(game.i18NBundle.get("oldVersion"));
                modalTable.row().padTop(Constants.H1);
                modalTable.add(labelInfo).expandX().fillX();

                modalTable.row().padTop(Constants.H1);
                TextButton updateGameButton = new TextButton(game.i18NBundle.get("update"), skin);
                updateGameButton.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Gdx.net.openURI(
                                "https://play.google.com/store/apps/details?id=com.lifeapp.shadowracers");
                    }
                });
                modalTable.add(updateGameButton);
                break;
            default:
                labelInfo.setText(game.i18NBundle.get("connectionLost"));
                modalTable.row().padTop(Constants.H1);
                modalTable.add(labelInfo).fillX();
                break;
        }


        modalTable.row().padTop(Constants.H1);
        TextButton menuButton = new TextButton(game.i18NBundle.get("menu"), skin);
        modalTable.add(menuButton);
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new MenuModal());
                remove();
            }
        });
    }
}
