package com.lifeapp.shadowracers.actors.modals.network;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.actors.modals.menu.MenuModal;
import com.lifeapp.shadowracers.screens.GameScreen;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class FailConnectionModal extends CenterModal {
    public FailConnectionModal(final GameScreen screen) {
        Label label = new Label(game.i18NBundle.get("serverUnavailable"), skin, "default", Color.RED);
        label.setAlignment(Align.center);
        modalTable.add(label);

        modalTable.row().padTop(Constants.H1);
        Label labelInfo = new Label(game.i18NBundle.get("serverWorks"),
                skin, "small", Color.WHITE);
        labelInfo.setAlignment(Align.center);
        labelInfo.setWrap(true);
        modalTable.add(labelInfo).fillX();

        modalTable.row().padTop(Constants.H1);
        TextButton menuButton = new TextButton(game.i18NBundle.get("menu"), skin);
        modalTable.add(menuButton);
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new MenuModal());
                remove();
            }
        });
    }
}
