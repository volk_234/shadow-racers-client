package com.lifeapp.shadowracers.actors.modals;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Constants;


public class InfoModal extends BaseModal {
    protected MyGame game = ((MyGame) Gdx.app.getApplicationListener());
    private Table modalTable;

    private ShapeRenderer shapeRenderer = game.shapeRenderer;

    public InfoModal(String text) {
        modalTable = new Table();
        addActor(modalTable);

        Label label = new Label(text, skin, "small", Color.WHITE);
        label.setWrap(true);


        modalTable.pad(Constants.UI_PAD).left().bottom();
        modalTable.add(label).width(Constants.W1 * 5);

        setPosition(Constants.W1 * 2.5f, -modalTable.getPrefHeight());

        Label clickForCloseLabel = new Label(game.i18NBundle.get("clickForClose"), skin, "small", Color.WHITE);
        clickForCloseLabel.setAlignment(Align.center);
        clickForCloseLabel.addAction(Actions.forever(
                Actions.sequence(
                        Actions.alpha(0.5f, 1f),
                        Actions.alpha(1f, 1f)
                )
        ));
        modalTable.row();
        modalTable.add(clickForCloseLabel).width(Constants.W1 * 5).padTop(10);
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
                InfoModal.this.clicked();
            }
        });
    }

    public InfoModal(String text, float duration) {
        this(text);
        show();

        addAction(Actions.sequence(
                Actions.delay(duration),
                Actions.run(
                        this::hide
                )
        ));
    }


    public void hide() {
        SequenceAction action = Actions.sequence(
                Actions.moveTo(getX(), -modalTable.getPrefHeight(), 0.3f, Interpolation.exp5Out),
                Actions.delay(0.3f)
        );
        action.addAction(Actions.removeActor());
        addAction(action);
    }

    public void clicked() {

    }

    public void show() {
        addAction(Actions.moveTo(getX(), 0, 0.3f, Interpolation.exp5Out));
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        Matrix4 shapeRendererMatrix = shapeRenderer.getTransformMatrix();
        Color shapeRendererColor = shapeRenderer.getColor();

        shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
        shapeRenderer.setColor(getColor());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.rect(getX() + modalTable.getX(Align.left), getY() + modalTable.getY(),
                modalTable.getPrefWidth(), modalTable.getPrefHeight(),
                new Color(0x7b1717ff),
                new Color(0x941b1bff),
                new Color(0xb03232ff),
                new Color(0xa92121ff));
        shapeRenderer.end();
        shapeRenderer.setTransformMatrix(shapeRendererMatrix);
        shapeRenderer.setColor(shapeRendererColor);
        batch.begin();
        super.draw(batch, parentAlpha);
    }
}
