package com.lifeapp.shadowracers.actors.modals.tuning;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.ui.ColorPicker;
import com.lifeapp.shadowracers.actors.ui.MoneyLabel;
import com.lifeapp.shadowracers.actors.modals.RightModal;
import com.lifeapp.shadowracers.utils.ShopUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class NeonsModal extends RightModal {


    private TextButton buyButton;


    public NeonsModal() {
        super();

        String usedCar = ShopUtils.getUsedCar();
        int price = mode.getPlayerCar().getCarInfo().priceNeons;
        MoneyLabel priceLabel = new MoneyLabel(price);
        modalTable.add(priceLabel).center().padBottom(Constants.UI_PAD);
        modalTable.row();

        MoneyLabel moneyLabel = new MoneyLabel(StatsUtils.getMoney());
        moneyLabel.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD - moneyLabel.getHeight());
        addActor(moneyLabel);


        ColorPicker colorPicker = new ColorPicker(skin, MyGame.getAssetManager()
                .get(Assets.COLOR_PICKER, Texture.class));
        modalTable.add(colorPicker).expandX().fillX().height(Constants.H1 * 3);
        colorPicker.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                mode.getPlayerCar().changeNeonsColor(colorPicker.getPickedColor());
                buyButton.setVisible(true);
            }
        });

        buyButton = new TextButton(game.i18NBundle.get("buy"), skin);
        if (StatsUtils.getMoney() >= price) {
            buyButton.setColor(Color.GREEN);
            buyButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Color selectedColor = colorPicker.getPickedColor();
                    StatsUtils.modifyMoney(-price);
                    moneyLabel.modify(-price);
                    ShopUtils.buyNeonsForCar(usedCar, selectedColor);
                    ShopUtils.useNeonsForCar(usedCar, selectedColor);
                    SoundUtils.playClick();
                    buyButton.setVisible(false);
                }
            });
        } else {
            buyButton.setColor(Constants.COLOR_RED);
            buyButton.setText("Недостаточно денег");
            buyButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundUtils.playDisabled();
                }
            });
        }
        buyButton.setVisible(false);
        modalTable.row().padTop(Constants.UI_PAD);
        modalTable.add(buyButton).padBottom(Constants.H1 * 0.5f);

        ImageButton backButton = new ImageButton(skin.getDrawable(Assets.ICON_LEFT));
        addActor(backButton);
        backButton.setPosition(20, Gdx.graphics.getHeight() * 0.5f -
                backButton.getHeight() * 0.5f);
        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mode.showModal(new TuningModal());
            }
        });
    }

    @Override
    public void show() {
        super.show();
        Vector2 cameraPos = new Vector2(mode.getPlayerCar().getX(Align.center) + Constants.W1 * 2.5f / 2f,
                mode.getPlayerCar().getY(Align.center));
        mode.setCameraTarget(cameraPos, 1.5f);
    }

    @Override
    public void hide() {
        String colorString = StatsUtils.getPlayerModel().neonsColor;
        Color color = colorString == null ? null : Color.valueOf(colorString);
        mode.getPlayerCar().changeNeonsColor(color);
        super.hide();
    }
}
