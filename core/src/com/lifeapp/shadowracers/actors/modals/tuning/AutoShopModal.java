package com.lifeapp.shadowracers.actors.modals.tuning;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.actors.cars.AstonCar;
import com.lifeapp.shadowracers.actors.cars.AudiCar;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.cars.BmwCar;
import com.lifeapp.shadowracers.actors.cars.DodgeCar;
import com.lifeapp.shadowracers.actors.cars.FerrariCar;
import com.lifeapp.shadowracers.actors.cars.HyperCar;
import com.lifeapp.shadowracers.actors.cars.LamboCar;
import com.lifeapp.shadowracers.actors.cars.MustangCar;
import com.lifeapp.shadowracers.actors.cars.PorscheCar;
import com.lifeapp.shadowracers.actors.cars.RenaultCar;
import com.lifeapp.shadowracers.actors.cars.ViperCar;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.actors.ui.MoneyLabel;
import com.lifeapp.shadowracers.network.PlayerModel;
import com.lifeapp.shadowracers.utils.ShopUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class AutoShopModal extends CenterModal {
    private String[] types = new String[]{
            BmwCar.class.getSimpleName(),
            MustangCar.class.getSimpleName(),
            AstonCar.class.getSimpleName(),
            RenaultCar.class.getSimpleName(),
            DodgeCar.class.getSimpleName(),
            FerrariCar.class.getSimpleName(),
            LamboCar.class.getSimpleName(),
            PorscheCar.class.getSimpleName(),
            AudiCar.class.getSimpleName(),
            ViperCar.class.getSimpleName(),
            HyperCar.class.getSimpleName(),
    };
    private int currentIndex = 0;
    private Table definitionTable;
    private MoneyLabel priceLabel;
    private ProgressBar speedBar;
    private ProgressBar accelerationBar;
    //    private ProgressBar controllingBar;
    private TextButton buyButton;

    private ImageButton nextButton, prevButton;
    private MoneyLabel moneyLabel;

    AutoShopModal() {
        super();

        moneyLabel = new MoneyLabel(StatsUtils.getMoney());
        moneyLabel.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD - moneyLabel.getHeight());
        addActor(moneyLabel);

        prevButton = new ImageButton(skin.getDrawable(Assets.ICON_UP));
        prevButton.setPosition(Gdx.graphics.getWidth() * 0.5f,
                Gdx.graphics.getHeight() - prevButton.getHeight(), Align.center);
        prevButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.getListenerActor().getColor().a = 1;
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                event.getListenerActor().getColor().a = 0.75f;
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentIndex > 0) {
                    currentIndex--;
                    updateCar();
                    updateArrows();
                }
                SoundUtils.playClick();
            }
        });
        prevButton.getColor().a = 0.75f;
        addActor(prevButton);

        nextButton = new ImageButton(skin.getDrawable(Assets.ICON_DOWN));
        nextButton.setPosition(Gdx.graphics.getWidth() * 0.5f, nextButton.getHeight(), Align.center);
        nextButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.getListenerActor().getColor().a = 1;
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                event.getListenerActor().getColor().a = 0.75f;
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                if (currentIndex < types.length - 1) {
                    currentIndex++;
                    updateCar();
                    updateArrows();
                }
            }
        });
        nextButton.getColor().a = 0.75f;
        addActor(nextButton);

        ImageButton backButton = new ImageButton(skin.getDrawable(Assets.ICON_LEFT));
        addActor(backButton);
        backButton.setPosition(20, Gdx.graphics.getHeight() * 0.5f -
                backButton.getHeight() * 0.5f);
        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mode.showModal(new TuningModal());
                SoundUtils.playClick();
            }
        });

        definitionTable = new Table();
        definitionTable.background(skin.newDrawable(Assets.PIXEL, new Color(0x00000099)));
        definitionTable.pad(Constants.UI_PAD);
        definitionTable.center();
        definitionTable.defaults().left();
        definitionTable.setSize(Constants.W1 * 3, Constants.H1 * 7);

        definitionTable.setPosition(Constants.W1 * 6, Constants.H1 * 5,
                Align.center | Align.left);


        priceLabel = new MoneyLabel(0);
        definitionTable.add(priceLabel).padBottom(Constants.UI_PAD).center();

        Label speedLabel = new Label("Скорость", skin, "default", Color.WHITE);
        definitionTable.row();
        definitionTable.add(speedLabel);

        speedBar = new ProgressBar(15, 45, 1, false, skin);
        definitionTable.row();
        speedBar.setAnimateDuration(0.3f);
        definitionTable.add(speedBar).expandX().fill().padBottom(Constants.UI_PAD);

        Label accelerationLabel = new Label("Ускорение", skin,
                "default", Color.WHITE);
        definitionTable.row();
        definitionTable.add(accelerationLabel);

        accelerationBar = new ProgressBar(60, 100, 1, false, skin);
        accelerationBar.setAnimateDuration(0.3f);
        definitionTable.row();
        definitionTable.add(accelerationBar).expandX().fill().padBottom(Constants.UI_PAD);

//        Label controllingLabel = new Label("Управляемость", skin,
//                "default", Color.WHITE);
//        definitionTable.row();
//        definitionTable.add(controllingLabel);

//        controllingBar = new ProgressBar(0, 100, 1, false, skin);
//        controllingBar.setAnimateDuration(0.3f);
//        definitionTable.row();
//        definitionTable.add(controllingBar).expandX().fill().padBottom(Constants.UI_PAD);

        buyButton = new TextButton("Купить", skin);
        definitionTable.row();
        definitionTable.add(buyButton).center();

        addActor(definitionTable);

    }

    @Override
    public void show() {
        super.show();
        mode.setBlur(0);
        updateCar();
        updateArrows();
    }

    private void updateArrows() {
        prevButton.setVisible(currentIndex > 0);
        nextButton.setVisible(currentIndex < types.length - 1);
    }

    private void updateCar() {
        mode.getPlayerCar().dispose();
        PlayerModel playerModel = new PlayerModel();
        playerModel.car = types[currentIndex];
        BaseCar car = mode.makeCar(playerModel, -1);
        car.controlByPlayer();
        mode.setCameraAtPlayer();

        BaseCar.CarInfo carInfo = car.getCarInfo();
        priceLabel.modifyTo(carInfo.price);
        speedBar.setValue(carInfo.maxForwardSpeed);
        accelerationBar.setValue(carInfo.backTireMaxDriveForce + carInfo.frontTireMaxDriveForce);
//        controllingBar.setValue(carInfo.backTireMaxLateralImpulse + carInfo.frontTireMaxLateralImpulse);
        buyButton.clearListeners();
        if (types[currentIndex].equals(ShopUtils.getUsedCar())) {
            buyButton.setText("Используется");
            buyButton.getLabel().setColor(Color.GRAY);
        } else if (ShopUtils.isCarBought(types[currentIndex])) {
            buyButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundUtils.playClick();
                    ShopUtils.useCar(types[currentIndex]);
                    buyButton.clearListeners();
                    buyButton.setText("Используется");
                    buyButton.getLabel().setColor(Color.GRAY);
                }
            });
            buyButton.setText("Использовать");
            buyButton.getLabel().setColor(Color.GREEN);
        } else if (StatsUtils.getMoney() >= carInfo.price) {
            buyButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundUtils.playClick();
                    StatsUtils.modifyMoney(-carInfo.price);
                    ShopUtils.buyCar(types[currentIndex]);
                    ShopUtils.useCar(types[currentIndex]);
                    moneyLabel.modify(-carInfo.price);
                    buyButton.clearListeners();
                    buyButton.setText("Используется");
                    buyButton.getLabel().setColor(Color.GRAY);
                }
            });
            buyButton.setText("Купить");
            buyButton.getLabel().setColor(Color.GREEN);
        } else {
            buyButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundUtils.playDisabled();
                }
            });
            buyButton.setText("Недостаточно денег");
            buyButton.getLabel().setColor(Color.RED);
        }
    }


    @Override
    protected void drawBackground(Batch batch, float parentAlpha) {
    }

    @Override
    public void hide() {
        mode.getPlayerCar().dispose();
        mode.makeCar(StatsUtils.getPlayerModel()).controlByPlayer();
        mode.setCameraAtPlayer();
        super.hide();
    }
}
