package com.lifeapp.shadowracers.actors.modals.tuning;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.actors.ui.IconTextButton;
import com.lifeapp.shadowracers.actors.ui.MoneyLabel;
import com.lifeapp.shadowracers.actors.modals.menu.MenuModal;
import com.lifeapp.shadowracers.actors.modals.RightModal;
import com.lifeapp.shadowracers.actors.ui.RectangleTextButton;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.02.2018.
 */

public class TuningModal extends RightModal {

    public TuningModal() {
        super();

        MoneyLabel moneyLabel = new MoneyLabel(StatsUtils.getMoney());
        moneyLabel.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD - moneyLabel.getHeight());
        addActor(moneyLabel);

        RectangleTextButton autoShopButton = new RectangleTextButton("Автомагазин",
                skin, skin.getDrawable(Assets.ICON_CAR), Constants.COLOR_BLUE, Color.WHITE);
        autoShopButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new AutoShopModal());
            }
        });
        modalTable.add(autoShopButton).colspan(3).expandX().fillX().height(Constants.H1 * 2).pad(Constants.UI_PAD);

        modalTable.row();
        IconTextButton coloringButton =
                new IconTextButton(game.i18NBundle.get("coloring"), skin,
                        skin.newDrawable(Assets.ICON_COLORS, Constants.COLOR_BLUE), Color.WHITE);
        coloringButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new ColoringModal());
            }
        });
        modalTable.add(coloringButton).pad(Constants.UI_PAD).left().fillX();

        IconTextButton lightsButton =
                new IconTextButton(game.i18NBundle.get("lights"), skin,
                        skin.newDrawable(Assets.ICON_LIGHTS, Constants.COLOR_BLUE), Color.WHITE);
        lightsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new LightsModal());
            }
        });
        modalTable.add(lightsButton).pad(Constants.UI_PAD).left().fillX();

        IconTextButton neonButton =
                new IconTextButton(game.i18NBundle.get("neon"), skin,
                        skin.newDrawable(Assets.ICON_NEONS, Constants.COLOR_BLUE), Color.WHITE);
        neonButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new NeonsModal());
            }
        });
        modalTable.add(neonButton).pad(Constants.UI_PAD).left().fillX();

        ImageButton backButton = new ImageButton(skin.getDrawable(Assets.ICON_LEFT));
        addActor(backButton);
        backButton.setPosition(20, Gdx.graphics.getHeight() * 0.5f -
                backButton.getHeight() * 0.5f);
        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new MenuModal());
            }
        });
    }

    @Override
    public void show() {
        super.show();

        Vector2 cameraPos = new Vector2(mode.getPlayerCar().getX(Align.center) + Constants.W1 * 2.5f,
                mode.getPlayerCar().getY(Align.center));
        mode.setCameraTarget(cameraPos, 1);
        mode.setBlur(0);
    }
}
