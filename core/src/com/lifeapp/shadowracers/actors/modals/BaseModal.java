package com.lifeapp.shadowracers.actors.modals;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.modes.Mode;

/**
 * Created by Александр on 11.02.2018.
 */

public class BaseModal extends Group {
    protected MyGame game = ((MyGame) Gdx.app.getApplicationListener());
    protected Mode mode = game.gameScreen.getMode();
    protected Table modalTable;
    protected Skin skin = MyGame.getSkin();

    public BaseModal() {
    }

    public void hide() {
    }

    public void show() {
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }
}
