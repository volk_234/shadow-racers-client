package com.lifeapp.shadowracers.actors.modals.campaign;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.ui.IconTextButton;
import com.lifeapp.shadowracers.actors.ui.StarsActor;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.modes.CampaignMode;
import com.lifeapp.shadowracers.utils.FireBaseUtils;
import com.lifeapp.shadowracers.utils.MissionsUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

class LevelPreviewModal extends CenterModal {
    LevelPreviewModal(int index) {
        MissionsUtils.Mission mission = MissionsUtils.getMission(index);

        modalTable.setFillParent(true);

        MissionsUtils.Mission.Progress progress = mission.savedProgress;
        StarsActor starsActor = new StarsActor(progress.stars);
        modalTable.add(starsActor).size(Constants.W1 * 3, Constants.W1 * 1.5f).colspan(3);
        starsActor.makeShowAnimation();
        starsActor.makeShakeAnimation();

        Label missionIndexLabel = new Label(String.format("Уровень %d", index),
                skin, "default", Color.WHITE);
        modalTable.row();
        modalTable.add(missionIndexLabel).colspan(3).center();

        modalTable.row().expand().fill();
        modalTable.add(mission.getDefinitionTable(skin))
                .colspan(2).padRight(Constants.UI_PAD).padLeft(Constants.UI_PAD);
        modalTable.add(mission.getLeadersTable(skin));

        modalTable.row().padBottom(Constants.UI_PAD).padTop(Constants.UI_PAD);
        IconTextButton menuButton =
                new IconTextButton(game.i18NBundle.get("back"), skin,
                        skin.newDrawable(Assets.ICON_LEFT, Constants.COLOR_GRAY));
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new LevelSelectModal());
            }
        });
        modalTable.add(menuButton);


        IconTextButton leadersButton =
                new IconTextButton(game.i18NBundle.get("leaders"), skin,
                        skin.newDrawable(Assets.ICON_LEADERS, Constants.COLOR_GRAY));
        leadersButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                FireBaseUtils.showLeaderBoardForLevel(index);
            }
        });
        modalTable.add(leadersButton);

        IconTextButton playButton =
                new IconTextButton(game.i18NBundle.get("play"), skin,
                        skin.getDrawable(Assets.ICON_PLAY), Constants.COLOR_BLUE);
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                CampaignMode mode = new CampaignMode(mission);
                MyGame.getInstance().gameScreen.initMode(mode);
            }
        });
        modalTable.add(playButton);
    }
}
