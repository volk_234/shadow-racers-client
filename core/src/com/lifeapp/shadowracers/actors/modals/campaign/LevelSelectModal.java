package com.lifeapp.shadowracers.actors.modals.campaign;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.actors.modals.menu.MenuModal;
import com.lifeapp.shadowracers.actors.ui.IconTextButton;
import com.lifeapp.shadowracers.actors.ui.LevelTextButton;
import com.lifeapp.shadowracers.actors.ui.MoneyLabel;
import com.lifeapp.shadowracers.utils.MissionsUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class LevelSelectModal extends CenterModal {
    private ClickListener clickListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            SoundUtils.playClick();
            LevelPreviewModal levelPreviewModal =
                    new LevelPreviewModal((Integer) event.getListenerActor().getUserObject());
            mode.showModal(levelPreviewModal);
        }
    };

    public LevelSelectModal() {
        Table levelsTable = new Table();

        for (int i = 1; i <= Constants.MISSIONS_COUNT; i++) {
            LevelTextButton levelTextButton;
            MissionsUtils.Mission.Progress progress = MissionsUtils.getProgress(i);
            if (progress != null) {
                levelTextButton = new LevelTextButton(skin, i, progress.stars);
                levelTextButton.setUserObject(i);
                levelTextButton.addListener(clickListener);
            } else {
                levelTextButton = new LevelTextButton(skin);
            }
            levelsTable.add(levelTextButton).pad(Constants.UI_PAD);
            if (i % 4 == 0) {
                levelsTable.row();
            }
        }

        Label label = new Label("Новые уровни скоро", skin, "small", Color.WHITE);
        label.addAction(Actions.forever(Actions.sequence(
                Actions.color(new Color(0xffffff33), 1),
                Actions.color(new Color(0xffffffff), 1)
        )));
        levelsTable.add(label).center().colspan(4);

        ScrollPane scrollPane = new ScrollPane(levelsTable);
        modalTable.add(scrollPane).expand().fill();

        IconTextButton menuButton =
                new IconTextButton(game.i18NBundle.get("menu"), skin,
                        skin.newDrawable(Assets.ICON_MENU, Constants.COLOR_GRAY));
        addActor(menuButton);
        menuButton.setPosition(Constants.UI_PAD + Constants.W1 * 1.5f - menuButton.getPrefWidth() * 0.5f,
                Constants.UI_PAD);
        menuButton.left().bottom();
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.showModal(new MenuModal());
            }
        });

        MoneyLabel moneyLabel = new MoneyLabel(StatsUtils.getMoney());
        moneyLabel.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD - moneyLabel.getHeight());
        addActor(moneyLabel);
    }
}
