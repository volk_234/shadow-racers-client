package com.lifeapp.shadowracers.actors.modals.campaign;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.modals.CenterModal;
import com.lifeapp.shadowracers.actors.ui.IconTextButton;
import com.lifeapp.shadowracers.actors.ui.MoneyLabel;
import com.lifeapp.shadowracers.actors.ui.StarsActor;
import com.lifeapp.shadowracers.modes.CampaignMode;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.utils.FireBaseUtils;
import com.lifeapp.shadowracers.utils.MissionsUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

public class LevelEndModal extends CenterModal {

    private MoneyLabel moneyLabel;
    private Table definitionTable;

    public LevelEndModal(Mode.GameInfo gameInfo, MissionsUtils.Mission mission) {

        modalTable.setFillParent(true);

        MissionsUtils.Mission.Progress newProgress = mission.getProgressForGameInfo(gameInfo);

        StarsActor starsActor = new StarsActor(newProgress.stars);
        starsActor.makeShowAnimation();
        starsActor.makeShakeAnimation();
        modalTable.add(starsActor).size(Constants.W1 * 3, Constants.W1 * 1.5f).colspan(3);

        Label missionIndexLabel = new Label(String.format("Уровень %d", mission.index),
                skin, "default", Color.WHITE);
        modalTable.row();
        modalTable.add(missionIndexLabel).colspan(3).center();

        modalTable.row().expand().fill();

        definitionTable = mission.getDefinitionTable(skin, gameInfo);
        modalTable.add(definitionTable)
                .colspan(2).padRight(Constants.UI_PAD).padLeft(Constants.UI_PAD);

        modalTable.add(mission.getLeadersTable(skin));

        modalTable.row().padBottom(Constants.UI_PAD).padTop(Constants.UI_PAD);
        IconTextButton menuButton =
                new IconTextButton(game.i18NBundle.get("menu"), skin,
                        skin.getDrawable(Assets.ICON_MENU), Constants.COLOR_BLUE);
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                LevelSelectModal levelSelectModal = new LevelSelectModal();
                mode.showModal(levelSelectModal);
            }
        });
        modalTable.add(menuButton);

        IconTextButton leadersButton =
                new IconTextButton(game.i18NBundle.get("leaders"), skin,
                        skin.newDrawable(Assets.ICON_LEADERS, Constants.COLOR_GRAY));
        leadersButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                FireBaseUtils.showLeaderBoardForLevel(mission.index);
            }
        });
        modalTable.add(leadersButton);

        IconTextButton restartButton =
                new IconTextButton(game.i18NBundle.get("restart"), skin,
                        skin.getDrawable(Assets.ICON_RESTART), Constants.COLOR_RED);
        restartButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundUtils.playClick();
                mode.addToQueue(() -> MyGame.getInstance().gameScreen.initMode(new CampaignMode(mission)));
            }
        });
        modalTable.add(restartButton);

        moneyLabel = new MoneyLabel(StatsUtils.getMoney());
        moneyLabel.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD - moneyLabel.getHeight());
        addActor(moneyLabel);
    }

    public MoneyLabel getMoneyLabel() {
        return moneyLabel;
    }

    public void updateBestScore(String string) {
        Label bestScoreLabel = definitionTable.findActor("bestScore");
        bestScoreLabel.addAction(
                Actions.sequence(
                        Actions.parallel(
                                Actions.scaleTo(1.2f, 1.2f, 0.6f),
                                Actions.color(Constants.COLOR_RED, 0.6f)
                        ),
                        Actions.run(() -> {
                            bestScoreLabel.setText(string);
                        }),
                        Actions.parallel(
                                Actions.scaleTo(1f, 1f, 0.6f),
                                Actions.color(Constants.COLOR_YELLOW, 0.6f)
                        )
                )
        );
    }
}
