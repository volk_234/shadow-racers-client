package com.lifeapp.shadowracers.playground;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;

public class TestScreen implements Screen {
    class ExampleStage extends Stage implements GestureDetector.GestureListener {
        float currentZoom = 1;
        float zoomSpeed = 2f;

        float worldWidth = Gdx.graphics.getWidth() * 3;
        float worldHeight = Gdx.graphics.getHeight() * 6;

        float maximumZoom;

        public ExampleStage() {
            GestureDetector gestureDetector = new GestureDetector(this);

            InputMultiplexer inputMultiplexer = new InputMultiplexer(gestureDetector, this);
            Gdx.input.setInputProcessor(inputMultiplexer);

            maximumZoom = worldWidth / getCamera().viewportWidth;

        }

        public ExampleStage(TextureRegion bgTexture) {
            this();
            Image bg = new Image(bgTexture);
            bg.setSize(worldWidth, worldHeight);
            addActor(bg);
        }


        @Override
        public boolean touchDown(float x, float y, int pointer, int button) {
            return false;
        }

        @Override
        public boolean tap(float x, float y, int count, int button) {
            return false;
        }

        @Override
        public boolean longPress(float x, float y) {
            ((OrthographicCamera) getCamera()).zoom = 1;
            getCamera().update();
            return false;
        }

        @Override
        public boolean fling(float velocityX, float velocityY, int button) {
            return false;
        }

        @Override
        public boolean pan(float x, float y, float deltaX, float deltaY) {
            getCamera().translate(-deltaX * currentZoom, deltaY * currentZoom, 0);
            checkAndUpdateCam();
            return false;
        }

        @Override
        public boolean panStop(float x, float y, int pointer, int button) {
            return false;
        }

        @Override
        public boolean zoom(float initialDistance, float distance) {
            ((OrthographicCamera) getCamera()).zoom = currentZoom + (initialDistance - distance) * zoomSpeed / 1000;
            checkAndUpdateCam();
            return false;
        }

        @Override
        public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
            return false;
        }

        @Override
        public void pinchStop() {
            currentZoom = ((OrthographicCamera) getCamera()).zoom;
        }

        private void checkAndUpdateCam() {
            OrthographicCamera camera = (OrthographicCamera) getCamera();
            if (camera.zoom > maximumZoom) {
                camera.zoom = maximumZoom;
            }
            float halfViewPortWidth = camera.viewportWidth * 0.5f * camera.zoom;
            float halfViewPortHeight = camera.viewportHeight * 0.5f * camera.zoom;

            if (camera.position.x < halfViewPortWidth) {
                camera.position.x = halfViewPortWidth;
            } else if (camera.position.x > worldWidth - halfViewPortWidth) {
                camera.position.x = worldWidth - halfViewPortWidth;
            }
            if (camera.position.y < halfViewPortHeight) {
                camera.position.y = halfViewPortHeight;
            } else if (camera.position.y > worldHeight - halfViewPortHeight) {
                camera.position.y = worldHeight - halfViewPortHeight;
            }

            getCamera().update();
        }
    }

    ExampleStage exampleStage;


    @Override
    public void show() {
        exampleStage = new ExampleStage(MyGame.getTextureRegion(Assets.ATLAS_UI, Assets.COLOR));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        exampleStage.act();
        exampleStage.draw();

        Gdx.app.log("FPS", String.valueOf(Gdx.graphics.getFramesPerSecond()));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
