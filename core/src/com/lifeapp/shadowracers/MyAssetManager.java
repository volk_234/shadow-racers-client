package com.lifeapp.shadowracers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.utils.I18NBundle;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.12.2017.
 */

public class MyAssetManager extends AssetManager {
    public MyAssetManager() {
        super();

        load(Assets.CAR_ENGINE, Sound.class);
        load(Assets.CAR_IDLE, Sound.class);
        load(Assets.CAR_DRIFT, Sound.class);
        load(Assets.CAR_CRASH, Sound.class);
        load(Assets.CAR_CRASH_SMALL, Sound.class);

        load(Assets.GOAL_FAILED, Sound.class);
        load(Assets.GOAL_REACHED, Sound.class);

        load(Assets.CLICK, Sound.class);
        load(Assets.DISABLED, Sound.class);
        load(Assets.SWITCH, Sound.class);

        load(Assets.BEEP_READY, Sound.class);
        load(Assets.BEEP_GO, Sound.class);

        load(Assets.MUSIC, Music.class);

        load(Assets.ATLAS, TextureAtlas.class);
        load(Assets.ATLAS_UI, TextureAtlas.class);

        load(Assets.COLOR_PICKER, Texture.class);

        FileHandleResolver resolver = new InternalFileHandleResolver();
        setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));
        setLoader(BitmapFont.class, ".otf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter freeTypeFontParameter =
                new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        freeTypeFontParameter.fontFileName = Assets.FONT;
        freeTypeFontParameter.fontParameters.characters = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя" +
                "abcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQR" +
                "STUVWXYZ0123456789][_!$%#@|/?-+=()*&.;:,{}´`'<>;";
        freeTypeFontParameter.fontParameters.size = (int) (Constants.W1 * 0.2f);
        freeTypeFontParameter.fontParameters.borderWidth = (int) Constants.H1 * 0.025f;
        freeTypeFontParameter.fontParameters.borderColor = new Color(0x00000088);
        load("small.otf", BitmapFont.class, freeTypeFontParameter);

        freeTypeFontParameter =
                new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        freeTypeFontParameter.fontFileName = Assets.FONT;
        freeTypeFontParameter.fontParameters.characters = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя" +
                "abcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQR" +
                "STUVWXYZ0123456789][_!$%#@|/?-+=()*&.;:,{}´`'<>;";
        freeTypeFontParameter.fontParameters.size = (int) (Constants.W1 * 0.35f);
//        freeTypeFontParameter.fontParameters.borderWidth = (int) Constants.H1 * 0.025f;
//        freeTypeFontParameter.fontParameters.borderColor = new Color(0x00000088);
        load("default.otf", BitmapFont.class, freeTypeFontParameter);

        freeTypeFontParameter =
                new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        freeTypeFontParameter.fontFileName = Assets.FONT;
        freeTypeFontParameter.fontParameters.characters = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя" +
                "abcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQR" +
                "STUVWXYZ0123456789][_!$%#@|/?-+=()*&.;:,{}´`'<>;";
        freeTypeFontParameter.fontParameters.size = (int) (Constants.H1 * 1f);
//        freeTypeFontParameter.fontParameters.borderWidth = (int) Constants.H1 * 0.08f;
//        freeTypeFontParameter.fontParameters.borderColor = new Color(0x00000044);
        load("big.otf", BitmapFont.class, freeTypeFontParameter);

        load(Assets.LOC_BUNDLE, I18NBundle.class);
    }
}