package com.lifeapp.shadowracers;


import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import java.util.ArrayList;

public interface FireBaseApi {
    class LbScore implements Comparable<LbScore> {
        public Drawable iconDrawable;
        public long rank;
        public String displayName;
        public String displayScore;

        @Override
        public int compareTo(LbScore o) {
            return (int) (this.rank - o.rank);
        }
    }

    class ResponseListener<V> {
        public interface CompleteListener<V> {
            void complete(V response);
        }

        ArrayList<CompleteListener> completeListeners = new ArrayList<>();

        public void addCompleteListener(CompleteListener<V> completeListener) {
            completeListeners.add(completeListener);
        }

        public void success(V response) {
            for (CompleteListener completeListener : completeListeners) {
                completeListener.complete(response);
            }
        }
    }

    void subscribeToTopic(String topic);

    void unSubscribeFromTopic(String topic);

    String getIID();

    void showLeaderBoardForLevel(int level);

    void putScoreForLevel(int level, float score);

    boolean isSignedIn();

    void signInSilently();

    void startSignInIntent();

    ResponseListener<ArrayList<LbScore>> getTopScores(int level);

    ResponseListener<LbScore> getPlayerScore(int level);
}
