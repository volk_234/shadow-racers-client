package com.lifeapp.shadowracers.variables;

/**
 * Created by Александр on 11.12.2017.
 */

public class Assets {

    public static String ATLAS = "atlas.atlas";
    public static String ATLAS_UI = "ui.atlas";
    public static String FONT = "american_captain.otf";
    public static String LOADING_IMAGE = "loading_image.png";
    public static String ARROW = "arrow";
    public static String PIXEL = "pixel";
    public static String CAR_ASTON = "car_aston";
    public static String CAR_AUDI = "car_audi";
    public static String CAR_BMW = "car_bmw";
    public static String CAR_DODGE = "car_dodge";
    public static String CAR_FERRARI = "car_ferrari";
    public static String CAR_HYPER = "car_hyper";
    public static String CAR_LAMBO = "car_lambo";
    public static String CAR_MUSTANG = "car_mustang";
    public static String CAR_PORSCHE = "car_porshe";
    public static String CAR_RENAULT = "car_renault";
    public static String CAR_TRUCK = "car_truck";
    public static String CAR_VIPER = "car_viper";

    public static String CAR_WHEEL = "wheel";
    public static String BARREL = "barrel";
    public static String LAMPPOST = "lamppost";
    public static String MENU_RIGHT = "menu_right";
    public static String GOAL = "goal";
    public static String PARTICLE_BURNOUT = "burnout.p";
    public static String PARTICLE_GOAL_REACHED = "goal_reached.p";
    public static String PARTICLE_CRASH = "crash.p";
    public static String BURNOUT = "burnout";
    public static String COIN = "coin";
    public static String BUILDING_LIGHT = "building_light";
    public static String COLOR = "color";
    public static String NOISE = "noise";
    public static String POINTER = "icon_pointer";
    public static String CUP = "cup";
    public static String COLOR_PICKER = "color_picker.png";

    public static String CARGO_CONTAINER = "container";

    public static String CIRCLE = "icon_circle";

    public static String CHECKBOX_ON = "checkbox_on";
    public static String CHECKBOX_OFF = "checkbox_off";

    public static String BUTTON_BG = "button_bg";

    public static String ICON_BG = "icon_bg";
    public static String ICON_SINGLE = "icon_single";
    public static String ICON_ONLINE = "icon_online";
    public static String ICON_OPTIONS = "icon_options";
    public static String ICON_TUNING = "icon_tuning";
    public static String ICON_COLORS = "icon_coloring";
    public static String ICON_LIGHTS = "icon_lights";
    public static String ICON_CAR = "icon_car";
    public static String ICON_NEONS = "icon_neons";
    public static String ICON_PLAY = "icon_play";
    public static String ICON_MENU = "icon_menu";
    public static String ICON_PAUSE = "icon_pause";
    public static String ICON_LOCK = "icon_lock";
    public static String ICON_LEADERS = "icon_leaders";
    public static String ICON_RESTART = "icon_restart";

    public static String ICON_DOWN = "icon_down";
    public static String ICON_UP = "icon_up";
    public static String ICON_LEFT = "icon_left";
    public static String ICON_RIGHT = "icon_right";

    public static String ICON_STAR_ON = "star_on";
    public static String ICON_STAR_OFF = "star_off";

    public static String ICON_FAIL = "fail";
    public static String ICON_SUCCESS = "success";

    public static String LOBBY_MODAL = "lobbymodal";

    public static String CAR_ENGINE = "audio/car-engine.wav";
    public static String CAR_IDLE = "audio/car-idle.ogg";
    //    public static String CAR_STARTUP = "audio/car-startup.wav";
    public static String CAR_DRIFT = "audio/car-drift.ogg";
    public static String CAR_CRASH = "audio/car-crash.ogg";
    public static String CAR_CRASH_SMALL = "audio/car-crash_1.ogg";

    public static String GOAL_REACHED = "audio/goal_reached.wav";
    public static String GOAL_FAILED = "audio/goal_failed.wav";

    public static String CLICK = "audio/click.wav";
    public static String DISABLED = "audio/disabled.wav";
    public static String SWITCH = "audio/switch.wav";

    public static String BEEP_READY = "audio/beep_ready.wav";
    public static String BEEP_GO = "audio/beep_go.wav";

    public static String MUSIC = "audio/music.mp3";

    public static String LOC_BUNDLE = "MyBundle";

    public static String SHADER_BLUR_VERTEX = "shaders/BlurVertex";
    public static String SHADER_BLUR_FRAGMENT = "shaders/BlurFragment";

    public static String SHADER_SHADOW_VERTEX = "shaders/ShadowVertex";
    public static String SHADER_SHADOW_FRAGMENT = "shaders/ShadowFragment";

    public static String MAP_1 = "maps/map.tmx";
    public static String MAP_2 = "maps/map_2.tmx";


    public enum Maps {
        Map1, Map2, Map3;

        public String getPath() {
            switch (this) {
                case Map1:
                    return "maps/map.tmx";
                case Map2:
                    return "maps/map_2.tmx";
                case Map3:
                    return "maps/map_3.tmx";
                default:
                    return "maps/map.tmx";
            }
        }
    }
}
