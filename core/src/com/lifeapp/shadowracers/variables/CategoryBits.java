package com.lifeapp.shadowracers.variables;

/**
 * Created by Александр on 16.01.2018.
 */

public class CategoryBits {
    public static short NONE = 0;
    public static short DEFAULT = 1;
    public static short CAR = 2;
    public static short OBJECT = 4;
    public static short GOAL = 8;
    public static short PLAYER = 16;
    public static short NPC_CAR = 32;
    public static short GHOST = 64;
}
