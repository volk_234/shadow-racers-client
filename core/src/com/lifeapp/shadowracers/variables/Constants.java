package com.lifeapp.shadowracers.variables;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

/**
 * Created by Александр on 11.12.2017.
 */

public class Constants {
    public static int NET_VERSION = 6;

    public static float W1 = Gdx.graphics.getWidth() * 0.1f;
    public static float H1 = Gdx.graphics.getHeight() * 0.1f;

    public static float UI_PAD = H1 * 0.33f;

    public static final int RAYS = 32;
    public static final int PPT = (int) ((float) Gdx.graphics.getWidth() / 32);
    public static final float WORLD_WIDTH = 300 * PPT;
    public static final float WORLD_HEIGHT = 300 * PPT;
    public static final float OBJECT_VELOCITY_DAMPING = 2;


    public static Color AMBIENT_LIGHT = new Color(0x0606A255);

    public static Color[] PLAYER_COLORS = {
            Color.RED,
            new Color(0x8573f3ff),
            Color.YELLOW,
            Color.PINK,
            Color.GREEN
    };

    public static final int MISSIONS_COUNT = 24;

    public static Color COLOR_BLUE = new Color(0x8573f3ff);
    public static Color COLOR_RED = new Color(0xc72424ff);
    public static Color COLOR_GRAY = new Color(0x787878ff);
    public static Color COLOR_YELLOW = new Color(0xffff00ff);

    public static float DriftThredshold = 50;
}
