package com.lifeapp.shadowracers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 11.12.2017.
 */

public class MySkin extends Skin {

    public MySkin(AssetManager assetManager) {
        super(assetManager.get(Assets.ATLAS_UI, TextureAtlas.class));

        add("default", assetManager.get("default.otf", BitmapFont.class));
        add("small", assetManager.get("small.otf", BitmapFont.class));
        add("big", assetManager.get("big.otf", BitmapFont.class));

        getFont("small").getData().markupEnabled = true;

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle(
                null,
                null,
                null,
                getFont("default")
        );
        textButtonStyle.fontColor = new Color(0xffffffff);
        textButtonStyle.downFontColor = new Color(0xffff00ff);
        add("default", textButtonStyle);

        Label.LabelStyle labelStyle = new Label.LabelStyle(
                getFont("default"),
                Color.WHITE
        );
        add("default", labelStyle);

        labelStyle = new Label.LabelStyle(
                getFont("small"),
                Color.WHITE
        );
        add("small", labelStyle);

        labelStyle = new Label.LabelStyle(
                getFont("big"),
                Color.WHITE
        );
        add("big", labelStyle);

        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(
                getFont("default"),
                Color.BLACK,
                newDrawable(Assets.PIXEL, Color.BLACK),
                newDrawable(Assets.PIXEL, Color.WHITE),
                newDrawable(Assets.PIXEL, Color.WHITE));
        textFieldStyle.background.setLeftWidth(20);
        add("default", textFieldStyle);

        CheckBox.CheckBoxStyle checkBoxStyle = new CheckBox.CheckBoxStyle(
                getDrawable(Assets.CHECKBOX_OFF),
                getDrawable(Assets.CHECKBOX_ON),
                getFont("default"),
                Color.WHITE
        );
        add("default", checkBoxStyle);

        ProgressBar.ProgressBarStyle progressBarStyle = new ProgressBar.ProgressBarStyle(
                newDrawable(Assets.PIXEL, Color.BLACK),
                newDrawable(Assets.PIXEL, Constants.COLOR_YELLOW)
        );
        progressBarStyle.background.setMinHeight(Constants.H1 * 0.5f);
        progressBarStyle.knob.setMinHeight(Constants.H1 * 0.5f);
        progressBarStyle.knob.setMinWidth(5);
        progressBarStyle.knobBefore = progressBarStyle.knob;

//        progressBarStyle.
        add("default-horizontal", progressBarStyle);
    }
}
