package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.goals.AdvGoal;
import com.lifeapp.shadowracers.actors.goals.BestDriftGoal;
import com.lifeapp.shadowracers.actors.goals.CollisionGoal;
import com.lifeapp.shadowracers.actors.goals.GoalPointer;
import com.lifeapp.shadowracers.actors.goals.GoalsCountGoal;
import com.lifeapp.shadowracers.actors.goals.TimeLeftGoal;
import com.lifeapp.shadowracers.actors.goals.TotalDriftGoal;
import com.lifeapp.shadowracers.actors.mapobjects.Goal;
import com.lifeapp.shadowracers.actors.modals.campaign.LevelEndModal;
import com.lifeapp.shadowracers.network.PlayerModel;
import com.lifeapp.shadowracers.utils.AdUtils;
import com.lifeapp.shadowracers.utils.FireBaseUtils;
import com.lifeapp.shadowracers.utils.MissionsUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.CategoryBits;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Александр on 20.12.2017.
 */

public class CampaignMode extends Mode {


    private int nextGoalIndex = 0;
    private int currentLap = 1;
    private Goal goal;
    private GoalPointer goalPointer;

    private VerticalGroup advGoalGroup;

    private MissionsUtils.Mission mission;

    private MissionsUtils.GhostSave currentGhostSave;
    private BaseCar ghostCar;


    public CampaignMode(MissionsUtils.Mission mission) {
        super(mission.map);
        this.mission = mission;
        PlayerModel playerModel = StatsUtils.getPlayerModel();

        playerCar = makeCar(playerModel, mission.spawn);
        playerCar.controlByPlayer();

        MissionsUtils.GhostSave ghostSave = mission.getGhost();
        if (ghostSave != null) {
            ghostCar = makeCar(ghostSave.playerModel, -1, false, false, false);
            ghostCar.getBody().getFixtureList().first().setUserData(CategoryBits.NONE);
            Filter filter = ghostCar.getBody().getFixtureList().first().getFilterData();
            filter.maskBits = 0;
            filter.categoryBits = CategoryBits.GHOST;
            for (BaseCar.Wheel wheel : ghostCar.getWheels()) {
                wheel.getBody().getFixtureList().first().setFilterData(filter);
            }
            ghostCar.getBody().getFixtureList().first().setFilterData(filter);
            ghostCar.getBody().setType(BodyDef.BodyType.StaticBody);
            ghostCar.getColor().a = 0.4f;
        }

        currentGhostSave = new MissionsUtils.GhostSave();
        currentGhostSave.playerModel = playerModel;

        ImageButton pauseButton = new ImageButton(MyGame.getSkin()
                .newDrawable(Assets.ICON_PAUSE, new Color(0xffffffaa)));
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.gameScreen.pause();
                SoundUtils.playClick();
            }
        });
        getUiGroup().addActor(pauseButton);
        pauseButton.setSize(Constants.H1, Constants.H1);
        pauseButton.setPosition(Gdx.graphics.getWidth() - Constants.UI_PAD,
                Gdx.graphics.getHeight() - Constants.UI_PAD, Align.topRight);

        advGoalGroup = new VerticalGroup();
        advGoalGroup.setPosition(Constants.UI_PAD, Gdx.graphics.getHeight() - Constants.UI_PAD, Align.topLeft);
        advGoalGroup.left().top();
        advGoalGroup.columnLeft();
        getUiGroup().addActor(advGoalGroup);
        buildMission();
        initControl();
        getUiGroup().setVisible(false);

        AtomicInteger timeToStart = new AtomicInteger(4);
        RepeatAction tickAction = Actions.repeat(4,
                Actions.delay(1,
                        Actions.run(() -> {
                            timeToStart.addAndGet(-1);
                            switch (timeToStart.get()) {
                                case 0:
                                    resetGameInfo();
                                    addAction(Actions.forever(Actions.delay(0.03f, Actions.run(() ->
                                            currentGhostSave.add(getGameInfo().passTime,
                                                    playerCar.getBody().getPosition(),
                                                    playerCar.getBody().getAngle())))));
                                    if (ghostCar != null) {
                                        ghostCar.addAction(Actions.forever(Actions.run(() ->
                                                ghostSave.update(getGameInfo().passTime, ghostCar))));
                                    }
                                    SoundUtils.play(Assets.BEEP_GO, 1);
                                    getUiGroup().setVisible(true);
                                    break;
                                case 1:
                                    SoundUtils.play(Assets.BEEP_READY, 1);
                                    showMessage(String.valueOf(timeToStart.get()), 0.75f, Constants.COLOR_RED);
                                    break;
                                default:
                                    SoundUtils.play(Assets.BEEP_READY, 1);
                                    showMessage(String.valueOf(timeToStart.get()), 0.75f, Constants.COLOR_YELLOW);
                            }
                        })
                ));
        addAction(tickAction);
    }

    @Override
    public void update(float delta) {
        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (!paused) {
            setCameraAtPlayer();
        }
    }

    @Override
    public void goalReached() {
        SoundUtils.play(Assets.GOAL_REACHED, 0.5f);
        goal.showParticle();
        if (goalPointer != null) {
            goalPointer.remove();
        }
        addToQueue(() -> goal.dispose());
        addToQueue(this::nextGoal);

        super.goalReached();
    }

    @Override
    public void goalFailed() {
        SoundUtils.play(Assets.GOAL_FAILED, 1);
        showMessage(game.i18NBundle.get("fail"), 1, Color.RED);
        if (goalPointer != null) {
            goalPointer.remove();
        }
        addToQueue(() -> goal.dispose());
        addToQueue(this::nextGoal);
    }


    private void nextGoal() {
        if (nextGoalIndex >= mission.goals.length) {
            if ((mission.laps > 0) && (currentLap < mission.laps)) {
                nextGoalIndex = 0;
                currentLap++;
            } else {
                end();
                return;
            }
        }

        int goalId = mission.goals[nextGoalIndex];

        Vector2 goalPos = findGoalById(goalId).getRectangle().getCenter(new Vector2()).scl(1f / 32);
        goal = new Goal(world, goalPos, 0, rayHandler);
        addActor(goal);
        goal.toBack();

        goalPointer = new GoalPointer(mission.goalPointerType, goal);
        getUiGroup().addActor(goalPointer);
        goalPointer.setPosition(Gdx.graphics.getWidth() * 0.5f, Gdx.graphics.getHeight() - Constants.UI_PAD);

        nextGoalIndex++;

        if (nextGoalIndex < mission.goals.length) {
            Vector2 nextGoalPos = findGoalById(mission.goals[nextGoalIndex]).getRectangle()
                    .getCenter(new Vector2()).scl(1f / 32);
            goal.makeArrow(90 + goalPos.sub(nextGoalPos).angle());
        }
    }

    private void buildMission() {
        AdvGoal advGoal = null;
        switch (mission.variableType) {
            case BestDrift:
                advGoal = new BestDriftGoal(this, mission.variable);
                break;
            case Time:
                advGoal = new TimeLeftGoal(this, mission.variable);
                break;
            case TotalDrift:
                advGoal = new TotalDriftGoal(this, mission.variable);
                break;
        }
        advGoalGroup.addActor(advGoal);

        if (mission.time > 0) {
            TimeLeftGoal timeLeftGoal = new TimeLeftGoal(this, mission.time);
            advGoalGroup.addActor(timeLeftGoal);
        }

        int totalGoals;
        if (mission.laps > 0) {
            totalGoals = mission.goals.length * mission.laps;
        } else {
            totalGoals = mission.goals.length;
        }
        GoalsCountGoal goalsCountGoal = new GoalsCountGoal(this, totalGoals);
        advGoalGroup.addActor(goalsCountGoal);

        if (mission.totalDrift > 0) {
            TotalDriftGoal totalDriftGoal = new TotalDriftGoal(this, mission.totalDrift);
            advGoalGroup.addActor(totalDriftGoal);
        }

        if (mission.bestDrift > 0) {
            BestDriftGoal bestDriftGoal = new BestDriftGoal(this, mission.bestDrift);
            advGoalGroup.addActor(bestDriftGoal);
        }

        if (mission.collisions > -1) {
            CollisionGoal collisionGoal = new CollisionGoal(this, mission.collisions);
            advGoalGroup.addActor(collisionGoal);
        }

        if (mission.goals.length > 0) {
            addToQueue(this::nextGoal);
        }
    }

    private void end() {
        paused = true;
        getUiGroup().clear();
        getRoot().clearActions();
        playerCar.successDrift();
        playerCar.updateLoops(0, 0);
        GameInfo gameInfo = getGameInfo();

        MissionsUtils.Mission.Progress currentProgress = mission.getProgressForGameInfo(gameInfo);

        LevelEndModal levelEndModal = new LevelEndModal(gameInfo, mission);
        showModal(levelEndModal);

        if (currentProgress.stars > 0) {
            if (mission.isNewProgressBetter(currentProgress)) {
                mission.saveGhost(currentGhostSave);
                FireBaseUtils.putScoreForLevel(mission.index, gameInfo.getAsLong(mission.variableType));
                int incValue = (currentProgress.stars - mission.savedProgress.stars) * 150 +
                        (currentProgress.stars == 3 ? 150 : 0);
                StatsUtils.modifyMoney(incValue);
                levelEndModal.getMoneyLabel().modify(incValue);
                levelEndModal.updateBestScore(String.format("BEST\n%s",
                        mission.variableType.getStringFromLong(currentProgress.record)));
                mission.saveProgress(currentProgress);
            }
            if (MissionsUtils.getProgress(mission.index + 1) == null) {
                MissionsUtils.saveProgress(mission.index + 1, new MissionsUtils.Mission.Progress());
            }
        }

        AdUtils.MissionEnd();
    }

    @Override
    public void control(Vector2 control) {
        super.control(control);
    }

    public void restart() {
        addToQueue(() -> screen.initMode(new CampaignMode(mission)));
    }
}
