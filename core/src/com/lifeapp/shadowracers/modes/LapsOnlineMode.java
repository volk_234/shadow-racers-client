package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.mapobjects.Goal;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.modals.InfoModal;
import com.lifeapp.shadowracers.network.OnlinePlayer;
import com.lifeapp.shadowracers.network.ServerConnection;
import com.lifeapp.shadowracers.network.messages.GameInitMessage;
import com.lifeapp.shadowracers.network.messages.GoalReachedMessage;
import com.lifeapp.shadowracers.network.messages.PlayerExitMessage;
import com.lifeapp.shadowracers.network.messages.PlayerReadyMessage;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.ArrayList;


/**
 * Created by Александр on 20.12.2017.
 */

public class LapsOnlineMode extends OnlineMode {
    private Goal goal;
    private Image arrow;
    private ArrayList<Integer> goals;
    private int totalLapsCount;
    private int currentGoal = 0;
    private int currentLap = 1;
    private Label lapsLabel;

    LapsOnlineMode(ServerConnection serverConnection, GameInitMessage gameInitMessage) {
        super(serverConnection);

        arrow = new Image(MyGame.getTextureRegion(Assets.ATLAS_UI, Assets.ARROW));
        arrow.setOrigin(Align.center);
        arrow.setVisible(false);

        lapsLabel = new Label(game.i18NBundle.format("lap", 0, 0), MyGame.getSkin());
        lapsLabel.setVisible(false);

        VerticalGroup verticalGroup = new VerticalGroup();
        verticalGroup.addActor(arrow);
        verticalGroup.addActor(lapsLabel);
        getUiGroup().addActor(verticalGroup);
        verticalGroup.invalidate();
        verticalGroup.setPosition(Constants.W1 * 5 - verticalGroup.getPrefWidth() * 0.5f,
                Gdx.graphics.getHeight() - verticalGroup.getHeight() - Constants.UI_PAD);

        initPlayers(gameInitMessage);
        goals = new ArrayList<>();
        for (int i : gameInitMessage.goals) {
            goals.add(i);
        }
        totalLapsCount = goals.get(0);
        goals.remove(0);
        addToQueue(this::newGoal);
        arrow.setVisible(true);
        lapsLabel.setVisible(true);

        InfoModal infoModal = new InfoModal(game.i18NBundle.get("lapsMode"), 5);
        showModal(infoModal);

        sendTcp(new PlayerReadyMessage());

        for (OnlinePlayer onlinePlayer : playersMap.values()) {
            playersGroup.addActor(onlinePlayer.getScoreLabel());
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (!paused) {
            setCameraAtPlayer();
        }
        if (goal != null) {
            arrow.setRotation(90 + getAngleFromPlayer(goal.getBody().getPosition()));
        }
    }

    @Override
    void gameStarted() {

    }

    @Override
    void onlinePlayerGoalReached(GoalReachedMessage goalReachedMessage) {
        OnlinePlayer onlinePlayer = playersMap.get(goalReachedMessage.name);

        onlinePlayer.score += goalReachedMessage.value;
        onlinePlayer.updateLabel();
        updatePlayerGroup();
    }

    @Override
    void playerExit(PlayerExitMessage playerExitMessage) {
        super.playerExit(playerExitMessage);
        updatePlayerGroup();
    }

    private void updatePlayerGroup() {
        playersGroup.getChildren().sort((left, right) ->
                (int) (((OnlinePlayer) right.getUserObject()).score - ((OnlinePlayer) left.getUserObject()).score));
    }

    private void initPlayers(GameInitMessage gameInitMessage) {
        int[] spawns = gameInitMessage.spawns;

        for (int i = 0; i < gameInitMessage.playerModels.length; i++) {
            BaseCar car = makeCar(gameInitMessage.playerModels[i], spawns[i]);
            OnlinePlayer onlinePlayer = new OnlinePlayer(car, gameInitMessage.playerModels[i], Constants.PLAYER_COLORS[i]);
            playersMap.put(onlinePlayer.getPlayerModel().name, onlinePlayer);

            if (onlinePlayer.getPlayerModel().name.equals(nickname)) {
                car.controlByPlayer();
                playerCar = car;
            }
        }

        updatePlayerGroup();
    }

    public void goalReached() {
        super.goalReached();
        GoalReachedMessage goalReachedMessage = new GoalReachedMessage();
        goalReachedMessage.value = 1;
        sendTcp(goalReachedMessage);
        currentGoal++;
        if (currentGoal < goals.size() || currentLap < totalLapsCount) {
            addToQueue(this::newGoal);
        }
        SoundUtils.play(Assets.GOAL_REACHED, 0.5f);
        goal.showParticle();
        goal.dispose();
    }

    private void newGoal() {
        if (currentGoal >= goals.size()) {
            currentGoal = 0;
            currentLap++;
        }

        Vector2 goalPos = findGoalById(goals.get(currentGoal))
                .getRectangle().getCenter(new Vector2()).scl(1f / 32);
        goal = new Goal(world, goalPos, 0, rayHandler);

        int nextGoalIndex = currentGoal + 1 < goals.size() ? currentGoal + 1 : 0;
        Vector2 nextGoalPos = findGoalById(goals.get(nextGoalIndex)).getRectangle()
                .getCenter(new Vector2()).scl(1f / 32);
        goal.makeArrow(90 + goalPos.sub(nextGoalPos).angle());

        addActor(goal);
        goal.toBack();
        String scr = game.i18NBundle.format("lap", currentLap, totalLapsCount);
        lapsLabel.setText(scr);
    }
}
