package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.modals.InfoModal;
import com.lifeapp.shadowracers.network.OnlinePlayer;
import com.lifeapp.shadowracers.network.ServerConnection;
import com.lifeapp.shadowracers.network.messages.GameInitMessage;
import com.lifeapp.shadowracers.network.messages.GoalReachedMessage;
import com.lifeapp.shadowracers.network.messages.PlayerExitMessage;
import com.lifeapp.shadowracers.network.messages.PlayerReadyMessage;
import com.lifeapp.shadowracers.variables.Constants;


/**
 * Created by Александр on 20.12.2017.
 */

public class TotalDriftOnlineMode extends OnlineMode {
    private Label driftLabel;
    private float totalDrift = 0;

    private Label timeLabel;
    private int timeLeft;

    TotalDriftOnlineMode(ServerConnection serverConnection, GameInitMessage gameInitMessage) {
        super(serverConnection);

        VerticalGroup verticalGroup = new VerticalGroup();
        verticalGroup.setPosition(Constants.W1 * 5 - verticalGroup.getPrefWidth() * 0.5f,
                Gdx.graphics.getHeight() - verticalGroup.getHeight() - Constants.UI_PAD);
        getUiGroup().addActor(verticalGroup);

        timeLeft = gameInitMessage.goals[0];
        timeLabel = new Label(game.i18NBundle.format("time", timeLeft),
                MyGame.getSkin(), "small", Color.WHITE);
        verticalGroup.addActor(timeLabel);

        driftLabel = new Label("", MyGame.getSkin(), "small", Color.WHITE);
        verticalGroup.addActor(driftLabel);

        initPlayers(gameInitMessage);

        InfoModal infoModal = new InfoModal(game.i18NBundle.get("totalDriftMode"), 5);
        showModal(infoModal);

        sendTcp(new PlayerReadyMessage());

        for (OnlinePlayer onlinePlayer : playersMap.values()) {
            playersGroup.addActor(onlinePlayer.getScoreLabel());
        }

        updateDriftLabel();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        setCameraAtPlayer();
    }

    @Override
    void gameStarted() {
        timeLabel.addAction(Actions.forever(
                Actions.sequence(
                        Actions.delay(1),
                        Actions.run(() -> {
                            timeLeft--;
                            timeLabel.setText(game.i18NBundle.format("time", timeLeft));
                        })
                )
        ));
    }

    @Override
    void onlinePlayerGoalReached(GoalReachedMessage goalReachedMessage) {
        OnlinePlayer onlinePlayer = playersMap.get(goalReachedMessage.name);

        onlinePlayer.score += goalReachedMessage.value;
        onlinePlayer.updateLabel();
        updatePlayerGroup();
    }

    @Override
    void playerExit(PlayerExitMessage playerExitMessage) {
        super.playerExit(playerExitMessage);
        updatePlayerGroup();
    }

    private void updatePlayerGroup() {
        playersGroup.getChildren().sort((left, right) ->
                (int) (((OnlinePlayer) right.getUserObject()).score - ((OnlinePlayer) left.getUserObject()).score));
    }

    private void updateDriftLabel() {
        driftLabel.setText(game.i18NBundle.format("driftTotalScore", totalDrift));
    }

    private void initPlayers(GameInitMessage gameInitMessage) {
        int[] spawns = gameInitMessage.spawns;

        for (int i = 0; i < gameInitMessage.playerModels.length; i++) {
            BaseCar car = makeCar(gameInitMessage.playerModels[i], spawns[i]);
            OnlinePlayer onlinePlayer = new OnlinePlayer(car, gameInitMessage.playerModels[i], Constants.PLAYER_COLORS[i]);
            playersMap.put(onlinePlayer.getPlayerModel().name, onlinePlayer);

            if (onlinePlayer.getPlayerModel().name.equals(nickname)) {
                car.controlByPlayer();
                playerCar = car;
            }
        }

        updatePlayerGroup();
    }

    @Override
    public void successDrift(float scr) {
        super.successDrift(scr);
        totalDrift += scr;
        GoalReachedMessage goalReachedMessage = new GoalReachedMessage();
        goalReachedMessage.value = scr;
        sendTcp(goalReachedMessage);
        updateDriftLabel();
    }
}
