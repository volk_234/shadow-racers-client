package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.goals.AdvGoal;
import com.lifeapp.shadowracers.actors.mapobjects.Goal;
import com.lifeapp.shadowracers.utils.AdUtils;
import com.lifeapp.shadowracers.utils.ShopUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 20.12.2017.
 */

public class SingleMode extends Mode {

    private Goal goal;
    private boolean goalType = false;
    private Image arrow;
    private Label goalDstValueLabel;
    private VerticalGroup goalDstGroup;

    private int goalsInRow = 0;

    private float timeLeft = 0;

    private VerticalGroup advGoalGroup;

    private Label scoreLabel;

    public SingleMode() {
        super(Assets.Maps.Map1);

        String usedCar = ShopUtils.getUsedCar();

        playerCar = makeCar(StatsUtils.getPlayerModel(), -1);
        playerCar.controlByPlayer();

        initControl();

        ImageButton pauseButton = new ImageButton(MyGame.getSkin()
                .newDrawable(Assets.ICON_PAUSE, new Color(0xffffffaa)));
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.gameScreen.pause();
                SoundUtils.playClick();
            }
        });
        getUiGroup().addActor(pauseButton);
        pauseButton.setSize(Constants.H1, Constants.H1);
        pauseButton.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - pauseButton.getHeight() - Constants.UI_PAD);

        arrow = new Image(MyGame.getTextureRegion(Assets.ATLAS, Assets.ARROW));
        arrow.setSize(Constants.H1, Constants.H1);
        arrow.setOrigin(Align.center);
        arrow.setVisible(false);
        arrow.setPosition(Gdx.graphics.getWidth() * 0.5f - arrow.getWidth() * 0.5f,
                Gdx.graphics.getHeight() - 20 - arrow.getHeight());
        getUiGroup().addActor(arrow);

        goalDstGroup = new VerticalGroup();
        goalDstValueLabel = new Label("0", MyGame.getSkin(), "big", new Color(0xfffd48ff));
        goalDstValueLabel.setAlignment(Align.center);
        goalDstGroup.addActor(goalDstValueLabel);
        Label goalDstLabel = new Label(game.i18NBundle.get("distanceToGoal"), MyGame.getSkin(), "small", Color.WHITE);
        goalDstLabel.setAlignment(Align.center);
        goalDstLabel.setFontScale(0.5f);
        goalDstGroup.addActor(goalDstLabel);
        goalDstGroup.setVisible(false);
        goalDstGroup.setPosition(Gdx.graphics.getWidth() * 0.5f - goalDstGroup.getWidth() * 0.5f,
                Gdx.graphics.getHeight());
        getUiGroup().addActor(goalDstGroup);


        HorizontalGroup horizontalGroup = new HorizontalGroup();
        Image coin = new Image(MyGame.getTextureRegion(Assets.ATLAS, Assets.COIN));
        horizontalGroup.addActor(new Container<>(coin).size(Constants.H1));
        scoreLabel = new Label(String.valueOf(StatsUtils.getMoney()),
                MyGame.getSkin(), "default", new Color(0xffffffff));
        horizontalGroup.addActor(scoreLabel);
        horizontalGroup.setPosition(Gdx.graphics.getWidth() - 20 - horizontalGroup.getWidth(),
                Gdx.graphics.getHeight() - 20);
        horizontalGroup.space(20).right().top();
        getUiGroup().addActor(horizontalGroup);

        advGoalGroup = new VerticalGroup();
        advGoalGroup.setPosition(20, pauseButton.getY() - advGoalGroup.getPrefHeight());
        advGoalGroup.left();
        getUiGroup().addActor(advGoalGroup);
        addToQueue(() -> newGoal(MathUtils.random(0, 1)));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (goal != null) {
            if (goalType)
                arrow.setRotation(90 + playerCar.getBody().getPosition().cpy().sub(goal.getBody().getPosition()).angle());
            else
                goalDstValueLabel.setText(String.valueOf((int) (playerCar.getBody().getPosition().cpy().dst(goal.getBody().getPosition()))));
        }
        setCameraAtPlayer();
    }

    @Override
    public void goalReached() {
        for (Actor actor : advGoalGroup.getChildren()) {
            if (actor instanceof AdvGoal && ((AdvGoal) actor).isFailed()) {
                ((AdvGoal) actor).blink();
                SoundUtils.playDisabled();
                return;
            }
        }

        SoundUtils.play(Assets.GOAL_REACHED, 0.5f);
        goalsInRow++;
        switch (goalsInRow) {
            case 3:
                showMessage(game.i18NBundle.format("inRow", 3), 1, Color.YELLOW);
                addAction(Actions.delay(
                        1.2f,
                        Actions.run(() -> {
                            showMessage(game.i18NBundle.format("addMoney", 200), 1, Color.WHITE);
                        })
                ));
                break;
            case 5:
                showMessage(game.i18NBundle.format("inRow", 5), 1, Color.YELLOW);
                addAction(Actions.delay(
                        1.2f,
                        Actions.run(() -> {
                            showMessage(game.i18NBundle.format("addMoney", 350), 1, Color.RED);
                        })
                ));
                break;
            case 7:
                showMessage(game.i18NBundle.format("inRow", 7), 1, Color.YELLOW);
                addAction(Actions.delay(
                        1.2f,
                        Actions.run(() -> {
                            showMessage(game.i18NBundle.format("addMoney", 700), 1, Color.RED);
                        })
                ));
                break;
        }
        goal.showParticle();
        goal.dispose();
        addToQueue(() -> newGoal(MathUtils.random(0, 1)));

        AdUtils.MissionEnd();
        super.goalReached();
    }

    @Override
    public void goalFailed() {
        SoundUtils.play(Assets.GOAL_FAILED, 1);
        goalsInRow = 0;
        showMessage(game.i18NBundle.get("fail"), 1, Color.RED);
        goal.dispose();
        addToQueue(() -> newGoal(MathUtils.random(0, 1)));
    }


    private void newGoal(int type) {
        advGoalGroup.clear();
        resetGameInfo();

        goalType = type == 1;
        arrow.setVisible(goalType);
        goalDstGroup.setVisible(!goalType);

        Vector2 goalPos = findGoalByIndex(-1).getRectangle().getCenter(new Vector2()).scl(1f / 32);
        goal = new Goal(world, goalPos, 0, rayHandler);
        addActor(goal);
        goal.toBack();

        float addTime = generateAdvGoals();

        float dst = playerCar.getBody().getPosition().cpy().dst(goal.getBody().getPosition());
        timeLeft = dst / 5 + addTime;
        timeLeft += 5 / StatsUtils.getDifficultyCoef();
        final Label timeLeftLabel = new Label(game.i18NBundle.format("time", timeLeft),
                MyGame.getSkin(), "small", new Color(0xffffff66));
        goal.addAction(Actions.forever(
                Actions.sequence(
                        Actions.delay(1),
                        Actions.run(() -> {
                            timeLeft--;
                            if (timeLeft < 0) {
                                goalFailed();
                            } else {
                                timeLeftLabel.setText(game.i18NBundle.format("time", timeLeft));
                            }
                        })
                )));
        advGoalGroup.addActorAt(0, timeLeftLabel);
        advGoalGroup.columnLeft();
    }

    private float generateAdvGoals() {
        float addTime = 0;
        Skin skin = MyGame.getSkin();
//        if (MathUtils.randomBoolean(0.2f)) {
//            AdvGoal advGoal = new TotalDriftGoal(this, skin);
//            addTime += advGoal.getAddTime();
//            advGoalGroup.addActor(advGoal);
//        }
//        if (MathUtils.randomBoolean(0.2f)) {
//            AdvGoal advGoal = new BestDriftGoal(this, skin);
//            addTime += advGoal.getAddTime();
//            advGoalGroup.addActor(advGoal);
//        }
//        if (MathUtils.randomBoolean(0.2f)) {
//            AdvGoal advGoal = new CollisionGoal(this, skin);
//            addTime += advGoal.getAddTime();
//            advGoalGroup.addActor(advGoal);
//        }
        return addTime;
    }

    private void updateScoreLabel() {
        scoreLabel.setText(String.valueOf(StatsUtils.getMoney()));
        scoreLabel.addAction(Actions.sequence(
                Actions.color(new Color(0xfff224ff), 0.2f),
                Actions.color(new Color(0xffffffff), 0.2f),
                Actions.color(new Color(0xfff224ff), 0.2f),
                Actions.color(new Color(0xffffffff), 0.2f)
        ));
    }
}
