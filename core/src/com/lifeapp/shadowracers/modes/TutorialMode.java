package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.goals.AdvGoal;
import com.lifeapp.shadowracers.actors.goals.TotalDriftGoal;
import com.lifeapp.shadowracers.actors.mapobjects.Goal;
import com.lifeapp.shadowracers.actors.modals.InfoModal;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 20.12.2017.
 */

public class TutorialMode extends Mode {

    private Goal goal;
    private boolean goalType = false;
    private Image arrow;
    private Label goalDstValueLabel;
    private VerticalGroup goalDstGroup;

    private VerticalGroup advGoalGroup;
    private Label scoreLabel;

    private Runnable goalReachedRunnable;

    public TutorialMode() {
        super(Assets.Maps.Map1);

        Skin skin = MyGame.getSkin();

        playerCar = makeCar(StatsUtils.getPlayerModel(), -1);
        playerCar.controlByPlayer();

        initControl();

        ImageButton pauseButton = new ImageButton(skin.newDrawable(Assets.ICON_PAUSE, new Color(0xffffffaa)));
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.gameScreen.pause();
                SoundUtils.playClick();
            }
        });
        getUiGroup().addActor(pauseButton);
        pauseButton.setSize(Constants.H1, Constants.H1);
        pauseButton.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - pauseButton.getHeight() - Constants.UI_PAD);

        arrow = new Image(skin.getRegion(Assets.ARROW));
        arrow.setSize(Constants.H1, Constants.H1);
        arrow.setOrigin(Align.center);
        arrow.setVisible(false);
        arrow.setPosition(Gdx.graphics.getWidth() * 0.5f - arrow.getWidth() * 0.5f,
                Gdx.graphics.getHeight() - 20 - arrow.getHeight());
        getUiGroup().addActor(arrow);

        goalDstGroup = new VerticalGroup();
        goalDstValueLabel = new Label("0", skin, "big", new Color(0xfffd48ff));
        goalDstValueLabel.setAlignment(Align.center);
        goalDstGroup.addActor(goalDstValueLabel);
        Label goalDstLabel = new Label(game.i18NBundle.get("distanceToGoal"), skin, "small", Color.WHITE);
        goalDstLabel.setAlignment(Align.center);
        goalDstLabel.setFontScale(0.5f);
        goalDstGroup.addActor(goalDstLabel);
        goalDstGroup.setVisible(false);
        goalDstGroup.setPosition(Gdx.graphics.getWidth() * 0.5f - goalDstGroup.getWidth() * 0.5f,
                Gdx.graphics.getHeight());
        getUiGroup().addActor(goalDstGroup);

        HorizontalGroup horizontalGroup = new HorizontalGroup();
        Image coin = new Image(skin.getDrawable(Assets.COIN));
        horizontalGroup.addActor(new Container<>(coin).size(Constants.H1));
        scoreLabel = new Label(String.valueOf(StatsUtils.getMoney()),
                skin, "default", new Color(0xffffffff));
        horizontalGroup.addActor(scoreLabel);
        horizontalGroup.setPosition(Gdx.graphics.getWidth() - 20 - horizontalGroup.getWidth(),
                Gdx.graphics.getHeight() - 20);
        horizontalGroup.space(20).right().top();
        getUiGroup().addActor(horizontalGroup);

        advGoalGroup = new VerticalGroup();
        advGoalGroup.setPosition(20, pauseButton.getY() - advGoalGroup.getPrefHeight());
        advGoalGroup.left();
        getUiGroup().addActor(advGoalGroup);

        InfoModal infoModal = new InfoModal(game.i18NBundle.get("tutorial_0")) {
            @Override
            public void clicked() {
                addToQueue(() -> newGoal(1));
                InfoModal infoModal1 = new InfoModal(game.i18NBundle.get("tutorial_1"));
                showModal(infoModal1);
                goalReachedRunnable = () -> {
                    InfoModal infoModal2 = new InfoModal(game.i18NBundle.get("tutorial_2")) {
                        @Override
                        public void clicked() {
                            InfoModal infoModal3 = new InfoModal(game.i18NBundle.get("tutorial_3"));
                            showModal(infoModal3);
                            addToQueue(() -> newGoal(0));
                            goalReachedRunnable = () -> {
                                resetGameInfo();
                                InfoModal infoModal4 = new InfoModal(game.i18NBundle.get("tutorial_4"));
                                showModal(infoModal4);
                                addToQueue(() -> {
                                    newGoal(1);
                                    AdvGoal advGoal = new TotalDriftGoal(TutorialMode.this, 600);
                                    advGoalGroup.addActor(advGoal);
                                });
                                goalReachedRunnable = () -> {
                                    StatsUtils.tutorialCompleted();
                                    InfoModal infoModal5 = new InfoModal(game.i18NBundle.get("tutorial_5")) {
                                        @Override
                                        public void clicked() {
                                            game.gameScreen.initMode(new SingleMode());
                                        }
                                    };
                                    showModal(infoModal5);
                                };
                            };
                        }
                    };
                    showModal(infoModal2);
                };
            }
        };
        showModal(infoModal);

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (goal != null) {
            if (goalType)
                arrow.setRotation(90 + playerCar.getBody().getPosition().cpy().sub(goal.getBody().getPosition()).angle());
            else
                goalDstValueLabel.setText(String.valueOf((int) (playerCar.getBody().getPosition().cpy().dst(goal.getBody().getPosition()))));
        }

        if (!paused) {
            setCameraAtPlayer();
        }
    }

    private void newGoal(int type) {
        advGoalGroup.clear();

        goalType = type == 1;
        arrow.setVisible(goalType);
        goalDstGroup.setVisible(!goalType);

        Vector2 goalPos = findGoalByIndex(-1).getRectangle().getCenter(new Vector2()).scl(1f / 32);
        goal = new Goal(world, goalPos, 0, rayHandler);
        addActor(goal);
        goal.toBack();
    }


    public void goalReached() {
        for (Actor actor : advGoalGroup.getChildren()) {
            if (actor instanceof AdvGoal && ((AdvGoal) actor).isFailed()) {
                ((AdvGoal) actor).blink();
                SoundUtils.playDisabled();
                return;
            }
        }
        SoundUtils.play(Assets.GOAL_REACHED, 0.5f);
        goal.dispose();
        if (goalReachedRunnable != null) {
            goalReachedRunnable.run();
        }
    }

    private void updateScoreLabel() {
        scoreLabel.setText(String.valueOf(StatsUtils.getMoney()));
        scoreLabel.addAction(Actions.sequence(
                Actions.color(new Color(0xfff224ff), 0.2f),
                Actions.color(new Color(0xffffffff), 0.2f),
                Actions.color(new Color(0xfff224ff), 0.2f),
                Actions.color(new Color(0xffffffff), 0.2f)
        ));
    }
}
