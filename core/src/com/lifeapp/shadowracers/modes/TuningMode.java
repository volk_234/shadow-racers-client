package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 20.12.2017.
 */

public class TuningMode extends Mode {
    private Label scoreLabel;

    public TuningMode() {
        super(Assets.Maps.Map1);

        playerCar = makeCar(StatsUtils.getPlayerModel(), 2);
        playerCar.controlByPlayer();

        HorizontalGroup scoreGroup = new HorizontalGroup();
        scoreGroup.space(20);
        Image coinImage = new Image(MyGame.getSkin().getRegion(Assets.COIN));
        coinImage.setSize(Constants.H1, Constants.H1);
        scoreGroup.addActor(new Container<>(coinImage).size(Constants.H1));
        scoreLabel = new Label(String.valueOf(StatsUtils.getMoney()), MyGame.getSkin());
        scoreGroup.addActor(scoreLabel);
        scoreGroup.setPosition(20, Gdx.graphics.getHeight() - 20);
        getUiGroup().addActor(scoreGroup);
        scoreGroup.space(20).left().top();

        playerCar.act(0);

//        toModal(TuningModal.class);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }


    public void updateScore() {
        scoreLabel.setText(String.valueOf(StatsUtils.getMoney()));
    }

//    public void toModal(Class clazz) {
//        String usedCar = ShopUtils.getUsedCar();
//        if (clazz == TuningModal.class) {
//            Vector2 cameraPos = new Vector2(playerCar.getX() + Constants.W1 * 2.5f, playerCar.getY());
//            setCameraTarget(cameraPos, 1);
//            showModal(new TuningModal(this));
//        } else if (clazz == ColoringModal.class) {
//            Vector2 cameraPos = new Vector2(playerCar.getX() + Constants.W1 * 2.5f / 2f, playerCar.getY());
//            setCameraTarget(cameraPos, 1.5f);
//            showModal(new ColoringModal(this));
//        } else if (clazz == LightsModal.class) {
//            Vector2 cameraPos = new Vector2(playerCar.getX() + Constants.W1 * 2.5f / 2f, playerCar.getY());
//            setCameraTarget(cameraPos, 0.8f);
//            showModal(new LightsModal(this));
//        } else if (clazz == NeonsModal.class) {
//            Vector2 cameraPos = new Vector2(playerCar.getX() + Constants.W1 * 2.5f / 2f, playerCar.getY());
//            setCameraTarget(cameraPos, 1.5f);
//            showModal(new NeonsModal(this));
//        }
//
//        playerCar.changeCarColor(ShopUtils.getColorForCar(usedCar));
//        playerCar.changeLightsColor(ShopUtils.getLightsForCar(usedCar));
//        playerCar.changeNeonsColor(ShopUtils.getNeonsForCar(usedCar));
//    }
}
