package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.modals.menu.PauseModal;
import com.lifeapp.shadowracers.actors.modals.network.FailConnectionModal;
import com.lifeapp.shadowracers.actors.modals.network.GameEndModal;
import com.lifeapp.shadowracers.actors.modals.network.NetworkErrorModal;
import com.lifeapp.shadowracers.network.OnlinePlayer;
import com.lifeapp.shadowracers.network.ServerConnection;
import com.lifeapp.shadowracers.network.messages.ErrorMessage;
import com.lifeapp.shadowracers.network.messages.GameEndMessage;
import com.lifeapp.shadowracers.network.messages.GameStartMessage;
import com.lifeapp.shadowracers.network.messages.GoalReachedMessage;
import com.lifeapp.shadowracers.network.messages.PlayerExitMessage;
import com.lifeapp.shadowracers.network.messages.StateMessage;
import com.lifeapp.shadowracers.utils.AdUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.HashMap;


/**
 * Created by Александр on 20.12.2017.
 */

public abstract class OnlineMode extends Mode {

    protected enum GameStatus {LOBBY, PLAYING, FINISHED}

    private ServerConnection connection;

    VerticalGroup playersGroup;
    int playerPlace = -1;

    private GameStatus gameStatus = GameStatus.LOBBY;
    String nickname = StatsUtils.getNickname();

    HashMap<String, OnlinePlayer> playersMap = new HashMap<>();

    OnlineMode(ServerConnection serverConnection) {
        super(Assets.Maps.Map1);

        ImageButton pauseButton = new ImageButton(MyGame.getSkin().newDrawable(Assets.ICON_PAUSE,
                new Color(0xffffffaa)));
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.gameScreen.pause();
                SoundUtils.playClick();
            }
        });
        getUiGroup().addActor(pauseButton);
        pauseButton.setSize(Constants.H1, Constants.H1);
        pauseButton.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - pauseButton.getHeight() - Constants.UI_PAD);

        playersGroup = new VerticalGroup();
        playersGroup.columnRight();
        getUiGroup().addActor(playersGroup);
        playersGroup.setPosition(Gdx.graphics.getWidth() - playersGroup.getPrefWidth() - Constants.UI_PAD,
                Gdx.graphics.getHeight() - playersGroup.getPrefHeight() - Constants.UI_PAD);
        playersGroup.right();

        connection = serverConnection;
        if (!connection.isAlive()) {
            FailConnectionModal failConnectionModal = new FailConnectionModal(screen);
            showModal(failConnectionModal);
            return;
        }

        connection.setListener(new Listener() {
            @Override
            public void disconnected(Connection connection) {
                OnlineMode.this.disconnected();
            }

            @Override
            public void received(Connection connection, final Object object) {
                if (object instanceof StateMessage) {
                    try {
                        StateMessage stateMessage = (StateMessage) object;
                        BaseCar car = playersMap.get(((StateMessage) object).name).getCar();
                        car.setControl(stateMessage.controlX, stateMessage.controlY);
                        Body stateBody = car.getBody();
                        Vector2 bodyPosition = stateBody.getPosition();
                        if (stateBody.getPosition().dst(stateMessage.positionX, stateMessage.positionY) > 1.5f) {
                            bodyPosition.x = stateMessage.positionX;
                            bodyPosition.y = stateMessage.positionY;
                        }
                        stateBody.setTransform(bodyPosition, stateMessage.angle);
                        stateBody.setLinearVelocity(stateMessage.speedX, stateMessage.speedY);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                } else if (object instanceof GoalReachedMessage) {
                    if (gameStatus == GameStatus.FINISHED) return;
                    onlinePlayerGoalReached((GoalReachedMessage) object);
                } else if (object instanceof GameEndMessage) {
                    gameStatus = GameStatus.FINISHED;
                    getUiGroup().setVisible(false);
                    GameEndModal gameEndModal = new GameEndModal(((GameEndMessage) object), playersMap.values());
                    showModal(gameEndModal);
                    AdUtils.onlinePlayed();
                } else if (object instanceof GameStartMessage) {
                    if (((GameStartMessage) object).timeLeft <= 0) {
                        SoundUtils.play(Assets.BEEP_GO, 1);
                        initControl();
                        showMessage(game.i18NBundle.get("go"), 1, Color.YELLOW);
                        gameStarted();
                    } else if (((GameStartMessage) object).timeLeft <= 3) {
                        SoundUtils.play(Assets.BEEP_READY, 1);
                        showMessage(String.valueOf(((GameStartMessage) object).timeLeft), 1, Color.RED);
                    } else {
                        SoundUtils.play(Assets.BEEP_READY, 1);
                        showMessage(String.valueOf(((GameStartMessage) object).timeLeft), 1, Color.WHITE);
                    }
                    enableMarkers();
                    enablePointers();
                    addAction(Actions.forever(Actions.delay(0.1f, Actions.run(
                            OnlineMode.this::sendState
                    ))));
                } else if (object instanceof PlayerExitMessage) {
                    playerExit((PlayerExitMessage) object);
                } else if (object instanceof ErrorMessage) {
                    OnlineMode.this.disconnected();
                }
            }
        });

    }

    abstract void gameStarted();

    abstract void onlinePlayerGoalReached(GoalReachedMessage goalReachedMessage);

    void playerExit(PlayerExitMessage playerExitMessage) {
        if (gameStatus == GameStatus.FINISHED) return;
        OnlinePlayer onlinePlayer = playersMap.get(playerExitMessage.nickname);
        onlinePlayer.disablePointer();
        onlinePlayer.getScoreLabel().remove();
        BaseCar car = onlinePlayer.getCar();
        if (car != null) {
            car.dispose();
        }
        playersMap.remove(playerExitMessage.nickname);
    }

    @Override
    public void dispose() {
        connection.close();
    }

    @Override
    public void control(Vector2 control) {
        super.control(control);
        sendState();
    }

    private void disconnected() {
        switch (gameStatus) {
            case LOBBY:
            case PLAYING:
                showModal(new NetworkErrorModal(new ErrorMessage()));
                break;
        }
    }

    void sendTcp(Object object) {
        connection.sendTCP(object);
    }

    private void sendUdp(Object object) {
        connection.sendUDP(object);
    }

    private void enableMarkers() {
        for (OnlinePlayer onlinePlayer : playersMap.values()) {
            if (!nickname.equals(onlinePlayer.getPlayerModel().name)) {
                onlinePlayer.enableMarker();
            }
        }
    }

    private void enablePointers() {
        for (OnlinePlayer onlinePlayer : playersMap.values()) {
            if (!nickname.equals(onlinePlayer.getPlayerModel().name)) {
                onlinePlayer.enablePointer();
            }
        }
    }

    private void sendState() {
        sendUdp(new StateMessage(playerCar.getName(), playerCar.getControl().x, playerCar.getControl().y,
                playerCar.getBody().getPosition().x, playerCar.getBody().getPosition().y,
                playerCar.getBody().getLinearVelocity().x, playerCar.getBody().getLinearVelocity().y,
                playerCar.getBody().getAngle()));
    }

    @Override
    public void pause() {
        if (getUiStage().getRoot().findActor("modal_0") != null) {
            return;
        }
        setBlur(3);
        PauseModal pauseModal = new PauseModal(screen);
        showModal(pauseModal);
    }

    @Override
    public void exit() {
        connection.close();
        getRoot().clearActions();
        paused = true;
    }
}
