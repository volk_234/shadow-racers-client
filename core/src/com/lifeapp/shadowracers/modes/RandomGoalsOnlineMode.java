package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.actors.mapobjects.Goal;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.modals.InfoModal;
import com.lifeapp.shadowracers.network.OnlinePlayer;
import com.lifeapp.shadowracers.network.ServerConnection;
import com.lifeapp.shadowracers.network.messages.GameInitMessage;
import com.lifeapp.shadowracers.network.messages.GoalReachedMessage;
import com.lifeapp.shadowracers.network.messages.PlayerExitMessage;
import com.lifeapp.shadowracers.network.messages.PlayerReadyMessage;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.ArrayList;


/**
 * Created by Александр on 20.12.2017.
 */

public class RandomGoalsOnlineMode extends OnlineMode {
    private Goal goal;
    private Image arrow;
    private ArrayList<Integer> goals;
    private int totalGoalsCount;
    private Label goalsCountLabel;

    RandomGoalsOnlineMode(ServerConnection serverConnection, GameInitMessage gameInitMessage) {
        super(serverConnection);

        arrow = new Image(MyGame.getTextureRegion(Assets.ATLAS, Assets.ARROW));
        arrow.setOrigin(Align.center);
        arrow.setVisible(false);

        goalsCountLabel = new Label(game.i18NBundle.format("lap", 0, 0), MyGame.getSkin());
        goalsCountLabel.setVisible(false);

        VerticalGroup verticalGroup = new VerticalGroup();
        verticalGroup.addActor(arrow);
        verticalGroup.addActor(goalsCountLabel);
        getUiGroup().addActor(verticalGroup);
        verticalGroup.invalidate();
        verticalGroup.setPosition(Constants.W1 * 5,
                Gdx.graphics.getHeight() - verticalGroup.getHeight() - Constants.UI_PAD);

        initPlayers(gameInitMessage);

        goals = new ArrayList<>();
        for (int i : gameInitMessage.goals) {
            goals.add(i);
        }
        totalGoalsCount = goals.size();
        addToQueue(this::newGoal);
        arrow.setVisible(true);
        goalsCountLabel.setVisible(true);

        InfoModal infoModal = new InfoModal(game.i18NBundle.get("randomGoalsMode"), 5);
        showModal(infoModal);

        sendTcp(new PlayerReadyMessage());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        setCameraAtPlayer();
        if (goal != null) {
            arrow.setRotation(90 + getAngleFromPlayer(goal.getBody().getPosition()));
        }
    }

    @Override
    void gameStarted() {

    }

    @Override
    void onlinePlayerGoalReached(GoalReachedMessage goalReachedMessage) {
        playersMap.get(goalReachedMessage.name).score += goalReachedMessage.value;
        updatePlayerGroup();
    }

    @Override
    void playerExit(PlayerExitMessage playerExitMessage) {
        super.playerExit(playerExitMessage);
        updatePlayerGroup();
    }

    private void updatePlayerGroup() {
        playersGroup.clear();
        for (String key : playersMap.keySet()) {
            String text = key + ": " + (int) playersMap.get(key).score;
            Label label = new Label(text, MyGame.getSkin(), "small", playersMap.get(key).color);
            playersGroup.addActor(label);
        }
    }

    private void initPlayers(GameInitMessage gameInitMessage) {
        int[] spawns = gameInitMessage.spawns;

        for (int i = 0; i < gameInitMessage.playerModels.length; i++) {
            BaseCar car = makeCar(gameInitMessage.playerModels[i], spawns[i]);
            OnlinePlayer onlinePlayer = new OnlinePlayer(car, gameInitMessage.playerModels[i], Constants.PLAYER_COLORS[i]);
            playersMap.put(onlinePlayer.getPlayerModel().name, onlinePlayer);
            if (onlinePlayer.getPlayerModel().name.equals(nickname)) {
                car.controlByPlayer();
                playerCar = car;
            }
        }

        updatePlayerGroup();
    }

    public void goalReached() {
        super.goalReached();
        GoalReachedMessage goalReachedMessage = new GoalReachedMessage();
        goalReachedMessage.value = 1;
        sendTcp(goalReachedMessage);
        if (goals.size() > 0) {
            addToQueue(this::newGoal);
        }
        SoundUtils.play(Assets.GOAL_REACHED, 0.5f);
        goal.showParticle();
        goal.dispose();
    }

    private void newGoal() {
        Vector2 goalPos = findGoalByIndex(goals.remove(0))
                .getRectangle().getCenter(new Vector2()).scl(1f / 32);
        goal = new Goal(world, goalPos, 0, rayHandler);
        if (goals.size() > 0) {
            Vector2 nextGoalPos = findGoalByIndex(goals.get(0)).getRectangle()
                    .getCenter(new Vector2()).scl(1f / 32);
            goal.makeArrow(90 + goalPos.sub(nextGoalPos).angle());
        }
        addActor(goal);
        goal.toBack();
        String scr = game.i18NBundle.format("goal",
                totalGoalsCount - goals.size() - 1, totalGoalsCount);
        goalsCountLabel.setText(scr);
    }
}
