package com.lifeapp.shadowracers.modes;

import com.appodeal.gdx.GdxAppodeal;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.FloatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.cars.AstonCar;
import com.lifeapp.shadowracers.actors.cars.AudiCar;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.cars.BmwCar;
import com.lifeapp.shadowracers.actors.cars.DodgeCar;
import com.lifeapp.shadowracers.actors.cars.FerrariCar;
import com.lifeapp.shadowracers.actors.cars.HyperCar;
import com.lifeapp.shadowracers.actors.cars.LamboCar;
import com.lifeapp.shadowracers.actors.cars.MustangCar;
import com.lifeapp.shadowracers.actors.cars.PorscheCar;
import com.lifeapp.shadowracers.actors.cars.RenaultCar;
import com.lifeapp.shadowracers.actors.cars.TruckCar;
import com.lifeapp.shadowracers.actors.cars.ViperCar;
import com.lifeapp.shadowracers.actors.modals.BaseModal;
import com.lifeapp.shadowracers.actors.modals.menu.PauseModal;
import com.lifeapp.shadowracers.listeners.ControlListener;
import com.lifeapp.shadowracers.listeners.WorldContactListener;
import com.lifeapp.shadowracers.network.PlayerModel;
import com.lifeapp.shadowracers.screens.GameScreen;
import com.lifeapp.shadowracers.utils.MapBodyBuilder;
import com.lifeapp.shadowracers.utils.MissionsUtils;
import com.lifeapp.shadowracers.utils.OptionsUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.ArrayList;

import box2dLight.RayHandler;

/**
 * Created by Александр on 17.01.2018.
 */

public class Mode extends Stage implements Disposable {
    MyGame game = (MyGame) Gdx.app.getApplicationListener();
    GameScreen screen = game.gameScreen;

    World world;
    RayHandler rayHandler;
    private TiledMap map;
    private OrthogonalTiledMapRenderer mapRenderer;
    private Box2DDebugRenderer box2DDebugRenderer;

    private Stage uiStage;
    private Group uiGroup;

    private Array<Body> deletingBodies = new Array<>();

    private Vector2 cameraTarget = new Vector2();
    private float cameraZoom;

    BaseCar playerCar;

    private Group mapGroup;

    private ArrayList<Runnable> queue = new ArrayList<>();

    boolean paused = false;

    private int fboWidth = Gdx.graphics.getWidth() / 4;
    private int fboHeight = Gdx.graphics.getHeight() / 4;

    private FloatAction blurAction;
    private ShaderProgram blurShader;
    private FrameBuffer blurTargetA;
    private FrameBuffer blurTargetB;

    private ShaderProgram shadowShader;

    private Rectangle cullingArea;

    public class GameInfo {
        public int totalDrift = 0;
        public int bestDrift = 0;
        public int collisionCount = 0;
        public int goalsReached = 0;
        public float passTime = 0;

        public long getAsLong(MissionsUtils.MissionParameter parameter) {
            float score = -1;
            switch (parameter) {
                case Time:
                    score = passTime * 100;
                    break;
                case BestDrift:
                    score = bestDrift;
                    break;
                case TotalDrift:
                    score = totalDrift;
                    break;
            }
            return (long) score;
        }

        public String getAsString(MissionsUtils.MissionParameter parameter) {
            String string = null;
            switch (parameter) {
                case Time:
                    string = String.format("%.2f", passTime);
                    break;
                case BestDrift:
                    string = String.format("%d", bestDrift);
                    break;
                case TotalDrift:
                    string = String.format("%d", totalDrift);
                    break;
                case Collisions:
                    string = String.format("%d", collisionCount);
                    break;
            }
            return string;
        }
    }

    private GameInfo gameInfo;

    public Mode(Assets.Maps mapAsset) {
        super(new ScreenViewport(), MyGame.getInstance().spriteBatch);
        cullingArea = new Rectangle(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uiStage = new Stage(new ScreenViewport());
        uiGroup = new Group();
        uiStage.addActor(uiGroup);
        map = new TmxMapLoader().load(mapAsset.getPath());
        mapRenderer = new OrthogonalTiledMapRenderer(map, (float) Constants.PPT / 32, getBatch());
        box2DDebugRenderer = new Box2DDebugRenderer();

        mapGroup = new Group();
        mapGroup.setTransform(false);
        mapGroup.setCullingArea(cullingArea);
        mapGroup.setSize(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT);
        addActor(mapGroup);
        makeMap();

        blurAction = new FloatAction(0, 0);
        blurShader = new ShaderProgram(Gdx.files.internal(Assets.SHADER_BLUR_VERTEX),
                Gdx.files.internal(Assets.SHADER_BLUR_FRAGMENT));
        blurTargetA = new FrameBuffer(Pixmap.Format.RGBA8888, fboWidth, fboHeight, false);
        blurTargetB = new FrameBuffer(Pixmap.Format.RGBA8888, fboWidth, fboHeight, false);
        shadowShader = new ShaderProgram(Gdx.files.internal(Assets.SHADER_SHADOW_VERTEX),
                Gdx.files.internal(Assets.SHADER_SHADOW_FRAGMENT));

//        debugFps();

        getRoot().setTransform(false);
        getRoot().setCullingArea(cullingArea);

        resetGameInfo();

//        debugFps();
    }

    @Override
    public void act(float delta) {
        while (deletingBodies.size > 0) {
            Body body = deletingBodies.pop();
            body.getWorld().destroyBody(body);
        }

        if (!paused) {
            gameInfo.passTime += delta;
            world.step(delta, 6, 3);
            super.act(delta);
        }
        updateCamera(delta);
        mapRenderer.setView((OrthographicCamera) getCamera());
        rayHandler.setCombinedMatrix(getCamera().combined.cpy().scl(Constants.PPT));
        rayHandler.update();

        uiStage.act();

        while (queue.size() > 0) {
            queue.remove(0).run();
        }
    }

    @Override
    public void draw() {
        if (blurAction.getValue() > 0) {
            drawBlured();
        } else {
            mapRenderer.render();
            super.draw();
            rayHandler.setLightMapRendering(true);
            rayHandler.render();
        }

//        box2DDebugRenderer.render(world, getCamera().combined.scl(Constants.PPT));
        uiStage.draw();

        getBatch().setProjectionMatrix(uiStage.getCamera().combined);
    }

    @Override
    public void dispose() {
        while (getActors().size > 0) {
            if (getActors().first() instanceof Disposable) {
                ((Disposable) getActors().first()).dispose();
            } else
                getActors().first().remove();
        }
        rayHandler.removeAll();
        mapRenderer.dispose();
        box2DDebugRenderer.dispose();
        rayHandler.dispose();
        clear();
        world.dispose();
        map.dispose();
        GdxAppodeal.hide(GdxAppodeal.BANNER_BOTTOM);
        Gdx.input.cancelVibrate();
    }

    public void update(float delta) {
        act(delta);
        draw();
    }

    private void makeMap() {
        mapGroup.clear();

        world = new World(new Vector2(0, 0), true);
        world.setContactListener(new WorldContactListener(this));
        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(Constants.AMBIENT_LIGHT);

        MapBodyBuilder.buildStaticObjects(map, 32, world);
        MapBodyBuilder.buildDynamicObjects(map, 32, world, mapGroup, rayHandler);

    }

    public void startDrift() {
        if (OptionsUtils.isVibrationEnabled()) {
            Gdx.input.vibrate(new long[]{0, 15, 100}, 0);
        }
    }

    public void successDrift(float scr) {
        Gdx.input.cancelVibrate();
        gameInfo.totalDrift += scr;
        if (gameInfo.bestDrift < scr)
            gameInfo.bestDrift = (int) scr;
        if (scr > StatsUtils.getBestDrift()) {
            StatsUtils.newBestDrift(scr);
            showMessage(game.i18NBundle.format("newDriftRecord", scr), 0.6f, Color.GREEN);

        }
    }

    public void failDrift() {
        Gdx.input.cancelVibrate();
    }

    public void goalReached() {
        gameInfo.goalsReached++;
        StatsUtils.incGoalReached();
        if (OptionsUtils.isVibrationEnabled()) {
            Gdx.input.vibrate(new long[]{0, 100, 60, 100}, -1);
        }
    }

    public void goalFailed() {

    }

    public void collision(Contact contact) {
        gameInfo.collisionCount++;
    }

    public void pause() {
        if (getUiStage().getRoot().findActor("modal_0") != null) {
            return;
        }
        setBlur(3);
        paused = true;
        if (playerCar != null) {
            playerCar.updateLoops(0, 0);
        }
        failDrift();
        PauseModal pauseModal = new PauseModal(screen);
        showModal(pauseModal);
    }

    public void resume() {
//        if (getUiStage().getRoot().findActor("modal_0") != null) {
//            return;
//        }
        paused = false;
        setBlur(0);
    }

    public void exit() {

    }

    public void setPlayerCar(BaseCar car) {
        playerCar = car;
    }

    public BaseCar getPlayerCar() {
        return playerCar;
    }

    public void control(Vector2 control) {
        playerCar.setControl(control);
    }

    public BaseCar makeCar(PlayerModel playerModel) {
        return makeCar(playerModel, -1);
    }

    public BaseCar makeCar(PlayerModel playerModel, int spawnIndex) {
        return makeCar(playerModel, spawnIndex, true, true, true);
    }

    public BaseCar makeCar(PlayerModel playerModel, int spawnId, boolean effects, boolean sounds, boolean lights) {
        if (playerModel == null) {
            throw new NullPointerException("player model can't be null");
        }

        MapLayer spawnsLayer = map.getLayers().get("spawns");
        MapObjects playerSpawns = spawnsLayer.getObjects();
        RectangleMapObject spawn = null;
        if (spawnId > 0) {
            for (MapObject object : playerSpawns) {
                if (object.getProperties().get("id", 0, Integer.class) == spawnId) {
                    spawn = (RectangleMapObject) object;
                    break;
                }
            }
        }
        if (spawn == null) {
            spawn = (RectangleMapObject) (playerSpawns.get(
                    MathUtils.random(playerSpawns.getCount() - 1)));
        }
        Vector2 pos = MapBodyBuilder.getCenter(spawn);
        float angle = -spawn.getProperties().get("rotation", 0f, Float.class);

        BaseCar car;
        if ("AstonCar".equals(playerModel.car)) {
            car = new AstonCar(world, rayHandler, pos, angle);
        } else if ("AudiCar".equals(playerModel.car)) {
            car = new AudiCar(world, rayHandler, pos, angle);
        } else if ("BmwCar".equals(playerModel.car)) {
            car = new BmwCar(world, rayHandler, pos, angle);
        } else if ("DodgeCar".equals(playerModel.car)) {
            car = new DodgeCar(world, rayHandler, pos, angle);
        } else if ("FerrariCar".equals(playerModel.car)) {
            car = new FerrariCar(world, rayHandler, pos, angle);
        } else if ("HyperCar".equals(playerModel.car)) {
            car = new HyperCar(world, rayHandler, pos, angle);
        } else if ("LamboCar".equals(playerModel.car)) {
            car = new LamboCar(world, rayHandler, pos, angle);
        } else if ("MustangCar".equals(playerModel.car)) {
            car = new MustangCar(world, rayHandler, pos, angle);
        } else if ("PorscheCar".equals(playerModel.car)) {
            car = new PorscheCar(world, rayHandler, pos, angle);
        } else if ("RenaultCar".equals(playerModel.car)) {
            car = new RenaultCar(world, rayHandler, pos, angle);
        } else if ("TruckCar".equals(playerModel.car)) {
            car = new TruckCar(world, rayHandler, pos, angle);
        } else if ("ViperCar".equals(playerModel.car)) {
            car = new ViperCar(world, rayHandler, pos, angle);
        } else {
            car = new RenaultCar(world, rayHandler, pos, angle);
        }

        if (playerModel.carColor != null)
            car.changeCarColor(Color.valueOf(playerModel.carColor));
        if (lights) {
            car.initLights();
            if (playerModel.ligtsColor != null)
                car.changeLightsColor(Color.valueOf(playerModel.ligtsColor));
            if (playerModel.neonsColor != null)
                car.changeNeonsColor(Color.valueOf(playerModel.neonsColor));
        }
        if (effects) {
            car.initEffects();
        }
        if (sounds) {
            car.initSounds();
        }
        addActor(car);
        car.toBack();
        return car;
    }

    RectangleMapObject findGoalByIndex(int index) {
        MapObjects goalSpawns = map.getLayers().get("points").getObjects();
        if (index == -1) {
            index = MathUtils.random(goalSpawns.getCount() - 1);
        }
        return (RectangleMapObject) (goalSpawns.get(index));
    }

    RectangleMapObject findGoalById(int id) {
        MapObjects goalSpawns = map.getLayers().get("points").getObjects();
        for (int i = 0; i < goalSpawns.getCount(); i++) {
            MapObject object = goalSpawns.get(i);
            if (id == object.getProperties().get("id", Integer.class)) {
                return (RectangleMapObject) object;
            }
        }
        return findGoalByIndex(-1);
    }

    public Stage getUiStage() {
        return uiStage;
    }

    public Group getUiGroup() {
        return uiGroup;
    }

    public Group getMapGroup() {
        return mapGroup;
    }

    private void drawBlured() {
        rayHandler.setLightMapRendering(false);
        rayHandler.render();

        blurTargetA.begin();
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mapRenderer.render();
        super.draw();

        blurTargetA.end();

//        blurTargetA.getColorBufferTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        getBatch().setProjectionMatrix(uiStage.getCamera().combined);

        blurTargetB.begin();
        getBatch().setShader(blurShader);
        getBatch().begin();
        blurShader.setUniformf("radius", blurAction.getValue());
        blurShader.setUniformf("dir", 1f, 0f);
        blurShader.setUniformf("resolution", fboWidth);
        getBatch().draw(
                blurTargetA.getColorBufferTexture(), 0, 0,
                Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                0, 0, fboWidth, fboHeight, false, true
        );


        getBatch().setShader(shadowShader);
        getBatch().enableBlending();
        getBatch().setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);

        shadowShader.setUniformf("ambient",
                Constants.AMBIENT_LIGHT.r * Constants.AMBIENT_LIGHT.a,
                Constants.AMBIENT_LIGHT.g * Constants.AMBIENT_LIGHT.a,
                Constants.AMBIENT_LIGHT.b * Constants.AMBIENT_LIGHT.a,
                1f - Constants.AMBIENT_LIGHT.a);
        getBatch().draw(
                rayHandler.getLightMapTexture(), 0, 0,
                Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                0, 0, fboWidth, fboHeight, false, true
        );

        getBatch().flush();
        blurTargetB.end();
        getBatch().setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        getBatch().setShader(blurShader);
        blurShader.setUniformf("dir", 0f, 1f);
        blurShader.setUniformf("resolution", fboHeight);
        getBatch().draw(
                blurTargetB.getColorBufferTexture(), 0, 0,
                Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                0, 0, fboWidth, fboHeight, false, true);
        getBatch().end();
        getBatch().setShader(null);
    }

    private void updateCamera(float delta) {
        OrthographicCamera orthographicCamera = (OrthographicCamera) getCamera();

        float cameraSpeed = delta * 6;
        float ispeed = 1.0f - cameraSpeed;

        Vector3 currentPosition = orthographicCamera.position;
        Vector2 targetPosition = cameraTarget.cpy();
        targetPosition.scl(cameraSpeed);
        currentPosition.scl(ispeed);
        currentPosition.add(targetPosition.x, targetPosition.y, 0);

        orthographicCamera.position.set(currentPosition);

        float currentZoom = orthographicCamera.zoom;
        float targetZoom = cameraZoom;
        targetZoom *= cameraSpeed;
        currentZoom *= ispeed;
        currentZoom += targetZoom;
        orthographicCamera.zoom = currentZoom;

        orthographicCamera.update();

        Vector3[] planePoints = orthographicCamera.frustum.planePoints;
        cullingArea.x = planePoints[0].x;
        cullingArea.y = planePoints[0].y;
    }

    public void setCameraTarget(Vector2 pos, float zoom) {
        cameraTarget = pos;
        cameraZoom = 1 / zoom;
    }

    public void setCameraAtPlayer() {
        Vector2 forwardVelocity = playerCar.getForwardVelocity();
        setCameraTarget(new Vector2(
                playerCar.getX(Align.center) + forwardVelocity.x * 15,
                playerCar.getY(Align.center) + forwardVelocity.y * 15
        ), 1f);
    }

    public void showModal(BaseModal modal) {
        showModal(modal, 0);
    }

    public void showModal(BaseModal modal, int groupIndex) {
        BaseModal oldModal = uiStage.getRoot().findActor("modal_" + groupIndex);
        float delay = 0;
        if (oldModal != null) {
            oldModal.hide();
            delay = 0.3f;
        }

        modal.setName("modal_" + groupIndex);
        uiStage.addActor(modal);
        modal.setMode(this);
        uiStage.addAction(Actions.delay(delay, Actions.run(modal::show)));
    }

    public float getAngleFromPlayer(Vector2 point) {
        try {
            return playerCar.getBody().getPosition().sub(point).angle();
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public float getDstFromPlayer(Vector2 point) {
        if (playerCar == null) {
            return 0;
        } else {
            return playerCar.getBody().getPosition().dst(point);
        }
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void deleteBody(Body body) {
        deletingBodies.add(body);
    }

    void resetGameInfo() {
        gameInfo = new GameInfo();
    }

    void initControl() {
        ControlListener controlListener = new ControlListener(this);

        float posX1, posX2;

        if (OptionsUtils.isInvertControlEnabled()) {
            posX1 = Constants.H1;
            posX2 = Gdx.graphics.getWidth() - Constants.H1 * 5;
        } else {
            posX1 = Gdx.graphics.getWidth() - Constants.H1 * 3;
            posX2 = Constants.H1;
        }

        ImageButton controlUp = new ImageButton(MyGame.getSkin().getDrawable(Assets.ICON_UP));
        controlUp.getColor().a = 0.5f;
        controlUp.setUserObject(ControlListener.Control.Up);
        controlUp.addListener(controlListener);
        controlUp.setPosition(
                posX1,
                Constants.H1 * 3f);
        controlUp.setSize(Constants.H1 * 2, Constants.H1 * 2);
        controlUp.getImageCell().size(Constants.H1);
        uiGroup.addActor(controlUp);

        ImageButton controlDown = new ImageButton(MyGame.getSkin().getDrawable(Assets.ICON_DOWN));
        controlDown.getColor().a = 0.5f;
        controlDown.setUserObject(ControlListener.Control.Down);
        controlDown.addListener(controlListener);
        controlDown.setPosition(
                posX1,
                Constants.H1 * 1);
        controlDown.setSize(Constants.H1 * 2, Constants.H1 * 2);
        controlDown.getImageCell().size(Constants.H1);
        uiGroup.addActor(controlDown);

        ImageButton controlLeft = new ImageButton(MyGame.getSkin().getDrawable(Assets.ICON_LEFT));
        controlLeft.getColor().a = 0.5f;
        controlLeft.setUserObject(ControlListener.Control.Left);
        controlLeft.addListener(controlListener);
        controlLeft.setPosition(
                posX2,
                Constants.H1 * 1);
        controlLeft.setSize(Constants.H1 * 2, Constants.H1 * 2);
        controlLeft.getImageCell().size(Constants.H1);
        uiGroup.addActor(controlLeft);

        ImageButton controlRight = new ImageButton(MyGame.getSkin().getDrawable(Assets.ICON_RIGHT));
        controlRight.getColor().a = 0.5f;
        controlRight.setUserObject(ControlListener.Control.Right);
        controlRight.addListener(controlListener);
        controlRight.setPosition(
                posX2 + Constants.H1 * 2,
                Constants.H1 * 1);
        controlRight.setSize(Constants.H1 * 2, Constants.H1 * 2);
        controlRight.getImageCell().size(Constants.H1);
        uiGroup.addActor(controlRight);
    }

    void showMessage(String text, float duration, Color color) {
        Label label = new Label(text, MyGame.getSkin(), "big", color);
        label.setAlignment(Align.center);
        Container labelContainer = new Container<>(label);
        labelContainer.setTransform(true);
        labelContainer.setOrigin(Align.center);
        labelContainer.setPosition(Gdx.graphics.getWidth() * 0.5f - labelContainer.getWidth() * 0.5f,
                Gdx.graphics.getHeight() * 0.5f - labelContainer.getHeight() * 0.5f);
        labelContainer.setScale(0);
        labelContainer.addAction(Actions.sequence(
                Actions.scaleTo(1, 1, 0.3f, Interpolation.elasticOut),
                Actions.delay(duration),
                Actions.parallel(
                        Actions.scaleTo(3, 3, 0.3f),
                        Actions.fadeOut(0.2f)
                ),
                Actions.removeActor()
        ));
        uiStage.addActor(labelContainer);
    }

    public void addToQueue(Runnable runnable) {
        queue.add(runnable);
    }

    public void setBlur(float value) {
        float currentBlur = blurAction.getValue();
        blurAction.reset();
        blurAction.setStart(currentBlur);
        blurAction.setEnd(value);
        blurAction.setDuration(0.3f);
        uiStage.addAction(blurAction);
    }

    private void debugFps() {
        Label fpsLabel = new Label("", MyGame.getSkin()) {
            @Override
            public void act(float delta) {
                super.act(delta);
                setText("FPS: " + String.valueOf(Gdx.graphics.getFramesPerSecond()));
            }
        };
        uiStage.addActor(fpsLabel);
        fpsLabel.setPosition(Gdx.graphics.getWidth() * 0.5f, 70, Align.center);
    }
}
