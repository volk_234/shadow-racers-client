package com.lifeapp.shadowracers.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.modals.menu.PauseModal;
import com.lifeapp.shadowracers.actors.modals.network.ConnectionModal;
import com.lifeapp.shadowracers.actors.modals.network.FailConnectionModal;
import com.lifeapp.shadowracers.actors.modals.network.LobbyModal;
import com.lifeapp.shadowracers.actors.modals.network.NetworkErrorModal;
import com.lifeapp.shadowracers.network.OnlinePlayer;
import com.lifeapp.shadowracers.network.PlayerModel;
import com.lifeapp.shadowracers.network.ServerConnection;
import com.lifeapp.shadowracers.network.messages.ErrorMessage;
import com.lifeapp.shadowracers.network.messages.GameInitMessage;
import com.lifeapp.shadowracers.network.messages.LobbyFoundMessage;
import com.lifeapp.shadowracers.network.messages.PlayerExitMessage;
import com.lifeapp.shadowracers.network.messages.PlayerJoinMessage;
import com.lifeapp.shadowracers.network.messages.RequestLobbyMessage;
import com.lifeapp.shadowracers.network.messages.StateMessage;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.HashMap;


/**
 * Created by Александр on 20.12.2017.
 */

public class LobbyMode extends Mode {

    private ServerConnection connection;

    private HashMap<String, OnlinePlayer> playersMap = new HashMap<>();

    private VerticalGroup boardGroup;
    private Label playersCountLabel;
    private Label startInLabel;
    private VerticalGroup playersGroup;

    private boolean messageShowed = false;

    private int waitTime;

    private String nickname;

    private LobbyModal lobbyModal;

    public LobbyMode() {
        super(Assets.Maps.Map1);

        playerCar = makeCar(StatsUtils.getPlayerModel(), -1);
        playerCar.controlByPlayer();

        showModal(new ConnectionModal());
        new Thread(() -> {
            connection = game.connectToServer();
            if (connection == null) {
                FailConnectionModal failConnectionModal = new FailConnectionModal(screen);
                showModal(failConnectionModal);
                return;
            }
            connection.setListener(new Listener() {
                @Override
                public void disconnected(Connection connection) {
                    LobbyMode.this.disconnected(null);
                }

                @Override
                public void received(Connection connection, final Object object) {
                    if (object instanceof StateMessage) {
                        try {
                            StateMessage stateMessage = (StateMessage) object;

                            BaseCar car = playersMap.get(stateMessage.name).getCar();
                            car.setControl(stateMessage.controlX, stateMessage.controlY);
                            Body stateBody = car.getBody();
                            Vector2 bodyPosition = stateBody.getPosition();
                            if (stateBody.getPosition().dst(stateMessage.positionX, stateMessage.positionY) > 1.5f) {
                                bodyPosition.x = stateMessage.positionX;
                                bodyPosition.y = stateMessage.positionY;
                            }
                            stateBody.setTransform(bodyPosition, stateMessage.angle);
                            stateBody.setLinearVelocity(stateMessage.speedX, stateMessage.speedY);
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    } else if (object instanceof GameInitMessage) {
                        getUiGroup().clear();
                        screen.resume();
                        switch (((GameInitMessage) object).mode) {
                            case 1:
                                addToQueue(() -> screen.initMode(
                                        new TotalDriftOnlineMode(LobbyMode.this.connection, (GameInitMessage) object)));
                                break;
                            case 2:
                                addToQueue(() -> screen.initMode(
                                        new LapsOnlineMode(LobbyMode.this.connection, (GameInitMessage) object)));
                                break;
                        }
                    } else if (object instanceof LobbyFoundMessage) {
                        setBlur(0);
                        getUiGroup().setVisible(true);
                        addAction(Actions.forever(Actions.delay(0.1f, Actions.run(LobbyMode.this::sendState))));
                    } else if (object instanceof PlayerJoinMessage) {
                        addToQueue(() -> playerJoin((PlayerJoinMessage) object));
                    } else if (object instanceof PlayerExitMessage) {
                        playerExit((PlayerExitMessage) object);
                    } else if (object instanceof ErrorMessage) {
                        LobbyMode.this.disconnected((ErrorMessage) object);
                    }
                }
            });
            lobbyModal = new LobbyModal(screen);
            showModal(lobbyModal);
        }).start();
        setBlur(3);

        initControl();
        ImageButton pauseButton = new ImageButton(MyGame.getSkin().newDrawable(Assets.ICON_PAUSE, new Color(0xffffffaa)));
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.gameScreen.pause();
                SoundUtils.playClick();
            }
        });
        getUiGroup().addActor(pauseButton);
        pauseButton.setSize(Constants.H1, Constants.H1);
        pauseButton.setPosition(Constants.UI_PAD,
                Gdx.graphics.getHeight() - pauseButton.getHeight() - Constants.UI_PAD);

        boardGroup = new VerticalGroup();
        boardGroup.right();
        boardGroup.columnRight();
        getUiGroup().addActor(boardGroup);
        boardGroup.setPosition(Gdx.graphics.getWidth() - boardGroup.getPrefWidth() - Constants.UI_PAD,
                Gdx.graphics.getHeight() - boardGroup.getPrefHeight() - Constants.UI_PAD);

        playersCountLabel = new Label("", MyGame.getSkin(), "small", Color.WHITE);
        boardGroup.addActor(playersCountLabel);

        startInLabel = new Label("", MyGame.getSkin(), "small", Color.WHITE);
        boardGroup.addActor(startInLabel);

        playersGroup = new VerticalGroup();
        playersGroup.columnRight();
        boardGroup.addActor(playersGroup);

        getUiGroup().setVisible(false);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (!paused) {
            setCameraAtPlayer();
        }
    }

    public void requestForLobby() {
        lobbyModal.hide();
        nickname = StatsUtils.getNickname();

        PlayerModel playerModel = StatsUtils.getPlayerModel();
        playerModel.name = nickname;

        RequestLobbyMessage requestLobbyMessage = new RequestLobbyMessage();
        requestLobbyMessage.netVersion = Constants.NET_VERSION;
        requestLobbyMessage.playerModel = playerModel;

        //todo
        OnlinePlayer onlinePlayer = new OnlinePlayer(playerCar, playerModel, Constants.PLAYER_COLORS[0]);
        playersMap.put(nickname, onlinePlayer);
        playersGroup.addActor(onlinePlayer.getScoreLabel());
        updatePlayersGroup();

        connection.sendTCP(requestLobbyMessage);
    }

    private void playerJoin(PlayerJoinMessage playerJoinMessage) {
        BaseCar car = makeCar(playerJoinMessage.playerModel, -1);
        OnlinePlayer newOnlinePlayer = new OnlinePlayer(car,
                playerJoinMessage.playerModel, Constants.PLAYER_COLORS[playersMap.size()]);
        newOnlinePlayer.enablePointer();
        newOnlinePlayer.enableMarker();
        playersGroup.addActor(newOnlinePlayer.getScoreLabel());
        playersMap.put(playerJoinMessage.playerModel.name, newOnlinePlayer);
        updatePlayersGroup();
    }

    private void playerExit(PlayerExitMessage playerExitMessage) {
        try {
            OnlinePlayer onlinePlayer = playersMap.get(playerExitMessage.nickname);
            onlinePlayer.disablePointer();
            BaseCar car = onlinePlayer.getCar();
            if (car != null) {
                addToQueue(car::dispose);
            }
            playersMap.remove(playerExitMessage.nickname);
            onlinePlayer.getScoreLabel().remove();
            updatePlayersGroup();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePlayersGroup() {
        playersGroup.getChildren().sort((left, right) ->
                ((OnlinePlayer) right.getUserObject()).getPlayerModel().wins -
                        ((OnlinePlayer) left.getUserObject()).getPlayerModel().wins);

        playersCountLabel.setText(game.i18NBundle.format("waitForPlayers", playersMap.size(), 4));

        startInLabel.clearActions();
        if (playersMap.size() > 1) {
            waitTime = 60;
            startInLabel.setVisible(true);
            startInLabel.addAction(Actions.repeat(60, Actions.sequence(
                    Actions.run(() -> {
                        waitTime--;
                        startInLabel.setText(game.i18NBundle.format("startIn", waitTime));
                    }),
                    Actions.delay(1)
            )));
        } else {
            startInLabel.setVisible(false);
        }
    }

    private void sendState() {
        sendUdp(new StateMessage(playerCar.getName(), playerCar.getControl().x, playerCar.getControl().y,
                playerCar.getBody().getPosition().x, playerCar.getBody().getPosition().y,
                playerCar.getBody().getLinearVelocity().x, playerCar.getBody().getLinearVelocity().y,
                playerCar.getBody().getAngle()));
    }

    private void disconnected(ErrorMessage errorMessage) {
        if (messageShowed) return;
        messageShowed = true;
        if (errorMessage == null)
            errorMessage = new ErrorMessage();
        NetworkErrorModal networkErrorModal = new NetworkErrorModal(errorMessage);
        showModal(networkErrorModal);
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void control(Vector2 control) {
        super.control(control);
        sendState();
    }

    private void sendUdp(Object object) {
        connection.sendUDP(object);
    }

    @Override
    public void pause() {
        messageShowed = true;
        if (getUiStage().getRoot().findActor("modal_0") != null) {
            return;
        }
        setBlur(3);
        PauseModal pauseModal = new PauseModal(screen);
        showModal(pauseModal);
    }

    @Override
    public void exit() {
        connection.close();
        getRoot().clearActions();
        paused = true;
    }
}
