package com.lifeapp.shadowracers.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.lifeapp.shadowracers.variables.Assets;

/**
 * Created by Александр on 06.04.2018.
 */

public class SoundUtils {
    private static AssetManager assetManager;
    private static float optionsVolume = OptionsUtils.isSoundEnabled() ? 1 : 0;

    public static void updateVolume() {
        optionsVolume = OptionsUtils.isSoundEnabled() ? 1 : 0;
    }

    public static void setAssetManager(AssetManager manager) {
        assetManager = manager;
    }

    public static long play(String name, float volume) {
        return assetManager.get(name, Sound.class).play(optionsVolume * volume);
    }

    public static long loop(String name, float volume) {
        return assetManager.get(name, Sound.class).loop(optionsVolume * volume);
    }

    public static long loop(String name, float volume, float pitch, float pan) {
        return assetManager.get(name, Sound.class).loop(optionsVolume * volume, pitch, pan);
    }

    public static void stop(String name) {
        assetManager.get(name, Sound.class).stop();
    }

    public static void stop(String name, long id) {
        assetManager.get(name, Sound.class).stop(id);
    }

    public static void setPitch(String name, long id, float pitch) {
        if (pitch < 0.5f)
            pitch = 0.5f;
        else if (pitch > 2)
            pitch = 2f;
        assetManager.get(name, Sound.class).setPitch(id, pitch);
    }

    public static void setVolume(String name, long id, float volume) {
        if (volume < 0f)
            volume = 0f;
        else if (volume > 1)
            volume = 1f;
        assetManager.get(name, Sound.class).setVolume(id, optionsVolume * volume);
    }

    public static void playClick() {
        assetManager.get(Assets.CLICK, Sound.class).play(optionsVolume * 1);
    }

    public static void playDisabled() {
        assetManager.get(Assets.DISABLED, Sound.class).play(optionsVolume * 1);
    }

    public static void playSwitch() {
        assetManager.get(Assets.SWITCH, Sound.class).play(optionsVolume * 1);
    }

    public static void playMusic() {
        Music music = assetManager.get(Assets.MUSIC, Music.class);
        music.setLooping(true);
        music.play();
    }
}
