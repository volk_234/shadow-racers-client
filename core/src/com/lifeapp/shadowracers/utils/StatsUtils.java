package com.lifeapp.shadowracers.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.network.PlayerModel;

/**
 * Created by Александр on 20.03.2018.
 */

public class StatsUtils {
    private static MyGame game = (MyGame) Gdx.app.getApplicationListener();
    private static Preferences preferences = Gdx.app.getPreferences("stats");


    public static int getOnlineWins() {
        return preferences.getInteger("online_wins", 0);
    }

    public static void incOnlineWins() {
        int wins = getOnlineWins();
        preferences.putInteger("online_wins", ++wins);
        preferences.flush();
    }

    public static int getGoalReached() {
        return preferences.getInteger("goals_reached", 0);
    }

    public static void incGoalReached() {
        int goalReached = getGoalReached();
        goalReached++;
        preferences.putInteger("goals_reached", goalReached);
        preferences.flush();
    }

    public static float getDifficultyCoef() {
        int goalReached = getGoalReached();
        float coef = 0.2f + goalReached / 20;
        return coef > 1 ? 1 : coef;
    }

    public static int getMoney() {
        return preferences.getInteger("score", 0);
    }

    public static int modifyMoney(int value) {
        int score = preferences.getInteger("score", 0);
        score += value;
        preferences.putInteger("score", score);
        preferences.flush();
        return score;
    }

    public static void setNickname(String nickname) {
        preferences.putString("name", nickname);
        preferences.flush();
    }

    public static String getNickname() {
        return preferences.getString("name", "Player" + MathUtils.random(1000));
    }

    public static float getBestDrift() {
        return preferences.getFloat("best_drift", 0);
    }

    public static float newBestDrift(float value) {
        float oldValue = getBestDrift();
        preferences.putFloat("best_drift", value);
        return (value - oldValue) / 5 + value / 20;
    }

    public static boolean isTutorialCompleted() {
        return preferences.getBoolean("tutorial", false);
    }

    public static void tutorialCompleted() {
        preferences.putBoolean("tutorial", true);
        preferences.flush();
    }

    public static PlayerModel getPlayerModel() {
        String usedCar = ShopUtils.getUsedCar();
        Color carColor = ShopUtils.getColorForCar(usedCar);
        Color lightsColor = ShopUtils.getLightsForCar(usedCar);
        Color neonsColor = ShopUtils.getNeonsForCar(usedCar);

        PlayerModel playerModel = new PlayerModel();
        playerModel.name = getNickname();
        playerModel.wins = getOnlineWins();
        playerModel.car = usedCar;
        if (carColor != null) playerModel.carColor = String.valueOf(carColor);
        if (lightsColor != null) playerModel.ligtsColor = String.valueOf(lightsColor);
        if (neonsColor != null) playerModel.neonsColor = String.valueOf(neonsColor);
        return playerModel;
    }
}
