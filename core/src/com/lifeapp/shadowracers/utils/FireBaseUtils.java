package com.lifeapp.shadowracers.utils;

import com.lifeapp.shadowracers.FireBaseApi;

import java.util.ArrayList;

public class FireBaseUtils {
    public static FireBaseApi fireBaseApi;

    static public void subscribeToTopic(String topic) {
        fireBaseApi.subscribeToTopic(topic);
    }

    static public void unSubscribeFromTopic(String topic) {
        fireBaseApi.unSubscribeFromTopic(topic);
    }

    static public String getIID() {
        return fireBaseApi.getIID();
    }

    static FireBaseApi.ResponseListener<ArrayList<FireBaseApi.LbScore>> getTopScores(int level) {
        return fireBaseApi.getTopScores(level);
    }

    static FireBaseApi.ResponseListener<FireBaseApi.LbScore> getPlayerScore(int level) {
        return fireBaseApi.getPlayerScore(level);
    }

    static public void showLeaderBoardForLevel(int level) {
        fireBaseApi.showLeaderBoardForLevel(level);
    }

    static public void putScoreForLevel(int level, float score) {
        fireBaseApi.putScoreForLevel(level, score);
    }

    static public boolean isSignedIn() {
        return fireBaseApi.isSignedIn();
    }

    static public void signInSilently() {
        fireBaseApi.signInSilently();
    }

    static public void startSignInIntent() {
        fireBaseApi.startSignInIntent();
    }
}
