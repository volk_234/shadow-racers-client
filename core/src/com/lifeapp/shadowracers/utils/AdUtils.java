package com.lifeapp.shadowracers.utils;

import com.appodeal.gdx.GdxAppodeal;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by Александр on 20.03.2018.
 */

public class AdUtils {
    private static int missionsFromLastAd = 0;
    private static int onlinePlayerFromLastAd = 0;
    private static long timeLastAd = TimeUtils.millis();
    private static boolean shouldShowNonSkippable = false;

    public static void MissionEnd() {
        missionsFromLastAd++;

        if (missionsFromLastAd >= 10 || TimeUtils.timeSinceMillis(timeLastAd) > 300000) {
            if (shouldShowNonSkippable) {
                if (GdxAppodeal.isLoaded(GdxAppodeal.NON_SKIPPABLE_VIDEO)) {
                    GdxAppodeal.show(GdxAppodeal.NON_SKIPPABLE_VIDEO);
                    missionsFromLastAd = 0;
                    timeLastAd = TimeUtils.millis();
                    shouldShowNonSkippable = false;
                    return;
                }
            }
            if (GdxAppodeal.isLoaded(GdxAppodeal.SKIPPABLE_VIDEO)) {
                GdxAppodeal.show(GdxAppodeal.SKIPPABLE_VIDEO);
                missionsFromLastAd = 0;
                timeLastAd = TimeUtils.millis();
                shouldShowNonSkippable = true;
            }
        }

    }

    public static void onlinePlayed() {
        onlinePlayerFromLastAd++;

        if (onlinePlayerFromLastAd >= 3 || TimeUtils.timeSinceMillis(timeLastAd) > 300000) {
            if (shouldShowNonSkippable) {
                if (GdxAppodeal.isLoaded(GdxAppodeal.NON_SKIPPABLE_VIDEO)) {
                    GdxAppodeal.show(GdxAppodeal.NON_SKIPPABLE_VIDEO);
                    missionsFromLastAd = 0;
                    timeLastAd = TimeUtils.millis();
                    shouldShowNonSkippable = false;
                    return;
                }
            }
            if (GdxAppodeal.isLoaded(GdxAppodeal.SKIPPABLE_VIDEO)) {
                GdxAppodeal.show(GdxAppodeal.SKIPPABLE_VIDEO);
                missionsFromLastAd = 0;
                timeLastAd = TimeUtils.millis();
                shouldShowNonSkippable = true;
            }
        }

    }
}
