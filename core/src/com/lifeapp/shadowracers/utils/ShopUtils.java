package com.lifeapp.shadowracers.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;

/**
 * Created by Александр on 20.03.2018.
 */

public class ShopUtils {
    private static Preferences preferences = Gdx.app.getPreferences("shop");

    public static String getUsedCar() {
        return preferences.getString("used_car", "BmwCar");
    }

    public static boolean isCarBought(String car) {
        return preferences.getBoolean(car, false);
    }

    public static void buyCar(String car) {
        preferences.putBoolean(car, true);
        preferences.flush();
    }

    public static void useCar(String car) {
        preferences.putString("used_car", car);
        preferences.flush();
    }

    public static void buyColorForCar(String car, Color color) {
        preferences.putBoolean(car + "_color_" + color.toString(), true);
        preferences.flush();
    }

    public static void buyLightsForCar(String car, Color color) {
        preferences.putBoolean(car + "_lights_" + color.toString(), true);
        preferences.flush();
    }

    public static void buyNeonsForCar(String car, Color color) {
        preferences.putBoolean(car + "_neons_" + color.toString(), true);
        preferences.flush();
    }


    public static void useColorForCar(String car, Color color) {
        preferences.putString(car + "_color", color.toString());
        preferences.flush();
    }

    public static void useLightsForCar(String car, Color color) {
        preferences.putString(car + "_lights", color.toString());
        preferences.flush();
    }

    public static void useNeonsForCar(String car, Color color) {
        preferences.putString(car + "_neons", color.toString());
        preferences.flush();
    }


    public static Color getColorForCar(String car) {
        String stringColor = preferences.getString(car + "_color", null);
        if (stringColor == null)
            return null;
        else
            return Color.valueOf(stringColor);
    }

    public static Color getLightsForCar(String car) {
        String stringColor = preferences.getString(car + "_lights", null);
        if (stringColor == null)
            return null;
        else
            return Color.valueOf(stringColor);
    }

    public static Color getNeonsForCar(String car) {
        String stringColor = preferences.getString(car + "_neons", null);
        if (stringColor == null)
            return null;
        else
            return Color.valueOf(stringColor);
    }
}
