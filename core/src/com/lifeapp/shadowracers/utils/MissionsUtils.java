package com.lifeapp.shadowracers.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Json;
import com.lifeapp.shadowracers.FireBaseApi;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.goals.GoalPointer;
import com.lifeapp.shadowracers.actors.ui.LoadingImage;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.network.PlayerModel;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

import java.util.ArrayList;
import java.util.Collections;

public class MissionsUtils {
    private static Preferences preferences = Gdx.app.getPreferences("missions");

    public enum MissionParameter {
        Time, TotalDrift, BestDrift, Collisions;

        public String getString() {
            String string = "";
            switch (this) {
                case Time:
                    string = "Время";
                    break;
                case BestDrift:
                    string = "Лучший дрифт";
                    break;
                case TotalDrift:
                    string = "Всего дрифта";
                    break;
                case Collisions:
                    string = "Столкновений";
                    break;
            }
            return string;
        }

        public String getStringFromLong(long value) {
            String string = "";
            switch (this) {
                case Time:
                    string = String.format("%.2f", value / 100f);
                    break;
                case BestDrift:
                case TotalDrift:
                case Collisions:
                    string = String.valueOf(value);
                    break;
            }
            return string;
        }
    }

    public static class Mission {
        public static class Progress {
            public int stars = 0;
            public long record = 0;

            public Progress() {

            }

        }

        public Progress savedProgress;
        public MissionParameter variableType;
        public int[] variable = new int[3];

        public Assets.Maps map = Assets.Maps.Map1;
        public int spawn = -1;
        public int index;
        public int[] goals = new int[]{};
        public int laps = -1;
        public GoalPointer.Type goalPointerType = GoalPointer.Type.ARROW;
        public int collisions = -1;

        public int time = -1;
        public int totalDrift = -1;
        public int bestDrift = -1;

        public Mission() {

        }

        public Progress getProgressForGameInfo(Mode.GameInfo gameInfo) {
            Progress progress = new Progress();
            progress.stars = calculateStars(gameInfo);
            if ((collisions > -1) && (collisions < gameInfo.collisionCount))
                progress.stars = 0;
            if (bestDrift > gameInfo.bestDrift)
                progress.stars = 0;
            if (totalDrift > gameInfo.totalDrift)
                progress.stars = 0;
            if ((time > 0) && (time < gameInfo.passTime))
                progress.stars = 0;
            progress.record = gameInfo.getAsLong(variableType);
            return progress;
        }

        int calculateStars(Mode.GameInfo gameInfo) {
            int stars = 0;
            switch (variableType) {
                case TotalDrift:
                    if (gameInfo.totalDrift > variable[0]) {
                        stars = 3;
                    } else if (gameInfo.totalDrift > variable[1]) {
                        stars = 2;
                    } else if (gameInfo.totalDrift > variable[2]) {
                        stars = 1;
                    }
                    break;
                case Time:

                    if (gameInfo.passTime < variable[0]) {
                        stars = 3;
                    } else if (gameInfo.passTime < variable[1]) {
                        stars = 2;
                    } else if (gameInfo.passTime < variable[2]) {
                        stars = 1;
                    }
                    break;
                case BestDrift:
                    if (gameInfo.bestDrift > variable[0]) {
                        stars = 3;
                    } else if (gameInfo.bestDrift > variable[1]) {
                        stars = 2;
                    } else if (gameInfo.bestDrift > variable[2]) {
                        stars = 1;
                    }
                    break;
            }
            return stars;
        }

        public void saveProgress(Progress progress) {
            MissionsUtils.saveProgress(index, progress);
            savedProgress = progress;
        }

        public Table getDefinitionTable(Skin skin) {
            return getDefinitionTable(skin, null);
        }

        public Table getDefinitionTable(Skin skin, Mode.GameInfo gameInfo) {
            Table table = new Table();
            Label titleLabel = new Label("Задание", skin, "default", Constants.COLOR_YELLOW);
            table.add(titleLabel).colspan(3).center();

            table.row();
            Label variableLabel = new Label(variableType.getString(), skin, "small", Constants.COLOR_YELLOW);
            variableLabel.setColor(Constants.COLOR_YELLOW);
            table.add(variableLabel).padRight(Constants.UI_PAD).left();
            table.add(new Label(
                    String.format("%s | %s | %s", variable[0], variable[1], variable[2]),
                    skin, "small", Constants.COLOR_YELLOW))
                    .left().padRight(Constants.UI_PAD);

            if (gameInfo != null) {
                Label label = new Label(gameInfo.getAsString(variableType),
                        skin, "small");
                if (getProgressForGameInfo(gameInfo).stars > 0) {
                    label.setColor(Color.GREEN);
                    Image successImage = new Image(skin.getRegion(Assets.ICON_SUCCESS));
                    table.add(successImage).size(Constants.H1 * 0.5f);
                } else {
                    label.setColor(Color.RED);
                    Image failImage = new Image(skin.getRegion(Assets.ICON_FAIL));
                    table.add(failImage).size(Constants.H1 * 0.5f);
                }
                table.add(label).left().padLeft(Constants.UI_PAD).padRight(Constants.UI_PAD);
            }

            Label bestScore = new Label("", skin, "small", Constants.COLOR_YELLOW);
            bestScore.setName("bestScore");
            bestScore.setAlignment(Align.center);
            table.add(bestScore);
            if (savedProgress != null && savedProgress.record > 0) {
                bestScore.setText(String.format("BEST\n%s",
                        variableType.getStringFromLong(savedProgress.record)));
            }

            if (goals.length > 0) {
                table.row();
                table.add(new Label("Целей",
                        skin, "small", Color.WHITE)).padRight(Constants.UI_PAD).left();
                table.add(new Label(String.valueOf(goals.length),
                        skin, "small", Color.WHITE)).left().padRight(Constants.UI_PAD);
            }

            if (laps > 0) {
                table.row();
                table.add(new Label("Кругов",
                        skin, "small", Color.WHITE)).padRight(Constants.UI_PAD).left();
                table.add(new Label(String.valueOf(laps),
                        skin, "small", Color.WHITE)).left().padRight(Constants.UI_PAD);
            }

            if (time > 0) {
                table.row();
                table.add(new Label("Время",
                        skin, "small", Color.WHITE)).padRight(Constants.UI_PAD).left();
                table.add(new Label(String.valueOf(time),
                        skin, "small", Color.WHITE)).left().padRight(Constants.UI_PAD);
                if (gameInfo != null) {
                    Label label = new Label(gameInfo.getAsString(MissionParameter.Time),
                            skin, "small");
                    if (gameInfo.passTime < time) {
                        label.setColor(Color.GREEN);
                        Image successImage = new Image(skin.getRegion(Assets.ICON_SUCCESS));
                        table.add(successImage).size(Constants.H1 * 0.5f);
                    } else {
                        label.setColor(Color.RED);
                        Image failImage = new Image(skin.getRegion(Assets.ICON_FAIL));
                        table.add(failImage).size(Constants.H1 * 0.5f);
                    }
                    table.add(label).left().padLeft(Constants.UI_PAD);
                }
            }

            if (totalDrift > 0) {
                table.row();
                table.add(new Label("Всего дрифта",
                        skin, "small")).padRight(Constants.UI_PAD).left();
                table.add(new Label(String.valueOf(totalDrift),
                        skin, "small", Color.WHITE)).left().padRight(Constants.UI_PAD);
                if (gameInfo != null) {
                    Label label = new Label(gameInfo.getAsString(MissionParameter.TotalDrift),
                            skin, "small");
                    if (gameInfo.totalDrift > totalDrift) {
                        label.setColor(Color.GREEN);
                        Image successImage = new Image(skin.getRegion(Assets.ICON_SUCCESS));
                        table.add(successImage).size(Constants.H1 * 0.5f);
                    } else {
                        label.setColor(Color.RED);
                        Image failImage = new Image(skin.getRegion(Assets.ICON_FAIL));
                        table.add(failImage).size(Constants.H1 * 0.5f);
                    }
                    table.add(label).left().padLeft(Constants.UI_PAD);
                }
            }

            if (bestDrift > 0) {
                table.row();
                table.add(new Label("Лучший дрифт",
                        skin, "small", Color.WHITE)).padRight(Constants.UI_PAD).left();
                table.add(new Label(String.valueOf(bestDrift),
                        skin, "small", Color.WHITE)).left().padRight(Constants.UI_PAD);
                if (gameInfo != null) {
                    Label label = new Label(gameInfo.getAsString(MissionParameter.BestDrift),
                            skin, "small");
                    if (gameInfo.bestDrift > bestDrift) {
                        label.setColor(Color.GREEN);
                        Image successImage = new Image(skin.getRegion(Assets.ICON_SUCCESS));
                        table.add(successImage).size(Constants.H1 * 0.5f);
                    } else {
                        label.setColor(Color.RED);
                        Image failImage = new Image(skin.getRegion(Assets.ICON_FAIL));
                        table.add(failImage).size(Constants.H1 * 0.5f);
                    }
                    table.add(label).left().padLeft(Constants.UI_PAD);
                }
            }

            if (collisions > -1) {
                table.row();
                table.add(new Label("Столкновений",
                        skin, "small", Color.WHITE)).padRight(Constants.UI_PAD).left();
                table.add(new Label(String.valueOf(collisions),
                        skin, "small", Color.WHITE)).left().padRight(Constants.UI_PAD);
                if (gameInfo != null) {
                    Label label = new Label(gameInfo.getAsString(MissionParameter.Collisions),
                            skin, "small");
                    if (gameInfo.collisionCount <= collisions) {
                        label.setColor(Color.GREEN);
                        Image successImage = new Image(skin.getRegion(Assets.ICON_SUCCESS));
                        table.add(successImage).size(Constants.H1 * 0.5f);
                    } else {
                        label.setColor(Color.RED);
                        Image failImage = new Image(skin.getRegion(Assets.ICON_FAIL));
                        table.add(failImage).size(Constants.H1 * 0.5f);
                    }
                    table.add(label).left().padLeft(Constants.UI_PAD);
                }
            }

            table.left().top();
            return table;
        }

        public Table getLeadersTable(Skin skin) {
            Table table = new Table();
            table.center().top();

            HorizontalGroup titleGroup = new HorizontalGroup();
            Image image = new Image(skin.getRegion(Assets.CUP));
            titleGroup.addActor(new Container<>(image).size(Constants.H1));
            Label titleLabel = new Label("Рекорды игроков",
                    skin, "default", Constants.COLOR_YELLOW);
            titleGroup.addActor(titleLabel);
            table.add(titleGroup).colspan(2).center();

            Table scoresTable = new Table();
            table.row();
            table.add(scoresTable).colspan(2).expandX().fillX();

            LoadingImage loadingImage = new LoadingImage(MyGame.getInstance().shapeRenderer);
            loadingImage.setColor(Constants.COLOR_BLUE);
            loadingImage.layout();
            scoresTable.row();
            scoresTable.add(loadingImage).size(Constants.H1).expand().colspan(2).top();

            ArrayList<FireBaseApi.LbScore> lbScores = new ArrayList<>();
            Runnable responseHandled = () -> {
                int countResponse = (int) scoresTable.getUserObject();
                scoresTable.setUserObject(++countResponse);
                if (countResponse < 2) {
                    return;
                }
                scoresTable.clearChildren();
                Collections.sort(lbScores);
                for (FireBaseApi.LbScore lbScore : lbScores) {
                    scoresTable.row();
                    Label label = new Label(String.format("%d. %s :", lbScore.rank, lbScore.displayName),
                            skin, "small", Color.WHITE);
                    scoresTable.add(label).left();
                    Label score = new Label(lbScore.displayScore, skin, "small", Color.WHITE);
                    scoresTable.add(score).padLeft(Constants.UI_PAD).left();
                }
                loadingImage.remove();
            };

            scoresTable.setUserObject(0);
            FireBaseApi.ResponseListener<ArrayList<FireBaseApi.LbScore>> topScoresListener =
                    FireBaseUtils.getTopScores(index);
            if (topScoresListener != null) {
                topScoresListener.addCompleteListener(response -> {
                    lbScores.addAll(response);
                    responseHandled.run();
                });
            }

            FireBaseApi.ResponseListener<FireBaseApi.LbScore> playerScoreListener =
                    FireBaseUtils.getPlayerScore(index);
            if (playerScoreListener != null) {
                playerScoreListener.addCompleteListener(response -> {
                    if (response.rank > 5) {
                        lbScores.add(response);
                    }
                    responseHandled.run();
                });
            }

            return table;
        }

        public GhostSave getGhost() {
            try {
                FileHandle file = Gdx.files.local("ghost_mission_" + index);
                if (file.exists()) {
                    return new Json().fromJson(GhostSave.class, file);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        public void saveGhost(GhostSave ghostSave) {
            FileHandle file = Gdx.files.local("ghost_mission_" + index);
            new Json().toJson(ghostSave, file);
        }

        public boolean isNewProgressBetter(Progress newProgress) {
            if (savedProgress == null || savedProgress.record == 0) {
                return true;
            } else {
                if (variableType == MissionParameter.Time) {
                    return savedProgress.record > newProgress.record;
                } else {
                    return savedProgress.record < newProgress.record;
                }
            }
        }
    }

    public static class GhostSave {
        public FloatArray times = new FloatArray();
        public FloatArray xPositions = new FloatArray();
        public FloatArray yPositions = new FloatArray();
        public FloatArray angles = new FloatArray();
        public PlayerModel playerModel = new PlayerModel();

        public int currentIndex = 0;

        public GhostSave() {

        }

        public void add(float time, Vector2 position, float angle) {
            times.add(time);
            xPositions.add(position.x);
            yPositions.add(position.y);
            angles.add(angle);
        }

        public void update(float time, BaseCar car) {

            if (currentIndex < times.size) {
                if (time >= times.get(currentIndex)) {
                    car.act(time - times.get(currentIndex));
                    car.getBody().setTransform(xPositions.get(currentIndex), yPositions.get(currentIndex),
                            angles.get(currentIndex));
                    currentIndex++;
                }
            }
        }
    }

    public static Mission getMission(int index) throws NullPointerException {
        if (index > Constants.MISSIONS_COUNT) {
            throw new NullPointerException("Mission index can't be more than total count");
        }
        Mission mission = new Json()
                .fromJson(MissionsUtils.Mission.class,
                        Gdx.files.internal("missions/" + index));
        mission.index = index;
        mission.savedProgress = getProgress(index);
        return mission;
    }

    public static Mission.Progress getProgress(int index) {
        String jsonProgress = preferences.getString("mission_" + index, null);
        if (jsonProgress != null) {
            return new Json().fromJson(Mission.Progress.class, jsonProgress);
        } else if (index == 1) {
            return new Mission.Progress();
        } else {
            return null;
        }
    }

    public static void saveProgress(int index, Mission.Progress progress) {
        preferences.putString("mission_" + index, new Json().toJson(progress));
        preferences.flush();
    }
}
