package com.lifeapp.shadowracers.utils;

import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.lifeapp.shadowracers.actors.mapobjects.Barrel;
import com.lifeapp.shadowracers.actors.mapobjects.BuildingLight;
import com.lifeapp.shadowracers.actors.mapobjects.CargoContainer;
import com.lifeapp.shadowracers.actors.mapobjects.LampPost;
import com.lifeapp.shadowracers.actors.mapobjects.NpcCar;
import com.lifeapp.shadowracers.actors.mapobjects.Tree;
import com.lifeapp.shadowracers.variables.CategoryBits;

import box2dLight.RayHandler;


public class MapBodyBuilder {

    private static float tpp = 0;
    private static EarClippingTriangulator earClippingTriangulator = new EarClippingTriangulator();

    public static void buildStaticObjects(Map map, float ppt, World world) {
        tpp = 1 / ppt;
        if (map.getLayers().getIndex("static") == -1)
            return;
        MapObjects objects = map.getLayers().get("static").getObjects();
        for (MapObject object : objects) {
            BodyDef bd = getBodyDef(object);
            Body body = world.createBody(bd);
            body.setUserData(object);
            FixtureDef[] fixturesDefs = getFixtures(object);
            for (FixtureDef fixtureDef : fixturesDefs) {
                fixtureDef.filter.categoryBits = CategoryBits.OBJECT;
                body.createFixture(fixtureDef).setUserData(CategoryBits.OBJECT);
                fixtureDef.shape.dispose();
            }
        }
    }

    public static void buildDynamicObjects(Map map, float ppt, World world, Group group, RayHandler rayHandler) {
        tpp = 1 / ppt;
        if (map.getLayers().getIndex("dynamic") == -1)
            return;
        MapObjects dynamic = map.getLayers().get("dynamic").getObjects();

        for (MapObject object : dynamic) {
            Vector2 position = getCenter(object);
            float angle = object.getProperties()
                    .get("rotation", 0f, Float.class);
            switch (object.getProperties().get("type", "", String.class)) {
                case "Car":
                    group.addActor(new NpcCar(world, position, angle, rayHandler));
                    break;
                case "Barrel":
                    group.addActor(new Barrel(world, position, angle));
                    break;
                case "Tree":
                    group.addActor(new Tree(world, position, angle));
                    break;
                case "LampPost":
                    group.addActor(new LampPost(world, position, angle, rayHandler));
                    break;
                case "Lamp":
                    group.addActor(new BuildingLight(world, position, angle, rayHandler));
                    break;
                case "Container":
                    group.addActor(new CargoContainer(world, position, angle));
                    break;
            }
        }
    }

    private static BodyDef getBodyDef(MapObject object) {
        BodyDef bodyDef = new BodyDef();
        Vector2 center = new Vector2();
        if (object instanceof PolygonMapObject) {
            Polygon polygon = ((PolygonMapObject) object).getPolygon();
            polygon.getBoundingRectangle().getCenter(center);
        } else if (object instanceof CircleMapObject) {
            Circle circle = ((CircleMapObject) object).getCircle();
            center.set(circle.x, circle.y);
        } else if (object instanceof EllipseMapObject) {
            Ellipse ellipse = ((EllipseMapObject) object).getEllipse();
            center.set(ellipse.x + ((EllipseMapObject) object).getEllipse().width * 0.5f,
                    ellipse.y + ((EllipseMapObject) object).getEllipse().width * 0.5f);
        } else if (object instanceof RectangleMapObject) {
            Rectangle rectangle = ((RectangleMapObject) object).getRectangle();
            rectangle.getCenter(center);
        }
        center.x *= tpp;
        center.y *= tpp;
        bodyDef.position.set(center);
        bodyDef.type = BodyDef.BodyType.StaticBody;
        return bodyDef;
    }

    private static FixtureDef[] getFixtures(MapObject object) {
        Array<FixtureDef> fixtureDefs = new Array<FixtureDef>();
        if (object instanceof PolygonMapObject) {
            float[] vertices = ((PolygonMapObject) object).getPolygon().getTransformedVertices();
            Vector2 center = new Vector2();
            ((PolygonMapObject) object).getPolygon().getBoundingRectangle().getCenter(center);
            for (int i = 0; i < vertices.length; i += 2) {
                vertices[i] = (vertices[i] - center.x) * tpp;
                vertices[i + 1] = (vertices[i + 1] - center.y) * tpp;
            }
            short[] triangles = earClippingTriangulator.computeTriangles(vertices).toArray();
            for (int i = 0; i < triangles.length; i += 3) {
                if ((vertices[triangles[i] * 2] == vertices[triangles[(i + 1)] * 2]) &&
                        (vertices[triangles[i] * 2] == vertices[triangles[(i + 2)] * 2]))
                    continue;
                if ((vertices[triangles[i] * 2 + 1] == vertices[triangles[(i + 1)] * 2 + 1]) &&
                        (vertices[triangles[i] * 2 + 1] == vertices[triangles[(i + 2)] * 2 + 1]))
                    continue;
                float[] convexVertices = {
                        vertices[triangles[i] * 2], vertices[triangles[i] * 2 + 1],
                        vertices[triangles[(i + 1)] * 2], vertices[triangles[(i + 1)] * 2 + 1],
                        vertices[triangles[(i + 2)] * 2], vertices[triangles[(i + 2)] * 2 + 1],
                };
                PolygonShape polygon = new PolygonShape();
                polygon.set(convexVertices);
                FixtureDef fixtureDef = new FixtureDef();
                fixtureDef.shape = polygon;
                fixtureDef.density = 1f;
                fixtureDef.restitution = 0;
                fixtureDef.friction = 0;
                fixtureDefs.add(fixtureDef);
            }
        } else if (object instanceof CircleMapObject) {
            Circle circle = ((CircleMapObject) object).getCircle();
            CircleShape circleShape = new CircleShape();
            circleShape.setRadius(circle.radius * tpp);
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = circleShape;
            fixtureDef.density = 1;
            fixtureDefs.add(fixtureDef);
        } else if (object instanceof EllipseMapObject) {
            Ellipse ellipse = ((EllipseMapObject) object).getEllipse();
            CircleShape circleShape = new CircleShape();
            circleShape.setRadius(ellipse.width * tpp * 0.5f);
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = circleShape;
            fixtureDef.density = 1;
            fixtureDefs.add(fixtureDef);
        } else if (object instanceof RectangleMapObject) {
            Rectangle rectangle = ((RectangleMapObject) object).getRectangle();
            PolygonShape polygon = new PolygonShape();
            polygon.setAsBox(rectangle.getWidth() * 0.5f * tpp, rectangle.getHeight() * 0.5f * tpp);
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygon;
            fixtureDef.density = 1;
            fixtureDefs.add(fixtureDef);
        }
        return fixtureDefs.toArray(FixtureDef.class);
    }

    public static Vector2 getCenter(MapObject object) {
        float angle = (float) Math.toRadians(object.getProperties().get("rotation", 0f, Float.class));
        float halfWidth = object.getProperties().get("width", Float.class) * 0.5f;
        float halfHeight = object.getProperties().get("height", Float.class) * 0.5f;
        float x = (float) (object.getProperties().get("x", Float.class) +
                halfWidth * Math.cos(angle) - halfHeight * Math.sin(angle));
        float y = (float) (object.getProperties().get("y", Float.class) -
                halfWidth * Math.sin(angle) - halfHeight * Math.cos(angle)) + halfHeight * 2;
        return new Vector2(x, y).scl(tpp);
    }
}