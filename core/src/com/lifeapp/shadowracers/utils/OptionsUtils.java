package com.lifeapp.shadowracers.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Created by Александр on 20.03.2018.
 */

public class OptionsUtils {
    private static Preferences preferences = Gdx.app.getPreferences("options");

    private static boolean sound = preferences.getBoolean("sound", true);
    private static boolean vibration = preferences.getBoolean("vibration", true);
    private static boolean invertControl = preferences.getBoolean("invert_control", false);
    private static boolean grain = preferences.getBoolean("grain", false);


    public static void setSound(boolean value) {
        sound = value;
        preferences.putBoolean("sound", value);
        preferences.flush();
        SoundUtils.updateVolume();
    }

    public static boolean isSoundEnabled() {
        return sound;
    }

    public static void setInvertControl(boolean value) {
        invertControl = value;
        preferences.putBoolean("invert_control", invertControl);
        preferences.flush();
    }

    public static boolean isInvertControlEnabled() {
        return invertControl;
    }

    public static void setVibration(boolean value) {
        vibration = value;
        preferences.putBoolean("vibration", value);
        preferences.flush();
    }

    public static boolean isVibrationEnabled() {
        return vibration;
    }

    public static void setGrain(boolean value) {
        grain = value;
        preferences.putBoolean("grain", grain);
        preferences.flush();
    }

    public static boolean isGrainEnabled() {
        return grain;
    }

    public static boolean isFirstRun() {
        boolean value = preferences.getBoolean("first_run", true);
        preferences.putBoolean("first_run", false);
        preferences.flush();
        return value;
    }

    public static void subscribeNewGame(boolean value) {
        if (value) {
            FireBaseUtils.subscribeToTopic("newGame");
        } else {
            FireBaseUtils.unSubscribeFromTopic("newGame");
        }
        preferences.putBoolean("subscribe_newGame", value);
        preferences.flush();
    }

    public static boolean isSubscribedNewGame() {
        return preferences.getBoolean("subscribe_newGame", true);
    }

    public static boolean isMusicEnabled(){
        return true;
    }

}
