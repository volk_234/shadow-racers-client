package com.lifeapp.shadowracers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.lifeapp.shadowracers.MyGame;


public class TestScreen implements Screen {
    Stage stage = new Stage();
    BitmapFont bitmapFont = MyGame.getSkin().getFont("small");
    String textTurePath = "particle.png";
    Skin skin = MyGame.getSkin();

    @Override
    public void show() {
        textureTest();
        Label label = new Label("fps", skin) {
            @Override
            public void act(float delta) {
                super.act(delta);
                setText(String.valueOf(Gdx.graphics.getFramesPerSecond()));
            }
        };
        label.setPosition((float) (Gdx.graphics.getWidth() * 0.5 - label.getPrefWidth()), 30);
        label.setColor(Color.RED);
        stage.addActor(label);
    }

    public void textureTest() {
        for (int i = 0; i < 1000; i++) {
            Texture texture = new Texture(Gdx.files.internal(textTurePath));
            Image image = new Image(texture);
            image.setPosition(MathUtils.random(Gdx.graphics.getWidth()),
                    MathUtils.random(Gdx.graphics.getHeight()));
            stage.addActor(image);
        }
    }

    public void textureRegionTest() {
        Texture texture = new Texture(Gdx.files.internal(textTurePath));
        TextureRegion textureRegion = new TextureRegion(texture, 0, 0, texture.getWidth(), texture.getHeight());
        for (int i = 0; i < 1000; i++) {
            Image image = new Image(textureRegion);
            image.setPosition(MathUtils.random(Gdx.graphics.getWidth()),
                    MathUtils.random(Gdx.graphics.getHeight()));
            stage.addActor(image);
        }
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
