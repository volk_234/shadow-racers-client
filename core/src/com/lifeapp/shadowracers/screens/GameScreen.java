package com.lifeapp.shadowracers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.utils.OptionsUtils;
import com.lifeapp.shadowracers.variables.Assets;

/**
 * Created by Александр on 20.12.2017.
 */

public class GameScreen implements Screen {
    protected MyGame game = (MyGame) Gdx.app.getApplicationListener();
    private Mode mode;
    private TiledDrawable grainDrawable;

    private boolean paused = false;

    public GameScreen() {
        grainDrawable = new TiledDrawable(MyGame.getTextureRegion(Assets.ATLAS, Assets.NOISE));
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (mode != null) {
            mode.update(delta);
        }
        if (OptionsUtils.isGrainEnabled()) {
            game.spriteBatch.begin();
            grainDrawable.draw(game.spriteBatch, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            game.spriteBatch.end();
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        if (paused) return;
        paused = true;
        if (mode != null) mode.pause();
    }

    @Override
    public void resume() {
        paused = false;
        if (mode != null) mode.resume();
    }

    @Override
    public void hide() {
        if (this.mode != null) {
            this.mode.dispose();
            this.mode = null;
        }
    }

    @Override
    public void dispose() {
    }

    public void initMode(Mode mode) {
        if (this.mode != null) {
            this.mode.dispose();
            this.mode = null;
        }
        this.mode = mode;
        Gdx.input.setInputProcessor(mode.getUiStage());
        paused = false;
    }


    public Mode getMode() {
        return mode;
    }
}
