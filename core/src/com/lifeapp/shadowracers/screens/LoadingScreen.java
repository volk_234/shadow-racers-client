package com.lifeapp.shadowracers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.lifeapp.shadowracers.MyGame;
import com.lifeapp.shadowracers.variables.Assets;
import com.lifeapp.shadowracers.variables.Constants;

/**
 * Created by Александр on 20.12.2017.
 */

public class LoadingScreen implements Screen {
    protected MyGame game = (MyGame) Gdx.app.getApplicationListener();
    private SpriteBatch batch = game.spriteBatch;
    private Texture texture;
    private Texture lineRed;

    private Vector2 imagePos;
    private Vector2 imageSize;
    private Vector2 linePos;
    private Vector2 lineSize;

    public LoadingScreen() {
        texture = new Texture(Gdx.files.internal(Assets.LOADING_IMAGE));
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.RED);
        pixmap.fillRectangle(0, 0, 1, 1);
        lineRed = new Texture(pixmap);
        pixmap.dispose();

        imagePos = new Vector2(Constants.W1 * 2, Constants.H1 * 3);
        imageSize = new Vector2(Constants.W1 * 6, Constants.W1 * 6 / 1.7f);

        linePos = new Vector2(imagePos.x + 0.0579f * imageSize.x, imagePos.y + imageSize.y * 0.1f);
        lineSize = new Vector2(imageSize.x * 0.88f, imageSize.y * 0.177f);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        //1.7
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (MyGame.getAssetManager().update()) game.loaded();
        batch.begin();
        batch.draw(lineRed, linePos.x, linePos.y, lineSize.x * MyGame.getAssetManager().getProgress(), lineSize.y);
        batch.draw(texture, imagePos.x, imagePos.y, imageSize.x, imageSize.y);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        texture.dispose();
        lineRed.dispose();
    }
}
