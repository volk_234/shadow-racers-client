package com.lifeapp.shadowracers;

import com.appodeal.gdx.GdxAppodeal;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.I18NBundle;
import com.lifeapp.shadowracers.actors.cars.BaseCar;
import com.lifeapp.shadowracers.actors.modals.InfoModal;
import com.lifeapp.shadowracers.actors.modals.menu.MenuModal;
import com.lifeapp.shadowracers.modes.LobbyMode;
import com.lifeapp.shadowracers.modes.Mode;
import com.lifeapp.shadowracers.modes.SingleMode;
import com.lifeapp.shadowracers.network.ServerConnection;
import com.lifeapp.shadowracers.screens.GameScreen;
import com.lifeapp.shadowracers.screens.LoadingScreen;
import com.lifeapp.shadowracers.utils.FireBaseUtils;
import com.lifeapp.shadowracers.utils.OptionsUtils;
import com.lifeapp.shadowracers.utils.ShopUtils;
import com.lifeapp.shadowracers.utils.SoundUtils;
import com.lifeapp.shadowracers.utils.StatsUtils;
import com.lifeapp.shadowracers.variables.Assets;

public class MyGame extends Game {

    private ServerConnection serverConnection;
    public GameScreen gameScreen;
    public I18NBundle i18NBundle;
    private MyAssetManager assetManager;
    private MySkin skin;
    public ShapeRenderer shapeRenderer;

    public SpriteBatch spriteBatch;

    public boolean shouldRunNetwork = false;

    private static MyGame instance;

    public MyGame(FireBaseApi fireBaseApi) {
        FireBaseUtils.fireBaseApi = fireBaseApi;
        instance = this;
    }

    @Override
    public void create() {
        GdxAppodeal.setTesting(true);
        String appKey = "3de916b8c77eb951ef7a5a5c46bef505bd1a8b6b1e48646f";
        GdxAppodeal.confirm(GdxAppodeal.SKIPPABLE_VIDEO);
        GdxAppodeal.initialize(appKey, GdxAppodeal.SKIPPABLE_VIDEO | GdxAppodeal.INTERSTITIAL | GdxAppodeal.BANNER | GdxAppodeal.REWARDED_VIDEO);

        assetManager = new MyAssetManager();
        spriteBatch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        SoundUtils.setAssetManager(assetManager);

        LoadingScreen loadingScreen = new LoadingScreen();
        setScreen(loadingScreen);

        if (OptionsUtils.isFirstRun()) {
            FireBaseUtils.subscribeToTopic("newGame");
            ShopUtils.buyCar("BmwCar");
            ShopUtils.useCar("BmwCar");
        }

        if (!FireBaseUtils.isSignedIn()) {
            FireBaseUtils.signInSilently();
        }

        ShaderProgram.pedantic = false;


    }

    public void loaded() {
        i18NBundle = assetManager.get(Assets.LOC_BUNDLE, I18NBundle.class);
        skin = new MySkin(assetManager);
        gameScreen = new GameScreen();
        if (shouldRunNetwork) {
            LobbyMode lobbyMode = new LobbyMode();
            gameScreen.initMode(lobbyMode);
            lobbyMode.requestForLobby();
        } else {
            Mode mode = new Mode(Assets.Maps.Map1);
            gameScreen.initMode(mode);
            BaseCar car = gameScreen.getMode().makeCar(StatsUtils.getPlayerModel());
            car.controlByPlayer();
            gameScreen.getMode().showModal(new MenuModal());
        }
        setScreen(gameScreen);
        if(OptionsUtils.isMusicEnabled()){
            SoundUtils.playMusic();
        }
    }

    public void offerOnline() {
        if (gameScreen.getMode() instanceof SingleMode) {
            InfoModal infoModal = new InfoModal("Найдена новая онлайн игра!\nКликни, чтобы присоединиться", 5);
            infoModal.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    LobbyMode lobbyMode = new LobbyMode();
                    gameScreen.initMode(lobbyMode);
                    lobbyMode.requestForLobby();
                    return true;
                }
            });
            gameScreen.getMode().showModal(infoModal, 1);
        }
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        if (serverConnection != null && serverConnection.isAlive()) {
            serverConnection.close();
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    public ServerConnection connectToServer() {
        if (serverConnection == null) {
            serverConnection = new ServerConnection();
        } else {
            serverConnection.reconnect();
        }
        return serverConnection.isAlive() ? serverConnection : null;
    }

    public static MyGame getInstance() {
        return instance;
    }

    public static TextureAtlas getAtlas(String atlas) {
        return getInstance().assetManager.get(atlas, TextureAtlas.class);
    }

    public static TextureRegion getTextureRegion(String atlas, String regionName) {
        return getInstance().assetManager.get(atlas, TextureAtlas.class).findRegion(regionName);
    }

    public static MyAssetManager getAssetManager() {
        return getInstance().assetManager;
    }

    public static Skin getSkin() {
        return getInstance().skin;
    }

}
